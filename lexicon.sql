/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : cqrobot

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2019-09-24 21:18:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lexicon
-- ----------------------------
DROP TABLE IF EXISTS `lexicon`;
CREATE TABLE `lexicon` (
  `id` int(11) NOT NULL auto_increment,
  `create_date` varchar(255) collate utf8_esperanto_ci default NULL,
  `create_name` varchar(255) collate utf8_esperanto_ci default NULL,
  `antistop` varchar(255) collate utf8_esperanto_ci default NULL,
  `code` varchar(255) collate utf8_esperanto_ci default NULL,
  `number` varchar(255) collate utf8_esperanto_ci default NULL,
  `response` varchar(255) collate utf8_esperanto_ci default NULL,
  `type` varchar(255) collate utf8_esperanto_ci default NULL,
  `isuser` bit(1) default NULL,
  `qunnumber` varchar(255) collate utf8_esperanto_ci default NULL,
  `sum` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_esperanto_ci;
