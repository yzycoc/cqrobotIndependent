/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : cqrobot

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2019-09-24 21:17:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for group_list
-- ----------------------------
DROP TABLE IF EXISTS `group_list`;
CREATE TABLE `group_list` (
  `id` int(11) NOT NULL auto_increment,
  `groupcode` varchar(255) collate utf8_esperanto_ci default NULL,
  `name` varchar(255) collate utf8_esperanto_ci default NULL,
  `isfalsetrue` varchar(255) collate utf8_esperanto_ci default NULL,
  `head_url` varchar(255) collate utf8_esperanto_ci default NULL,
  `adminlist` varchar(255) collate utf8_esperanto_ci default NULL,
  `board` varchar(255) collate utf8_esperanto_ci default NULL,
  `comoleteintro` varchar(255) collate utf8_esperanto_ci default NULL,
  `createtime` varchar(255) collate utf8_esperanto_ci default NULL,
  `isadmin` varchar(255) collate utf8_esperanto_ci default NULL,
  `level` varchar(255) collate utf8_esperanto_ci default NULL,
  `maxnumber` varchar(255) collate utf8_esperanto_ci default NULL,
  `number` int(255) default NULL,
  `opentype` varchar(255) collate utf8_esperanto_ci default NULL,
  `ownerqq` varchar(255) collate utf8_esperanto_ci default NULL,
  `pos` varchar(255) collate utf8_esperanto_ci default NULL,
  `tags` varchar(255) collate utf8_esperanto_ci default NULL,
  `sysuser` varchar(255) collate utf8_esperanto_ci default NULL,
  `endtime` varchar(255) collate utf8_esperanto_ci default NULL,
  `borad` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_esperanto_ci;
