/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : cqrobot

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2019-09-24 21:17:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for coc_binding
-- ----------------------------
DROP TABLE IF EXISTS `coc_binding`;
CREATE TABLE `coc_binding` (
  `id` int(11) NOT NULL auto_increment,
  `create_date` varchar(255) default NULL,
  `create_name` varchar(255) default NULL,
  `msg` varchar(255) default NULL,
  `qqcode` varchar(255) default NULL,
  `tag` varchar(255) default NULL,
  `type` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
