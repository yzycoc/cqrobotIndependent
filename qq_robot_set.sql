/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50018
Source Host           : localhost:3306
Source Database       : cqrobot

Target Server Type    : MYSQL
Target Server Version : 50018
File Encoding         : 65001

Date: 2019-09-24 21:18:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for qq_robot_set
-- ----------------------------
DROP TABLE IF EXISTS `qq_robot_set`;
CREATE TABLE `qq_robot_set` (
  `id` int(11) NOT NULL auto_increment,
  `create_date` varchar(255) default NULL,
  `create_name` varchar(255) default NULL,
  `cq_path` varchar(255) default NULL,
  `encode` varchar(255) default NULL,
  `link_ip` varchar(255) default NULL,
  `localqqcode` varchar(255) default NULL,
  `localqqnick` varchar(255) default NULL,
  `java_port` varchar(255) default NULL,
  `server_path` varchar(255) default NULL,
  `server_port` varchar(255) default NULL,
  `backlog` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qq_robot_set
-- ----------------------------
INSERT INTO `qq_robot_set` VALUES ('1', null, null, 'C:\\Users\\936642284\\Desktop\\酷Q Pro', '', '', '艾特我', '2058308326', '0', '', '9999', '6');
