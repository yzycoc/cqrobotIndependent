package tgc.edu.yzy.custom.entity.coc;
/***
 * 返回的部落图片
 * @author 936642284
 *
 */
public class BadgeUrls {
	private String small;
	private String large;
	private String medium;
	/***
	 * 小图片
	 * @return
	 */
	public String getSmall() {
		return small;
	}
	public void setSmall(String small) {
		this.small = small;
	}
	/***
	 * 
	 * @return
	 */
	public String getLarge() {
		return large;
	}
	public void setLarge(String large) {
		this.large = large;
	}
	/***
	 * 大图片
	 * @return
	 */
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	
}
