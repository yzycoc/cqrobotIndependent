package tgc.edu.yzy.custom.entity.goTulin;

public class Location {
	private String city;
	private String province;
	private String street;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Location(String city, String province, String street) {
		super();
		this.city = city;
		this.province = province;
		this.street = street;
	}
	public Location(String city, String province) {
		super();
		this.city = city;
		this.province = province;
	}
	public Location(String city) {
		super();
		this.city = city;
	}
	
}
