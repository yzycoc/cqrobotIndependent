package tgc.edu.yzy.custom;

public class CocForm {
	private String reason;
	private String message;
	private String gototype;
	private Object detail;
	private String error;//返回的内容 
	private Boolean dataisnull;//数据是否为空
	public String getReason() {
		return reason;
	}
	public Boolean getDataisnull() {
		return dataisnull;
	}
	public void setDataisnull(Boolean dataisnull) {
		this.dataisnull = dataisnull;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getGototype() {
		return gototype;
	}
	public void setGototype(String gototype) {
		this.gototype = gototype;
	}
	public Object getDetail() {
		return detail;
	}
	public void setDetail(Object detail) {
		this.detail = detail;
	}
	public CocForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public CocForm(String reason, String message, String gototype, Object detail, String error, Boolean dataisnull) {
		super();
		this.reason = reason;
		this.message = message;
		this.gototype = gototype;
		this.detail = detail;
		this.error = error;
		this.dataisnull = dataisnull;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}

	
}
