package tgc.edu.yzy.custom.entity.coc;

public class Clans {
	private String tag;
	private String name;
	private String type;
	private String description;
	private String locationId;
	private BadgeUrls badgeUrls;
	private String clanLevel;
	private String clanPoints;
	private String clanVersusPoints;
	private String requiredTrophies;
	private String warFrequency;
	private String warWinStreak;
	private String warWins;
	private String warTies;
	private String warLosses;
	private Boolean isWarLogPublic;
	private Bests bests;
	private String members;
	private String coolDownRefreshPlayerEndTime;
	private Info info;
	private Boolean hasDetailedHistory;
	
	/** 失败 **/
	private String statusCode;
	private String error;
	private String message;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public BadgeUrls getBadgeUrls() {
		return badgeUrls;
	}
	public void setBadgeUrls(BadgeUrls badgeUrls) {
		this.badgeUrls = badgeUrls;
	}
	public String getClanLevel() {
		return clanLevel;
	}
	public void setClanLevel(String clanLevel) {
		this.clanLevel = clanLevel;
	}
	public String getClanPoints() {
		return clanPoints;
	}
	public void setClanPoints(String clanPoints) {
		this.clanPoints = clanPoints;
	}
	public String getClanVersusPoints() {
		return clanVersusPoints;
	}
	public void setClanVersusPoints(String clanVersusPoints) {
		this.clanVersusPoints = clanVersusPoints;
	}
	public String getRequiredTrophies() {
		return requiredTrophies;
	}
	public void setRequiredTrophies(String requiredTrophies) {
		this.requiredTrophies = requiredTrophies;
	}
	public String getWarFrequency() {
		return warFrequency;
	}
	public void setWarFrequency(String warFrequency) {
		this.warFrequency = warFrequency;
	}
	public String getWarWinStreak() {
		return warWinStreak;
	}
	public void setWarWinStreak(String warWinStreak) {
		this.warWinStreak = warWinStreak;
	}
	public String getWarWins() {
		return warWins;
	}
	public void setWarWins(String warWins) {
		this.warWins = warWins;
	}
	public String getWarTies() {
		return warTies;
	}
	public void setWarTies(String warTies) {
		this.warTies = warTies;
	}
	public String getWarLosses() {
		return warLosses;
	}
	public void setWarLosses(String warLosses) {
		this.warLosses = warLosses;
	}
	
	public Boolean getIsWarLogPublic() {
		return isWarLogPublic;
	}
	public void setIsWarLogPublic(Boolean isWarLogPublic) {
		this.isWarLogPublic = isWarLogPublic;
	}
	public Bests getBests() {
		return bests;
	}
	public void setBests(Bests bests) {
		this.bests = bests;
	}
	public String getMembers() {
		return members;
	}
	public void setMembers(String members) {
		this.members = members;
	}
	public String getCoolDownRefreshPlayerEndTime() {
		return coolDownRefreshPlayerEndTime;
	}
	public void setCoolDownRefreshPlayerEndTime(String coolDownRefreshPlayerEndTime) {
		this.coolDownRefreshPlayerEndTime = coolDownRefreshPlayerEndTime;
	}
	public Info getInfo() {
		return info;
	}
	public void setInfo(Info info) {
		this.info = info;
	}
	public Boolean getHasDetailedHistory() {
		return hasDetailedHistory;
	}
	public void setHasDetailedHistory(Boolean hasDetailedHistory) {
		this.hasDetailedHistory = hasDetailedHistory;
	}
	
}
