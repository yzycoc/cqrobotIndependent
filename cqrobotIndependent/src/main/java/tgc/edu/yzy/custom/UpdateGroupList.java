package tgc.edu.yzy.custom;

import java.util.ArrayList;
import java.util.List;

import com.forte.qqrobot.beans.messages.result.inner.Group;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.bean.GroupList;
import tgc.edu.yzy.controller.MyqqRobot;
import tgc.edu.yzy.service.GroupListService;

public class UpdateGroupList {
	public static List<String> qqgroupList = new ArrayList<String>();;
	public static void uodate(MsgSender msgSender, CQCodeUtil cqCodeUtil) {
		System.out.println("刷新群内容");
		/** 获取数据库所有的群  **/
		GroupListService bean = SpringContextUtil.getBean(GroupListService.class);
		
		com.forte.qqrobot.beans.messages.result.GroupList groupList3 = msgSender.GETTER.getGroupList();
		Group[] Grouplist = groupList3.getList();
		MyqqRobot.myrobotqq.put("qqgroupList",Grouplist.length);
		for(int j=0;j<Grouplist.length;j++) {
			UpdateGroupList.qqgroupList.add(Grouplist[j].getCode());
			GroupList one = bean.query().eq("groupcode", Grouplist[j].getCode()).one();
			if(one==null) {
				try {
					GroupList grouplists = new GroupList();
					grouplists.setGroupcode(Grouplist[j].getCode());
					grouplists.setName(Grouplist[j].getName());
					grouplists.setIsfalsetrue("在群");
					grouplists.setHeadUrl(Grouplist[j].getHeadUrl());
					bean.save(grouplists);
				} catch (Exception e) {
					GroupList grouplists = new GroupList();
					grouplists.setGroupcode(Grouplist[j].getCode());
					bean.save(grouplists);
				}
			}else {
				one.setName(Grouplist[j].getName());
				one.setHeadUrl(Grouplist[j].getHeadUrl());
				bean.updateById(one);
			}
		}
		List<GroupList> list = bean.list();
		
		for (int i = 0; i < list.size(); i++) {
			Boolean isfalsetrue = false;
			for (int j = 0; j < Grouplist.length; j++) {
				if(list.get(i).getGroupcode().equals(Grouplist[j].getCode())) {
					if(list.get(i).getIsfalsetrue()==null||list.get(i).getIsfalsetrue()==""||list.get(i).getIsfalsetrue().equals("离群")) {
						GroupList groupList2 = new GroupList();
						groupList2.setId(list.get(i).getId());
						groupList2.setIsfalsetrue("在群");
						bean.updateById(groupList2);
					}
					isfalsetrue = true;
					break;
				}	
			}
			if(isfalsetrue ==false) {
				GroupList groupList2 = new GroupList();
				groupList2.setId(list.get(i).getId());
				groupList2.setIsfalsetrue("离群");
				bean.updateById(groupList2);
			}
		}
	}
	
	public static Boolean RobotUpdate(MsgSender msgSender, CQCodeUtil cqCodeUtil, String string) {
		Boolean isZq = false;
		System.out.println("刷新群内容");
		GroupListService bean = SpringContextUtil.getBean(GroupListService.class);
		/** 获取数据库所有的群  **/
		com.forte.qqrobot.beans.messages.result.GroupList groupList3 = msgSender.GETTER.getGroupList();
		Group[] Grouplist = groupList3.getList();
		MyqqRobot.myrobotqq.put("qqgroupList",Grouplist.length);
		for(int j=0;j<Grouplist.length;j++) {
			string = string ==null?"-":string;
			if(string.equals(Grouplist[j].getCode())) {
				isZq = true;
			}
				
				
			UpdateGroupList.qqgroupList.add(Grouplist[j].getCode());
			GroupList one = bean.query().eq("groupcode", Grouplist[j].getCode()).one();
			if(one==null) {
				GroupList grouplists = new GroupList();
				grouplists.setName(Grouplist[j].getName());
				grouplists.setGroupcode(Grouplist[j].getCode());
				grouplists.setIsfalsetrue("在群");
				grouplists.setHeadUrl(Grouplist[j].getHeadUrl());
				try {
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				bean.save(grouplists);
			}else {
				one.setName(Grouplist[j].getName());
				one.setHeadUrl(Grouplist[j].getHeadUrl());
				bean.updateById(one);
			}
		}
		List<GroupList> list = bean.list();
		
		for (int i = 0; i < list.size(); i++) {
			Boolean isfalsetrue = false;
			for (int j = 0; j < Grouplist.length; j++) {
				if(list.get(i).getGroupcode().equals(Grouplist[j].getCode())) {
					if(list.get(i).getIsfalsetrue()==null||list.get(i).getIsfalsetrue()==""||list.get(i).getIsfalsetrue().equals("离群")) {
						GroupList groupList2 = new GroupList();
						groupList2.setId(list.get(i).getId());
						groupList2.setIsfalsetrue("在群");
						bean.updateById(groupList2);
					}
					isfalsetrue = true;
					break;
				}	
			}
			if(isfalsetrue ==false) {
				GroupList groupList2 = new GroupList();
				groupList2.setId(list.get(i).getId());
				groupList2.setIsfalsetrue("离群");
				bean.updateById(groupList2);
			}
		}
		return isZq;
	}
}
