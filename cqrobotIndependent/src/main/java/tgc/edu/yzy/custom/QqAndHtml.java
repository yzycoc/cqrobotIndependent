package tgc.edu.yzy.custom;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.FileCopyUtils;


public class QqAndHtml {
	/***
	 * 将酷Q 图片格式转换成 image格式
	 * @param msg
	 * @return
	 */
	public static String getMsghtml(String msg) {
		//查询语言类是否包含图片
		String[] split = msg.split("\\[CQ:image,file=");
		if(split.length>1) {
			//将所有图片的文件名取出
			String a[] = new String[split.length-1];
			for (int i = 1; i < split.length; i++) {
				a[i-1] = split[i].substring(0, split[i].indexOf("]"));
			}
			for (int i = 0; i < a.length; i++) {
				//取出文件里的图片路径及高度
				String[] jpgCqimg = getJpgCqimg(a[i]);
				//将CQ图片替换成img格式的图片
				msg = msg.replace(
						"[CQ:image,file="+a[i]+"]", 
						"<a href=\""+jpgCqimg[0]+"\" target=\"_blank\" class=\"layui-btn layui-btn-xs\">查看图片</a>");
			}
		}
		return msg;
	}
	/****
	 * 转换成其他的
	 * @param msg
	 * @return
	 */
	public static String getMsgCq(String msg) {
		//查询语言类是否包含图片
		String[] split = msg.split("\\[CQ:image,file=");
		if(split.length>1) {
			//将所有图片的文件名取出
			String a[] = new String[split.length-1];
			for (int i = 1; i < split.length; i++) {
				a[i-1] = split[i].substring(0, split[i].indexOf("]"));
			}
			for (int i = 0; i < a.length; i++) {
				//取出文件里的图片路径及高度
				getCopy(a[i]);
				//将CQ图片替换成img格式的图片
				msg = msg.replace(
						"[CQ:image,file="+a[i]+"]", 
						"&#91;CQ:image,file=image/user/"+a[i]+"&#93;");
			}
		}
		return msg;
	}

	
	private static void getCopy(String string) {
		String cqPath = BaseConfiguration.getCqPath();
		InputStream input;
		try {
			input = new FileInputStream(cqPath+"\\data\\image\\"+string +".cqimg");
			FileOutputStream output = new FileOutputStream(cqPath+"\\data\\image\\image\\user\\"+string +".cqimg"); 
			FileCopyUtils.copy(input, output);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	public static Boolean isNumberc(String numbers) {
		boolean flag = false;
	    Pattern p = Pattern.compile(".*\\d+.*");
	    Matcher m = p.matcher(numbers);
	    if (m.matches()) {
	        flag = true;
	    }
	    return flag;
	}
	
	
	
	
	/***
	 * 取出图片的
	 * 	1.地址
	 * 	2.高度
	 * 	3.长度
	 * @param string
	 * @return
	 */
	private static String[] getJpgCqimg(String string) {
		String imageattribute[] = new String[3];
		String cqPath = BaseConfiguration.getCqPath();
		File file = new File(cqPath+"\\data\\image\\"+string +".cqimg");
		StringBuilder result = new StringBuilder();
	    try{
	        BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
	        String s = null;
	        while((s = br.readLine())!=null){//使用readLine方法，一次读一行
	            result.append(System.lineSeparator()+s);
	        }
	        br.close();    
	    }catch(Exception e){
	        e.printStackTrace();
	    }
	    String[] split = result.toString().split("\n");
	    imageattribute[0] = split[6].substring(split[6].indexOf("=")+1, split[6].length()-1);
	    imageattribute[1] = split[3].substring(split[3].indexOf("=")+1, split[3].length()-1);
	    imageattribute[2] = split[4].substring(split[4].indexOf("=")+1, split[4].length()-1);
	    return imageattribute;
	}
	/***
	 * 去除字段中的所有number
	 * @param str
	 * @return
	 */
	public static String stringint(String str) {
		str = str.trim();
		String str2 = "";
		if (str != null && !"".equals(str)) {
			for (int i = 0; i < str.length(); i++) {
				if (str.charAt(i) >= 48 && str.charAt(i) <= 57) {
					str2 += str.charAt(i);
				}
			}

		}
		return str2;
	}
}
