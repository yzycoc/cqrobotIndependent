package tgc.edu.yzy.custom.entity.goTulin;

public class InputImage {
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public InputImage(String url) {
		super();
		this.url = url;
	}

	public InputImage() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
