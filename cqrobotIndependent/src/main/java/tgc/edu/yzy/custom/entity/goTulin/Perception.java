package tgc.edu.yzy.custom.entity.goTulin;

public class Perception {
	private InputText inputText;
	private InputImage inputImage;
	private SelfInfo selfInfo;
	public InputText getInputText() {
		return inputText;
	}
	public void setInputText(InputText inputText) {
		this.inputText = inputText;
	}
	public InputImage getInputImage() {
		return inputImage;
	}
	public void setInputImage(InputImage inputImage) {
		this.inputImage = inputImage;
	}
	public SelfInfo getSelfInfo() {
		return selfInfo;
	}
	public void setSelfInfo(SelfInfo selfInfo) {
		this.selfInfo = selfInfo;
	}
	public Perception() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Perception(InputText inputText, InputImage inputImage, SelfInfo selfInfo) {
		super();
		this.inputText = inputText;
		this.inputImage = inputImage;
		this.selfInfo = selfInfo;
	}
	public Perception(InputText inputText) {
		super();
		this.inputText = inputText;
	}
	public Perception(InputText inputText, SelfInfo selfInfo) {
		super();
		this.inputText = inputText;
		this.selfInfo = selfInfo;
	}
	
}
