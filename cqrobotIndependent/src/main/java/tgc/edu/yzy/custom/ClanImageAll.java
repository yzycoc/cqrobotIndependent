package tgc.edu.yzy.custom;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import javax.imageio.ImageIO;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.forte.qqrobot.utils.BaseLocalThreadPool;

import tgc.edu.yzy.cocApi.custom.CocApiAndCqCustom;
import tgc.edu.yzy.cocApi.custom.DataProcessing;

/***
 * 最新版查询部落配置
 * @author 936642284
 *
 */
public class ClanImageAll {
	//储存网络临时获取的图片
	public static CacheMap<String,BufferedImage> cachebuffer = new CacheMap<String, BufferedImage>();
	static final String[] clansId={"clanid000","clanid56000000","clanid56000001","clanid56000002","clanid56000003","clanid56000004","clanid56000005","clanid56000006","clanid56000007","clanid56000008","clanid56000009","clanid56000010","clanid56000011","clanid56000012","clanid56000013","clanid56000014","clanid56000015"};
	//存储常用图片的地方
	private static Map<String,BufferedImage> buffer = new HashMap<>();
	static {
		BufferedImage b3 = null;
		try {
			while(b3==null) {
				b3 = ImageIO.read(new File("C:\\cocutil\\matter\\badgeUrlsClan.png"));//获取
				System.out.println("已获取C盘下badgeUrls图片");
				buffer.put("badgeUrlsClan", b3);
				buffer.put("cocdiandian", ImageIO.read(new File("C:\\cocutil\\matter\\cocdiandian.png")));
				buffer.put("JBZclan", ImageIO.read(new File("C:\\cocutil\\matter\\JBZclan.png")));//主世界奖杯
				buffer.put("JBYclan", ImageIO.read(new File("C:\\cocutil\\matter\\JBYclan.png")));//主世界奖杯
				for (int i = 0; i < clansId.length; i++) {
					buffer.put(clansId[i], ImageIO.read(new File("C:\\cocutil\\matter\\"+clansId[i]+".png")));//主世界奖杯
					System.out.println("素材录入"+clansId[i]);
				}
				System.out.println("已获取C盘下图片：cocheader");
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	
	public static String get(String tag) {
		try {
			/** 1.数据准备区间  ***/
			//首先是查询部落
			String sendGetCoc = BackEndHttpRequest.sendGetCoc("https://api.clashofclans.com/v1/clans/%23"+tag);
			//String sendGetCoc = BackEndHttpRequest.sendGet("http://47.100.197.180:8855/coctest","tag="+tag);
			if(sendGetCoc.length()<4) {
				return "无法查询到部落信息！";
			}
			JSONObject clans = JSON.parseObject(sendGetCoc);
			//查询部落人数
			Integer members = clans.getInteger("members")==null?0:clans.getInteger("members");
			if(members==null||members<1) {
				return "部落无人，无统计价值，现可直接查询部落信息。";
			}
			//储存玩家全部信息的集合
			List<JSONObject> palyer = new ArrayList<>();
			JSONArray palyerList = clans.getJSONArray("memberList");
			//--- 获取连接池
			Executor threadPool = BaseLocalThreadPool.getThreadPool();
			CountDownLatch count = new CountDownLatch(palyerList.size());//玩家线程阻塞
			for (int i = 0; i < palyerList.size(); i++) {
				JSONObject jsonObject = palyerList.getJSONObject(i);
				threadPool.execute(()->{
					//获取玩家信息保存到 玩家集合当中去
					try {
						//palyer.add(JSON.parseObject(BackEndHttpRequest.sendGet("http://47.100.197.180:8855/coc","tag="+jsonObject.getString("tag").substring(1, jsonObject.getString("tag").length()))));
						palyer.add(JSON.parseObject(BackEndHttpRequest.sendGetCoc("https://api.clashofclans.com/v1/players/%23"+jsonObject.getString("tag").substring(1, jsonObject.getString("tag").length()))));
					} catch (Exception e) {
						System.out.println("获取部落成员失败:"+clans.getString("name")+"    "+clans.getString("tag")+"   玩家："+jsonObject.getString("tag"));
					}
					count.countDown();
				});
			}
			
			CountDownLatch badgeUrlscount = new CountDownLatch(1);//专门为获取部落图片开启的线程 ，最后关闭
			BufferedImage badgeUrlsbufferedImage = cachebuffer.get("badgeUrls"+tag);//
			//--- 获取部落图片
			if(badgeUrlsbufferedImage==null) {
				//这里开线程是为了获取部落图标
				Executor threadPool2 = BaseLocalThreadPool.getThreadPool();
				threadPool2.execute(()->{
					String badgerUrlslarge = clans.getJSONObject("badgeUrls").getString("large");
					if(badgerUrlslarge!=null) {
						try {
							BufferedImage httpsGet = Https.httpsGet(badgerUrlslarge, null);
							cachebuffer.putPlusMinutes("badgeUrls"+tag, httpsGet, 30);//存储30分钟
						} catch (Exception e) {
							System.out.println("获取部落图片失败:"+clans.getString("name")+"    "+clans.getString("tag"));
						}
						cachebuffer.detect();//删除缓存存在过期的值
					}
					badgeUrlscount.countDown();
				});
			}else {
				badgeUrlscount.countDown();//进程直接删除一个，不进行拦截
			}
			
			
			//这里获取玩家三个竞标图片	
			List<BufferedImage> clansIDList = new ArrayList<>();
			JSONArray jsonArray = clans.getJSONArray("labels");
			CountDownLatch clansIDS = new CountDownLatch(jsonArray==null?0:jsonArray.size());
			if(jsonArray!=null&&jsonArray.size()>0) {
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject ClanID = jsonArray.getJSONObject(i);
					Executor threadPool3 = BaseLocalThreadPool.getThreadPool();
					threadPool3.execute(()->{
						String getIDS = ClanID.getString("id");
						BufferedImage bufferedImage = buffer.get("clanid"+getIDS);
						if(bufferedImage==null) {//获取这个ID图片为空则去获取网络图片
							bufferedImage = Https.httpsGet(ClanID.getJSONObject("iconUrls").getString("small"), null);
							try {
								ImageIO.write(bufferedImage, "png", new File("C:\\cocutil\\matter\\clanid"+getIDS+".png"));
								buffer.put("clanid"+getIDS,bufferedImage);
								clansIDList.add(bufferedImage);
								clansIDS.countDown();
							} catch (IOException e) {
								e.printStackTrace();
								System.out.println("获取图片失败");
							}
						}else {
							clansIDList.add(bufferedImage);
							clansIDS.countDown();
						}
						
					});
				}
			}
			
			
			
			// 主线程阻塞，等待计数结束 ---> 获取玩家信息的拦截
			count.await();
			if(members != palyer.size()) {
				System.out.println("部落玩家"+members+"\t实际获取的玩家"+palyer.size());
				return "玩家信息获取失败，请稍后重新获取...";
			} 
			/** 2.数据处理区间 **/
			DataProcessing data = dataProcessing(clans,palyer);
			
			
			
			/** 3.图片生成区   **/ 
			//获取头部图片
			BufferedImage headerImage = ImageIO.read(new File("C:\\cocutil\\matter\\cocheader.png"));//buffer.get("cocheader");
			Graphics2D g = (Graphics2D)headerImage.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			
			
			//部落名称
			Font font = new Font("微软雅黑",Font.BOLD,36);
			g.setFont(font);
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,1f));
			g.drawString(clans.getString("name"), 180, 140);//左边 宽度 右边高度
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			//主世界总奖杯
			g.setColor(Color.yellow);
			g.drawImage(buffer.get("JBZclan"), 172, 155, 35, 40, null);
			g.drawString(clans.getString("clanPoints"), 210, 184);
			//夜世界总奖杯
			g.drawImage(buffer.get("JBYclan"), 360, 153, 35, 40, null);
			g.drawString(clans.getString("clanVersusPoints"), 400, 181);
			
			//国家 等级 人数 进步罗情况
			StringBuffer ClanAllNumber = new StringBuffer();
			String locations = clans.getJSONObject("location")==null?"":clans.getJSONObject("location").getString("name");
			ClanAllNumber.append(CocApiAndCqCustom.location(locations)+" | ");
			ClanAllNumber.append(clans.getString("clanLevel")+"级 | ");
			ClanAllNumber.append(clans.getString("members")+"人 | ");
			ClanAllNumber.append(CocApiAndCqCustom.CocTpe(clans.getString("type")));
			
			g.setFont(new Font("微软雅黑",Font.TYPE1_FONT,22));
			g.setColor(Color.white);
			g.drawString(ClanAllNumber.toString(), 180, 220);
			
			//塞入部落标签
			g.setFont(new Font("微软雅黑",Font.ITALIC,24));
			g.setColor(Color.blue);
			g.drawString(clans.getString("tag"), 208, 265);
			
			//塞入部落图标
			badgeUrlscount.await();
			if(badgeUrlsbufferedImage==null) {
				BufferedImage newbadgeUrlsbufferedImage = cachebuffer.get("badgeUrls"+tag);
				if(newbadgeUrlsbufferedImage!=null) {//判断网络获取是否成功
					g.drawImage(newbadgeUrlsbufferedImage, 55, 125, 120, 120, null);
				}else {//不成功直接获取系统的图片
					g.drawImage(buffer.get("badgeUrlsClan"), 55, 125, 120, 120, null);
				}
			}else {
				g.drawImage(badgeUrlsbufferedImage, 55, 125, 120, 120, null);
			}
			
			//放部落竞赛图片
			clansIDS.await();
			for (int i = 0; i < clansIDList.size(); i++) {
				g.drawImage(clansIDList.get(i), 50+(i*60), 295, 50, 50, null);
			}
			
			//处理部落配置配置
			BufferedImage cocdiandian = buffer.get("cocdiandian");
			int h_heigth = 480;
			int h_width = 10;
			int h_i = 0;
			String[][] townHallLevel = data.getTownHallLevel();
			g.drawImage(cocdiandian, 80, h_heigth, 20, 30, null);
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			g.setColor(Color.white);
			g.drawString("主世界配置", 115, h_heigth+25);
			h_heigth+=35;
			for (String[] strings : townHallLevel) {
				String User_number = strings[1];
				if(!"无".equals(User_number)) {
					BufferedImage coc = ImageIO.read(new File("C:\\cocutil\\matter\\moban\\"+strings[0]+".png"));
					Graphics2D cocg = (Graphics2D)coc.createGraphics();
					cocg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
					if(User_number.length()>=2) {
						cocg.setFont(new Font("微软雅黑",Font.BOLD,20));
					}else {
						cocg.setFont(new Font("微软雅黑",Font.BOLD,26));
					}
					cocg.setColor(Color.yellow);
					cocg.drawString(User_number+" 个", 153, 62);
					g.drawImage(coc, h_width, h_heigth, 228, 187, null);
					if(h_i==3) {
						h_heigth+=80;
						h_width = 10;
						h_i = -1;
					}else {
						h_width+=170;
					}
					h_i++;
				}
			}
			if(h_width!=10) {
				h_heigth+=120;
				h_width = 10;
				h_i = 0;
			}else {
				h_heigth+=40;
			}
			g.drawImage(cocdiandian, 80, h_heigth, 20, 30, null);
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			g.setColor(Color.white);
			g.drawString("夜世界配置", 115, h_heigth+25);
			h_heigth+=35;
			String[][] builderHallLevel = data.getBuilderHallLevel();
			for (String[] strings : builderHallLevel) {
				String User_number = strings[1];
				if(!"无".equals(User_number)) {
					BufferedImage coc = ImageIO.read(new File("C:\\cocutil\\matter\\moban\\y"+strings[0]+".png"));
					Graphics2D cocg = (Graphics2D)coc.createGraphics();
					cocg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
					if(User_number.length()>=2) {
						cocg.setFont(new Font("微软雅黑",Font.BOLD,20));
					}else {
						cocg.setFont(new Font("微软雅黑",Font.BOLD,26));
					}
					cocg.setColor(Color.yellow);
					cocg.drawString(User_number+" 个", 153, 62);
					g.drawImage(coc, h_width, h_heigth, 228, 187, null);
					if(h_i==3) {
						h_heigth+=80;
						h_width = 10;
						h_i = -1;
					}else {
						h_width+=170;
					}
					h_i++;
				}
			}
			if(h_width!=10) {
				h_heigth+=120;
				h_width = 10;
				h_i = 0;
			}else {
				h_heigth+=40;
			}
			g.drawImage(cocdiandian, 80, h_heigth, 20, 30, null);
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			g.setColor(Color.white);
			g.drawString("段位配置", 115, h_heigth+25);
			h_heigth+=35;
			String[][] trophies = data.getTrophies();
			for (String[] strings : trophies) {
				String User_number = strings[1];
				if(!"无".equals(User_number)) {
					BufferedImage coc = ImageIO.read(new File("C:\\cocutil\\matter\\moban\\"+strings[0]+".png"));
					Graphics2D cocg = (Graphics2D)coc.createGraphics();
					cocg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
					if(User_number.length()>=2) {
						cocg.setFont(new Font("微软雅黑",Font.BOLD,20));
					}else {
						cocg.setFont(new Font("微软雅黑",Font.BOLD,26));
					}
					cocg.setColor(Color.yellow);
					cocg.drawString(User_number+" 个", 153, 62);
					g.drawImage(coc, h_width, h_heigth, 228, 187, null);
					if(h_i==3) {
						h_heigth+=80;
						h_width = 10;
						h_i = -1;
					}else {
						h_width+=170;
					}
					h_i++;
				}
			}
			if(h_width==10) {
				h_heigth-=80;
			}
			
			
			/** 处理底部图片 **/
			BufferedImage coc_content = ImageIO.read(new File("C:\\cocutil\\matter\\coccontent.png"));//buffer.get("coccontent");
			Graphics2D cg = (Graphics2D)coc_content.createGraphics();
			cg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			cg.setColor(Color.yellow);
			//捐兵最高的三个
			String[][] donations = data.getDonations();
			int donationslength = 125;
			for (int i = donations.length; i > 0; i--) {
				String[] dondate = donations[i-1];//获取玩家信息
				if(NameWidth(dondate[0])>11) {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,28-(NameWidth(dondate[0])/2)));
				}else {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				}
				cg.drawString(dondate[0], 130, donationslength);
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				cg.drawString(dondate[1], 425 - (NameWidth(dondate[1])*8), donationslength);
				cg.drawString(dondate[2], 638 - (NameWidth(dondate[2])*8), donationslength);
				donationslength+=40;
				
			}
			//统计等级最高的一个
			String[] maxexpLevel = data.getMaxexpLevel();
			String maxlevel = maxexpLevel[0];
			
			int nameexpLevellength = NameWidth(maxlevel);
			if(nameexpLevellength>23) {
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,17));
				cg.drawString(maxlevel, 185- (NameWidth(maxlevel)*4), 360);
			}else { 
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				cg.drawString(maxlevel, 194- (NameWidth(maxlevel)*6), 360);
			}
			cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
			cg.drawString(maxexpLevel[1], 425 - (NameWidth(maxexpLevel[1])*8), 360);
			cg.drawString(maxexpLevel[2], 638 - (NameWidth(maxexpLevel[2])*8), 360);
			//最高奖杯
			String[][] maxTrophies = data.getMaxTrophies();
			int maxTrophieslength = 505;
			for (int i = 0; i < maxTrophies.length; i++) {
				String[] max = maxTrophies[i];
				String namelength = max[0];
				int nameWidth = NameWidth(namelength);
				if(nameWidth>=22) {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,14));
					cg.drawString(namelength,  205- (NameWidth(namelength)*3), maxTrophieslength-6);
				}else if(nameWidth<22&&nameWidth>13) {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,17));
					cg.drawString(namelength,  205- (NameWidth(namelength)*4), maxTrophieslength-6);
				}else {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
					cg.drawString(namelength,  195- (NameWidth(namelength)*6), maxTrophieslength);
				}
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				cg.drawString(max[1], 425 - (NameWidth(max[1])*8), maxTrophieslength);
				cg.drawString(max[2], 638 - (NameWidth(max[2])*8), maxTrophieslength);
				maxTrophieslength+=40;
			}
			cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,26));
			//其他文字
			cg.drawString(data.getExpSum(), 240, 605);
			cg.drawString(data.getTownHalSum(), 620, 605);
			cg.drawString(data.getDonationssum().toString(), 240, 675);
			cg.drawString(data.getAttackWinsSum(), 620, 675);
			String loLeader = data.getCoLeader().toString()+" 个";
			String loAdmin = data.getAdmin().toString()+" 个";
			cg.drawString(loLeader,385, 744);
			cg.drawString(loAdmin, 600 , 744);
			cg.drawString(data.getTroSum(), 380 , 817);
			cg.drawString(data.getVersusTrophiesSum(), 615 , 817);
			/** 头部和内容进行合并 */
			headerImage = headerImage.getSubimage(0, 0, headerImage.getWidth(), h_heigth + 100);
			BufferedImage cocImageAll = new BufferedImage(headerImage.getWidth(), headerImage.getHeight()+coc_content.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D cocImageAllG = (Graphics2D)cocImageAll.createGraphics();
			cocImageAllG.drawImage(headerImage,0, 0, headerImage.getWidth()-1, headerImage.getHeight() , null);
			cocImageAllG.drawImage(coc_content,0, headerImage.getHeight(), headerImage.getWidth(), coc_content.getHeight(), null);
			//ImageIO.write(cocImageAll, "png", new File("G:\\酷Q\\酷Q Air\\新建文件夹\\"+TimeUtils.getTimeS()+".png"));
			ImageIO.write(cocImageAll, "png", new File(BaseConfiguration.getCqPath()+"\\data\\image\\image\\clanAll\\ClanAll"+tag));
		} catch (Exception e) {
			e.printStackTrace();
			return "查询部落配置失败，请反馈作者。505";
		}
		return null;
	}
	
	public static String getstart(String tag) {
		try {
			/** 1.数据准备区间  ***/
			//首先是查询部落
			//String sendGetCoc = BackEndHttpRequest.sendGetCoc("https://api.clashofclans.com/v1/clans/%23"+tag);
			String sendGetCoc = BackEndHttpRequest.sendGet("http://47.100.197.180:8855/coctest","tag="+tag);
			if(sendGetCoc.length()<4) {
				return "无法查询到部落信息！";
			}
			JSONObject clans = JSON.parseObject(sendGetCoc);
			//查询部落人数
			Integer members = clans.getInteger("members")==null?0:clans.getInteger("members");
			if(members==null||members<1) {
				return "部落无人，无统计价值，现可直接查询部落信息。";
			}
			//储存玩家全部信息的集合
			List<JSONObject> palyer = new ArrayList<>();
			JSONArray palyerList = clans.getJSONArray("memberList");
			//--- 获取连接池
			Executor threadPool = BaseLocalThreadPool.getThreadPool();
			CountDownLatch count = new CountDownLatch(palyerList.size());//玩家线程阻塞
			for (int i = 0; i < palyerList.size(); i++) {
				JSONObject jsonObject = palyerList.getJSONObject(i);
				threadPool.execute(()->{
					//获取玩家信息保存到 玩家集合当中去
					try {
						palyer.add(JSON.parseObject(BackEndHttpRequest.sendGet("http://47.100.197.180:8855/coc","tag="+jsonObject.getString("tag").substring(1, jsonObject.getString("tag").length()))));
						//palyer.add(JSON.parseObject(BackEndHttpRequest.sendGetCoc("https://api.clashofclans.com/v1/players/%23"+jsonObject.getString("tag").substring(1, jsonObject.getString("tag").length()))));
					} catch (Exception e) {
						System.out.println("获取部落成员失败:"+clans.getString("name")+"    "+clans.getString("tag")+"   玩家："+jsonObject.getString("tag"));
					}
					count.countDown();
				});
			}
			
			CountDownLatch badgeUrlscount = new CountDownLatch(1);//专门为获取部落图片开启的线程 ，最后关闭
			BufferedImage badgeUrlsbufferedImage = cachebuffer.get("badgeUrls"+tag);//
			//--- 获取部落图片
			if(badgeUrlsbufferedImage==null) {
				//这里开线程是为了获取部落图标
				Executor threadPool2 = BaseLocalThreadPool.getThreadPool();
				threadPool2.execute(()->{
					String badgerUrlslarge = clans.getJSONObject("badgeUrls").getString("large");
					if(badgerUrlslarge!=null) {
						try {
							BufferedImage httpsGet = Https.httpsGet(badgerUrlslarge, null);
							cachebuffer.putPlusMinutes("badgeUrls"+tag, httpsGet, 30);//存储30分钟
						} catch (Exception e) {
							System.out.println("获取部落图片失败:"+clans.getString("name")+"    "+clans.getString("tag"));
						}
						cachebuffer.detect();//删除缓存存在过期的值
					}
					badgeUrlscount.countDown();
				});
			}else {
				badgeUrlscount.countDown();//进程直接删除一个，不进行拦截
			}
			
			
			//这里获取玩家三个竞标图片	
			List<BufferedImage> clansIDList = new ArrayList<>();
			JSONArray jsonArray = clans.getJSONArray("labels");
			CountDownLatch clansIDS = new CountDownLatch(jsonArray==null?0:jsonArray.size());
			if(jsonArray!=null&&jsonArray.size()>0) {
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject ClanID = jsonArray.getJSONObject(i);
					Executor threadPool3 = BaseLocalThreadPool.getThreadPool();
					threadPool3.execute(()->{
						String getIDS = ClanID.getString("id");
						BufferedImage bufferedImage = buffer.get("clanid"+getIDS);
						if(bufferedImage==null) {//获取这个ID图片为空则去获取网络图片
							bufferedImage = Https.httpsGet(ClanID.getJSONObject("iconUrls").getString("small"), null);
							try {
								ImageIO.write(bufferedImage, "png", new File("C:\\cocutil\\matter\\clanid"+getIDS+".png"));
								buffer.put("clanid"+getIDS,bufferedImage);
								clansIDList.add(bufferedImage);
								clansIDS.countDown();
							} catch (IOException e) {
								e.printStackTrace();
								System.out.println("获取图片失败");
							}
						}else {
							clansIDList.add(bufferedImage);
							clansIDS.countDown();
						}
						
					});
				}
			}
			
			
			
			// 主线程阻塞，等待计数结束 ---> 获取玩家信息的拦截
			count.await();
			if(members != palyer.size()) {
				return "玩家信息获取失败，请稍后重新获取...";
			}
			/** 2.数据处理区间 **/
			DataProcessing data = dataProcessing(clans,palyer);
			
			
			
			/** 3.图片生成区   **/ 
			
			/** 获取头部图片   **/ 
			BufferedImage headerImage = ImageIO.read(new File("C:\\cocutil\\matter\\startheader.png"));
			Graphics2D g = (Graphics2D)headerImage.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			
			//部落名称
			Font font = new Font("微软雅黑",Font.BOLD,36);
			g.setFont(font);
			g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,1f));
			g.drawString(clans.getString("name"), 180, 140);//左边 宽度 右边高度
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			//主世界总奖杯
			g.setColor(Color.yellow);
			g.drawImage(buffer.get("JBZclan"), 172, 155, 35, 40, null);
			g.drawString(clans.getString("clanPoints"), 210, 184);
			//夜世界总奖杯
			g.drawImage(buffer.get("JBYclan"), 360, 153, 35, 40, null);
			g.drawString(clans.getString("clanVersusPoints"), 400, 181);
			
			//国家 等级 人数 进步罗情况
			StringBuffer ClanAllNumber = new StringBuffer();
			ClanAllNumber.append(CocApiAndCqCustom.location(clans.getJSONObject("location").getString("name"))+" | ");
			ClanAllNumber.append(clans.getString("clanLevel")+"级 | ");
			ClanAllNumber.append(clans.getString("members")+"人 | ");
			ClanAllNumber.append(CocApiAndCqCustom.CocTpe(clans.getString("type")));
			
			g.setFont(new Font("微软雅黑",Font.TYPE1_FONT,22));
			g.setColor(Color.white);
			g.drawString(ClanAllNumber.toString(), 180, 220);
			
			//塞入部落标签
			g.setFont(new Font("微软雅黑",Font.ITALIC,24));
			g.setColor(Color.blue);
			g.drawString(clans.getString("tag"), 200, 265);
			
			badgeUrlscount.await();
			
			if(badgeUrlsbufferedImage==null) {
				BufferedImage newbadgeUrlsbufferedImage = cachebuffer.get("badgeUrls"+tag);
				if(newbadgeUrlsbufferedImage!=null) {//判断网络获取是否成功
					badgeUrlsbufferedImage = newbadgeUrlsbufferedImage;
				}else {//不成功直接获取系统的图片
					badgeUrlsbufferedImage = buffer.get("badgeUrlsClan");
				}
				g.drawImage(badgeUrlsbufferedImage, 55, 125, 120, 120, null);
			}else {
				g.drawImage(badgeUrlsbufferedImage, 55, 125, 120, 120, null);
			}
			//放部落竞赛图片
			clansIDS.await();
			for (int i = 0; i < clansIDList.size(); i++) {
				g.drawImage(clansIDList.get(i), 50+(i*60), 295, 50, 50, null);
			}
			BufferedImage cocdiandian = buffer.get("cocdiandian");
			//处理配置
			int h_heigth = 480;
			int h_width = 80;
			int h_i = 0;
			String[][] townHallLevel = data.getTownHallLevel();
			g.drawImage(cocdiandian, 80, h_heigth, 20, 30, null);
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			g.setColor(Color.white);
			g.drawString("主世界配置", 115, h_heigth+25);
			h_heigth+=80;
			for (String[] strings : townHallLevel) {
				String User_number = strings[1];
				if(!"无".equals(User_number)) {
					String string ="● "+ strings[2]+"：";
					Integer namelength = NameWidth(string);
					g.setFont(new Font("苹方",Font.ITALIC,30));
					g.setColor(Color.WHITE);
					g.drawString( string, h_width, h_heigth);
					g.setColor(Color.yellow);
					g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
					g.drawString(User_number+" 个", h_width+(namelength*13), h_heigth);
					if(h_i==2) {
						h_heigth+=40;
						h_width = 80;
						h_i = -1;
					}else {
						h_width+=210;
					}
					h_i++;
				}
			}
			if(h_width!=80) {
				h_heigth+=40;
				h_width = 80;
				h_i = 0;
			}
			g.drawImage(cocdiandian, 80, h_heigth, 20, 30, null);
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			g.setColor(Color.white);
			g.drawString("夜世界配置", 115, h_heigth+25);
			h_heigth+=75;
			String[][] builderHallLevel = data.getBuilderHallLevel();
			for (String[] strings : builderHallLevel) {
				String User_number = strings[1];
				if(!"无".equals(User_number)) {
					String string ="● "+ strings[0]+"本：";
					Integer namelength = NameWidth(string);
					g.setFont(new Font("苹方",Font.ITALIC,30));
					g.setColor(Color.WHITE);
					g.drawString( string, h_width, h_heigth);
					g.setColor(Color.yellow);
					g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
					g.drawString(User_number+" 个", h_width+(namelength*13), h_heigth);
					if(h_i==2) {
						h_heigth+=40;
						h_width = 80;
						h_i = -1;
					}else {
						h_width+=210;
					}
					h_i++;
				}
			}
			if(h_width!=80) {
				h_heigth+=40;
				h_width = 80;
				h_i = 0;
			}
			g.drawImage(cocdiandian, 80, h_heigth, 20, 30, null);
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
			g.setColor(Color.white);
			g.drawString("段位配置", 115, h_heigth+25);
			h_heigth+=75;
			String[][] trophies = data.getTrophies();
			for (String[] strings : trophies) {
				String User_number = strings[1];
				if(!"无".equals(User_number)) {
					String string ="● "+ strings[0]+"：";
					Integer namelength = NameWidth(string);
					g.setFont(new Font("苹方",Font.ITALIC,30));
					g.setColor(Color.WHITE);
					g.drawString( string, h_width, h_heigth);
					g.setColor(Color.yellow);
					g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,30));
					g.drawString(User_number+" 个", h_width+(namelength*13), h_heigth);
					if(h_i==2) {
						h_heigth+=40;
						h_width = 80;
						h_i = -1;
					}else {
						h_width+=210;
					}
					h_i++;
				}
			}
			if(h_width==80) {
				h_heigth-=40;
			}
			/** 处理底部图片 **/
			BufferedImage coc_content = ImageIO.read(new File("C:\\cocutil\\matter\\startcontent.png"));//buffer.get("coccontent");
			Graphics2D cg = (Graphics2D)coc_content.createGraphics();
			cg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			cg.setColor(Color.yellow);
			//捐兵最高的三个
			String[][] donations = data.getDonations();
			int donationslength = 155;
			for (int i = donations.length; i > 0; i--) {
				String[] dondate = donations[i-1];//获取玩家信息
				if(NameWidth(dondate[0])>11) {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,28-(NameWidth(dondate[0])/2)));
				}else {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				}
				cg.drawString(dondate[0], 115, donationslength);
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				cg.drawString(dondate[1], 415 - (NameWidth(dondate[1])*8), donationslength);
				cg.drawString(dondate[2], 608 - (NameWidth(dondate[2])*8), donationslength);
				donationslength+=40;
				
			}
			//统计等级最高的一个
			String[] maxexpLevel = data.getMaxexpLevel();
			String maxlevel = maxexpLevel[0];
			
			int nameexpLevellength = NameWidth(maxlevel);
			if(nameexpLevellength>23) {
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,17));
				cg.drawString(maxlevel, 145- (NameWidth(maxlevel)*4), 420);
			}else { 
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				cg.drawString(maxlevel, 154- (NameWidth(maxlevel)*6), 420);
			}
			cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
			cg.drawString(maxexpLevel[1], 395 - (NameWidth(maxexpLevel[1])*8), 420);
			cg.drawString(maxexpLevel[2], 598 - (NameWidth(maxexpLevel[2])*8), 420);
			//最高奖杯
			String[][] maxTrophies = data.getMaxTrophies();
			int maxTrophieslength = 583;
			for (int i = 0; i < maxTrophies.length; i++) {
				String[] max = maxTrophies[i];
				String namelength = max[0];
				int nameWidth = NameWidth(namelength);
				if(nameWidth>=22) {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,14));
					cg.drawString(namelength,  160- (NameWidth(namelength)*3), maxTrophieslength-6);
				}else if(nameWidth<22&&nameWidth>13) {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,17));
					cg.drawString(namelength,  160- (NameWidth(namelength)*4), maxTrophieslength-6);
				}else {
					cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
					cg.drawString(namelength,  150- (NameWidth(namelength)*6), maxTrophieslength);
				}
				cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,23));
				cg.drawString(max[1], 395 - (NameWidth(max[1])*8), maxTrophieslength);
				cg.drawString(max[2], 598 - (NameWidth(max[2])*8), maxTrophieslength);
				maxTrophieslength+=40;
			}
			cg.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,26));
			//其他文字
			cg.drawString(data.getExpSum(), 253, 675);
			cg.drawString(data.getTownHalSum(), 610, 671);
			cg.drawString(data.getDonationssum().toString(), 245, 750);
			cg.drawString(data.getAttackWinsSum(), 610, 750);
			cg.drawString(data.getCoLeader().toString(),330-(NameWidth(data.getCoLeader().toString())*6), 834);
			cg.drawString(data.getAdmin().toString(), 540-(NameWidth(data.getAdmin().toString())*6) , 834);
			cg.drawString(data.getTroSum(), 330-(NameWidth(data.getTroSum())*6) , 927);
			cg.drawString(data.getVersusTrophiesSum(), 540-(NameWidth(data.getVersusTrophiesSum())*6) , 927);
			/** 头部和内容进行合并 */
			headerImage = headerImage.getSubimage(0, 0, headerImage.getWidth(), h_heigth+20);
			BufferedImage cocImageAll = new BufferedImage(headerImage.getWidth(), headerImage.getHeight()+coc_content.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D cocImageAllG = (Graphics2D)cocImageAll.createGraphics();
			cocImageAllG.drawImage(headerImage,0, 0, headerImage.getWidth()-1, headerImage.getHeight() , null);
			cocImageAllG.drawImage(coc_content,0, headerImage.getHeight(), headerImage.getWidth(), coc_content.getHeight(), null);
			//ImageIO.write(cocImageAll, "png", new File("G:\\酷Q\\酷Q Air\\新建文件夹\\"+TimeUtils.getTimeS()+".png"));
			ImageIO.write(cocImageAll, "png", new File(BaseConfiguration.getCqPath()+"\\data\\image\\image\\clanAll\\start"+tag));
		} catch (Exception e) {
			e.printStackTrace();
			return "查询部落配置失败，请反馈作者。505";
		}
		return null;
	}
	
	
	
	
	private static int NameWidth(String dondate) {
		int dondatelength = 0;
		try {
			dondatelength = dondate.getBytes("GB2312").length;
		} catch (UnsupportedEncodingException e) {
			dondatelength = dondate.length();
		}
		return dondatelength;
	}
	private static String Namelength(String dondate) {
		int dondatelength = 0;
		try {
			dondatelength = dondate.getBytes("GB2312").length;
		} catch (UnsupportedEncodingException e) {
			dondatelength = dondate.length();
		}
		if(dondatelength>11) {
			return dondate.substring(0, 5)+"…";
		}
		return dondate;
	}
	/****
	 * 数据处理区
	 * @param clans 部落信息
	 * @param palyer 玩家信息
	 */
	private static DataProcessing dataProcessing(JSONObject clans, List<JSONObject> palyer) {
		
		/****
		 * 设置最初的变量
		 */
		//无序的
		Map<String,Integer> maptrophies = new HashMap<String,Integer>();//主世界奖杯分布
		Map<String,Integer> mapbuilderHallLevel = new HashMap<String,Integer>();//主世界奖杯分布
		Map<String,Integer> mapHallLevelMap = new HashMap<String,Integer>();//主世界大本营分布
		String dondate[][] = {{"无","-","0"},{"无","-","0"},{"无","-","0"}};//捐兵前三的用户
		//获取部落捐兵总和         长老        大本营总
		int donationssum = 0,
				coLeader = 0,//副首领
				admin = 0,//长老
				townHallLevelsum = 0,//大本营总
				tro = 0,//现主世界奖杯总
				versusTrophiessum = 0,	//夜世界奖杯数
				expLevelsum = 0,//经验等级
				attackWins= 0;//部落进攻胜利总场次
				;
		String MaxexpLevel[] = {"无","-","0"};//部落等级最高
		
		String maxTrophies[][] = {{"无","-","0"},{"无","-","0"}};//部落等级最高
		/***
		 * 处理数据保存到 DataProcessing 对象里取
		 */
		DataProcessing data = new DataProcessing();
		//循环遍历所有玩家信息
		for (int i = 0; i < palyer.size(); i++) {
			JSONObject play = palyer.get(i);
			//查询各部落 1.杯段 ，然后塞入到 trophies 
			String troph = CocApiAndCqCustom.trophies(play.getString("trophies"));
			Integer trophiessum = maptrophies.get(troph) == null?1:(maptrophies.get(troph)+1);
			maptrophies.put(troph, trophiessum);
			
			//查询各夜世界大本营等级排序情况
			Integer builderHallLevelsum = mapbuilderHallLevel.get(play.getString("builderHallLevel")) == null ? 1:(mapbuilderHallLevel.get(play.getString("builderHallLevel"))+1);
			mapbuilderHallLevel.put(play.getString("builderHallLevel"),builderHallLevelsum);
			
			//查询各主世界大本营等级排序情况
			String town = play.getString("townHallLevel");
			if("12".equals(town)||"13".equals(town)) {
				town = town + play.getString("townHallWeaponLevel");
			}
			mapHallLevelMap.put(town,mapHallLevelMap.get(town)==null?1:(mapHallLevelMap.get(town)+1));
			//处理捐兵前三的用户  + 获取部落捐兵总和
			Integer donations = play.getInteger("donations")==null?0:play.getInteger("donations");
			donationssum +=donations;//获取部落捐兵总和
			Integer don_1 = Integer.valueOf(dondate[0][2]);//取数据库捐兵数--最低捐兵
			Integer don_2 = Integer.valueOf(dondate[1][2]);//取数据库捐兵数--
			Integer don_3 = Integer.valueOf(dondate[2][2]);//取数据库捐兵数--最高捐兵
			if(donations>don_3) {
				dondate[0][0] = dondate[1][0];
				dondate[0][1] = dondate[1][1];
				dondate[0][2] = dondate[1][2];
				dondate[1][0] = dondate[2][0];
				dondate[1][1] = dondate[2][1];
				dondate[1][2] = dondate[2][2];
				dondate[2][0] = play.getString("name");
				dondate[2][1] = play.getString("tag");
				dondate[2][2] = play.getString("donations");
			}else if(donations<don_3&&donations>don_2) {
				dondate[0][0] = dondate[1][0];
				dondate[0][1] = dondate[1][1];
				dondate[0][2] = dondate[1][2];
				dondate[1][0] = play.getString("name");
				dondate[1][1] = play.getString("tag");
				dondate[1][2] = play.getString("donations");
			}else if(donations>don_1&&donations<don_2) {
				dondate[0][0] = play.getString("name");
				dondate[0][1] = play.getString("tag");
				dondate[0][2] = play.getString("donations");
			}
			
			//部落等级最高
			Integer MaxexpLevelIf = Integer.valueOf(MaxexpLevel[2]==null?"0":MaxexpLevel[2]);
			Integer expLevelMaxinteger = play.getInteger("expLevel")==null?0:play.getInteger("expLevel");
			if(MaxexpLevelIf<expLevelMaxinteger) {
				MaxexpLevel[0] = play.getString("name");
				MaxexpLevel[1] = play.getString("tag");
				MaxexpLevel[2] = play.getString("expLevel");
			}
			//获取副首领 长老个数
			String role = play.getString("role");
			if("coLeader".equals(role)) {
				coLeader ++;
			}else if("admin".equals(role)) {
				admin ++;
			}
			//奖杯最高的用户
			Integer trophi = play.getInteger("trophies");
			if(trophi>Integer.valueOf(maxTrophies[0][2])) {
				maxTrophies[0][0] = play.getString("name");
				maxTrophies[0][1] = play.getString("tag");
				maxTrophies[0][2] = play.getString("trophies");
			}
			Integer versusTrop = play.getInteger("versusTrophies");
			if(versusTrop>Integer.valueOf(maxTrophies[1][2])) {
				maxTrophies[1][0] = play.getString("name");
				maxTrophies[1][1] = play.getString("tag");
				maxTrophies[1][2] = play.getString("versusTrophies");
			}
			//统计部落大本营总共多少本
			Integer townHallLevel = play.getInteger("townHallLevel");
			townHallLevelsum +=townHallLevel;
			//现主世界奖杯总
			tro += play.getInteger("trophies");
			//夜世界奖杯数
			versusTrophiessum += play.getInteger("versusTrophies");
			//经验等级
			expLevelsum += play.getInteger("expLevel");
			//进攻胜利场次
			attackWins += play.getInteger("attackWins");
			
		}
		//存入捐兵前三
		data.setDonations(dondate);
		//将Map的值 转储到数组中，变成有序的   夜世界大本排行
		String[][] builderHallLevel = data.getBuilderHallLevel();
		for (int i = 0; i < builderHallLevel.length; i++) {
			Integer variable = mapbuilderHallLevel.get(builderHallLevel[i][0]);
			if(variable!=null&&variable>0) {
				builderHallLevel[i][1] = variable.toString();
			}
		}
		data.setBuilderHallLevel(builderHallLevel);
		//主世界大本排行
		String[][] townHallLevel = data.getTownHallLevel();
		for (int i = 0; i < townHallLevel.length; i++) {
			Integer variable = mapHallLevelMap.get(townHallLevel[i][0]);
			if(variable!=null&&variable>0) {
				townHallLevel[i][1] = variable.toString();
			}
		}
		data.setTownHallLevel(townHallLevel);
		
		//奖杯排行
		String[][] trophies = data.getTrophies();
		for (int i = 0; i < trophies.length; i++) {
			Integer variable = maptrophies.get(trophies[i][0]);
			if(variable!=null&&variable>0) {
				trophies[i][1] = variable.toString();
			}
		}
		int size = palyer.size();
		//计算平均经验等级
		double exp = expLevelsum / size;
		String expSum = doubleString(exp);
		String townHalSum = doubleString((double)townHallLevelsum / size);
		String attackWinsSum = doubleString((double)attackWins / size);
		String troSum = doubleString((double)tro / size);
		String versusTrophiesSum = doubleString((double)versusTrophiessum / size);
		data.setExpSum(expSum);
		data.setTownHalSum(townHalSum);
		data.setAttackWinsSum(attackWinsSum);
		data.setTroSum(troSum);
		data.setVersusTrophiesSum(versusTrophiesSum);
		
		data.setTrophies(trophies);
		data.setMaxexpLevel(MaxexpLevel);
		data.setMaxTrophies(maxTrophies);
		data.setDonationssum(donationssum);//捐兵总和
		data.setCoLeader(coLeader);//副首领
		data.setAdmin(admin);//长老
		
		return data;
	}
	
    public static String doubleString(double number) {
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(number);
    }
}
