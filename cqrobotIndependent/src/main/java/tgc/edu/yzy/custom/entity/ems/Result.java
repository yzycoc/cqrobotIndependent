package tgc.edu.yzy.custom.entity.ems;

import java.util.ArrayList;
import java.util.List;


public class Result {
	private String company;
	private String com;
	private String no;
	private String status;
	private String status_detail;
	private String statusDetail;
	private List<Remark> list = new ArrayList<>();
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCom() {
		return com;
	}
	public void setCom(String com) {
		this.com = com;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatus_detail() {
		return status_detail;
	}
	public void setStatus_detail(String status_detail) {
		this.status_detail = status_detail;
	}
	public List<Remark> getList() {
		return list;
	}
	public void setList(List<Remark> list) {
		this.list = list;
	}
	public String getStatusDetail() {
		return statusDetail;
	}
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}
	
}
