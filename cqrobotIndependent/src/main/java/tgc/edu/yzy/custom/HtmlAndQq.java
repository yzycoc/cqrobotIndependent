package tgc.edu.yzy.custom;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.forte.qqrobot.beans.messages.result.GroupInfo;
import com.forte.qqrobot.beans.messages.result.GroupMemberInfo;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

public class HtmlAndQq {
	/****
	 * @账号
	 * @param msg
	 * @param cqCodeUtil
	 * @param qq
	 * @return
	 */
	public static String athtmltoUser(String msg, CQCodeUtil cqCodeUtil, String qq) {
		
		if(qq!=null) {
			String[] face = getNumber(msg,"\\[@账号");
			if(face!=null) {
				for (int i = 0; i < face.length; i++) {
					msg = msg.replace("[@账号]", cqCodeUtil.getCQCode_At(qq)==null?"":cqCodeUtil.getCQCode_at(qq));
				}
			}
		}
		return msg;
	}
	
	/****
	 * 账号
	 * 	获取用户信息，并且跟换的操作
	 * @param msg
	 * @param cqCodeUtil
	 * @param qq 
	 * @return
	 */
	public static String htmltoUser(String msg, CQCodeUtil cqCodeUtil, String qq) {
		
		if(qq!=null) {
			String[] face = getNumber(msg,"\\[账号");
			if(face!=null) {
				for (int i = 0; i < face.length; i++) {
					msg = msg.replace("[账号]", qq);
				}
			}
			
		}
		return msg;
	}


	/****
	 * 	对时间变量的处理
	 * @param msg
	 * @return
	 */
	public static String htmltoCq(String msg) {
		String[] at = getNumber(msg,"\\[当前时间");
		if(at!=null) {
			for (int i = 0; i < at.length; i++) {
				Date time = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateString = formatter.format(time);
				msg = msg.replace("[当前时间]", dateString);
			}
		}
		
		
		String[] data = getNumber(msg,"\\[获取时间");
		if(data!=null) {
			for (int i = 0; i < data.length; i++) {
				String dateString = "yyyy-MM-dd HH:mm:ss";
				try {
					Date time = new Date();
					SimpleDateFormat formatter = new SimpleDateFormat(data[i]);
					dateString = formatter.format(time);
				}catch (Exception e) {
					// TODO: handle exception
				}
				msg = msg.replace("[获取时间"+data[i]+"]",dateString);
			}
		}
		
		return msg;
	}



	/****
	 * 转换工具
	 * html码转换成cq认可的码
	 * @param msg
	 * @param cqCodeUtil
	 * @return
	 *//*
	public static String getMsghtml(String msg,CQCodeUtil cqCodeUtil,String qq) {
		
		//转换face格式的图片,CQCodeUtil cqCodeUtil
		String[] face = getNumber(msg,"\\[face-");
		for (int i = 0; i < face.length; i++) {
			msg = msg.replace("[face-"+face[i]+"]", cqCodeUtil.getCQCode_face(face[i]));
		}
		
		String[] bface = getNumber(msg, "\\[bface-");
		for (int i = 0; i < bface.length; i++) {
			msg = msg.replace("[bface-"+bface[i]+"]", cqCodeUtil.getCQCode_bface(bface[i]));
		}
		String[] sface = getNumber(msg, "\\[sface-");
		for (int i = 0; i < bface.length; i++) {
			msg = msg.replace("[bface-"+sface[i]+"]", cqCodeUtil.getCQCode_sface(sface[i]));
		}
		String[] image = getNumber(msg, "\\[image-");
		for (int i = 0; i < bface.length; i++) {
			msg = msg.replace("[bface-"+image[i]+"]", cqCodeUtil.getCQCode_image(image[i]));
		}
		String[] record = getNumber(msg, "\\[image-");
		for (int i = 0; i < bface.length; i++) {
			msg = msg.replace("[bface-"+record[i]+"]", cqCodeUtil.getCQCode_record(record[i]));
		}
		String[] at = getNumber(msg, "\\[@-");
		for (int i = 0; i < bface.length; i++) {
			msg = msg.replace("[bface-"+at[i]+"]", cqCodeUtil.getCQCode_at(at[i]));
		}
		
		if(qq!=null) {
			String[] face = getNumber(msg,"\\[@当事人");
			for (int i = 0; i < face.length; i++) {
				msg = msg.replace("[@当事人]", cqCodeUtil.getCQCode_at(qq));
			}
		}
		
		
		String[] at = getNumber(msg,"\\[当前时间");
		for (int i = 0; i < at.length; i++) {
			Date time = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String dateString = formatter.format(time);
			msg = msg.replace("[当前时间]", dateString);
		}
		
		String[] data = getNumber(msg,"\\[获取时间");
		for (int i = 0; i < data.length; i++) {
			String dateString = "yyyy-MM-dd HH:mm:ss";
			try {
				Date time = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat(data[i]);
				dateString = formatter.format(time);
			}catch (Exception e) {
				// TODO: handle exception
			}
			msg = msg.replace("[获取时间"+data[i]+"]",dateString);
		}
		
		
		return msg;
	}*/
	

	
	
	public static String[] getNumber(String msg,String code) {
		String[] split = msg.split(code);
		if(split.length>1) {
			//将所有图片的文件名取出
			String a[] = new String[split.length-1];
			for (int i = 1; i < split.length; i++) {
				a[i-1] = split[i].substring(0, split[i].indexOf("]"));
			}
			return a;
		}
		return null;
	}
	/***
	 * 取陌生人信息
	 * @param msg
	 * @param msgsender
	 * @param string 
	 * @param string2 
	 * @return
	 */
	public static String htmlStrangerInfo(String msg, MsgSender msgsender, String string, String string2) {
		try {
			GroupMemberInfo groupMemberInfo = msgsender.GETTER.getGroupMemberInfo(string2, string);
			if(groupMemberInfo!=null) {
				String[] Code = getNumber(msg, "\\[群号");
				if(Code!=null) {
					for (int i = 0; i < Code.length; i++) {
						msg = msg.replace("[群号]",string2==null?"":string2);
					}
				}
				String[] getName = getNumber(msg, "\\[qq昵称");
				if(getName!=null) {
					for (int i = 0; i < getName.length; i++) {
						msg = msg.replace("[qq昵称]",groupMemberInfo.getName()==null?"":groupMemberInfo.getName());
					}
				}
				
				
			
				
				String[] getCard = getNumber(msg, "\\[群名片");
				if(getCard!=null) {
					for (int i = 0; i < getCard.length; i++) {
						msg = msg.replace("[群名片]",groupMemberInfo.getCard()==null?"":groupMemberInfo.getCard());
					}
				}
				
				
				String[] getSex = getNumber(msg, "\\[性别");
				if(getSex!=null) {
					for (int i = 0; i < getSex.length; i++) {
						msg = msg.replace("[性别]",groupMemberInfo.getSex().TO_STRING==null?"":groupMemberInfo.getSex().TO_STRING);
					}
					
				}
				
				String[] getCity = getNumber(msg, "\\[所在城市");
				if(getCity!=null) {
					for (int i = 0; i < getCity.length; i++) {
						msg = msg.replace("[所在城市]",groupMemberInfo.getCity()==null?"":groupMemberInfo.getCity());
					}
				}
				
				String[] getPowerType = getNumber(msg, "\\[权限类型");
				if(getPowerType!=null) {
					for (int i = 0; i < getPowerType.length; i++) {
						msg = msg.replace("[权限类型]",groupMemberInfo.getPowerType().TO_STRING==null?"":groupMemberInfo.getPowerType().TO_STRING);
					}
				}
				
				String[] getExTitle = getNumber(msg, "\\[专属头衔");
				if(getExTitle!=null) {
					for (int i = 0; i < getExTitle.length; i++) {
						msg = msg.replace("[专属头衔]",groupMemberInfo.getExTitle()==null?"":groupMemberInfo.getExTitle());
					}
				}
				
				String[] getLevelName = getNumber(msg, "\\[群成员等级名称");
				if(getLevelName!=null) {
					for (int i = 0; i < getLevelName.length; i++) {
						msg = msg.replace("[群成员等级名称]",groupMemberInfo.getLevelName()==null?"":groupMemberInfo.getLevelName());
					}
				}
				
				String[] getHeadImgUrl = getNumber(msg, "\\[头像地址");
				if(getHeadImgUrl!=null) {
					for (int i = 0; i < getHeadImgUrl.length; i++) {
						msg = msg.replace("[头像地址]","[CQ:image,file="+groupMemberInfo.getHeadImgUrl()+"]");
					}
				}
				
			}
		} catch (Exception e) {
			return msg;
		}
		
		return msg;
	}
	
	public static void main(String[] args) {
		String sq = "[]asd";
		System.out.println(sq.contains("["));
	}
	/***
	 * 取陌生人信息
	 * @param msg
	 * @param msgsender
	 * @param string 
	 * @param string2 
	 * @return
	 */
	public static String htmlgroup(String msg, MsgSender msgsender, String string, String string2) {
		try {
			if(msg.contains("[")&&msg.contains("]")) {
				GroupMemberInfo groupMemberInfo = msgsender.GETTER.getGroupMemberInfo(string2, string);
				if(groupMemberInfo!=null) {
					String[] Code = getNumber(msg, "\\[群号");
					if(Code!=null) {
						for (int i = 0; i < Code.length; i++) {
							msg = msg.replace("[群号]",string2==null?"":string2);
						}
					}
					String[] getName = getNumber(msg, "\\[qq昵称");
					if(getName!=null) {
						for (int i = 0; i < getName.length; i++) {
							msg = msg.replace("[qq昵称]",groupMemberInfo.getName()==null?"":groupMemberInfo.getName());
						}
					}
					
					
				
					
					String[] getCard = getNumber(msg, "\\[群名片");
					if(getCard!=null) {
						for (int i = 0; i < getCard.length; i++) {
							msg = msg.replace("[群名片]",groupMemberInfo.getCard()==null?"":groupMemberInfo.getCard());
						}
					}
					
					
					String[] getSex = getNumber(msg, "\\[性别");
					if(getSex!=null) {
						for (int i = 0; i < getSex.length; i++) {
							msg = msg.replace("[性别]",groupMemberInfo.getSex().TO_STRING==null?"":groupMemberInfo.getSex().TO_STRING);
						}
						
					}
					
					String[] getCity = getNumber(msg, "\\[所在城市");
					if(getCity!=null) {
						for (int i = 0; i < getCity.length; i++) {
							msg = msg.replace("[所在城市]",groupMemberInfo.getCity()==null?"":groupMemberInfo.getCity());
						}
					}
					
					String[] getPowerType = getNumber(msg, "\\[权限类型");
					if(getPowerType!=null) {
						for (int i = 0; i < getPowerType.length; i++) {
							msg = msg.replace("[权限类型]",groupMemberInfo.getPowerType().TO_STRING==null?"":groupMemberInfo.getPowerType().TO_STRING);
						}
					}
					
					String[] getExTitle = getNumber(msg, "\\[专属头衔");
					if(getExTitle!=null) {
						for (int i = 0; i < getExTitle.length; i++) {
							msg = msg.replace("[专属头衔]",groupMemberInfo.getExTitle()==null?"":groupMemberInfo.getExTitle());
						}
					}
					
					String[] getLevelName = getNumber(msg, "\\[群成员等级名称");
					if(getLevelName!=null) {
						for (int i = 0; i < getLevelName.length; i++) {
							msg = msg.replace("[群成员等级名称]",groupMemberInfo.getLevelName()==null?"":groupMemberInfo.getLevelName());
						}
					}
					
					String[] getHeadImgUrl = getNumber(msg, "\\[头像地址");
					if(getHeadImgUrl!=null) {
						for (int i = 0; i < getHeadImgUrl.length; i++) {
							msg = msg.replace("[头像地址]","[CQ:image,file="+groupMemberInfo.getHeadImgUrl()+"]");
						}
					}
					
				}
			}
			
		} catch (Exception e) {
			return msg;
		}
		
		return msg;
	}
	/****
	 * 获取群人数
	 * @param msg 需要发出去的文字
	 * @param group //群号
	 * @param msgsender 
	 * @return
	 */
	public static String GroupNumber(String msg, String group, MsgSender msgsender) {
		try {
			if(msg.indexOf("群人数")!=-1||msg.indexOf("群成员上限")!=-1||msg.indexOf("本群类型")!=-1) {
				GroupInfo groupInfo = msgsender.GETTER.getGroupInfo(group);
				if(groupInfo.getMaxMember()!=null&&groupInfo.getMaxMember()!=0) {
					String[] getMemberNum = getNumber(msg, "\\[群人数");
					if(getMemberNum!=null) {
						for (int i = 0; i < getMemberNum.length; i++) {
							msg = msg.replace("[群人数]",groupInfo.getMemberNum()==null?"":groupInfo.getMemberNum()+"");
						}
					}
					
					String[] getMaxMember = getNumber(msg, "\\[群成员上限");
					if(getMaxMember!=null) {
						for (int i = 0; i < getMaxMember.length; i++) {
							msg = msg.replace("[群成员上限]",groupInfo.getMaxMember()==null?"":groupInfo.getMaxMember()+"");
						}
					}
					
					String[] getLevel = getNumber(msg, "\\[本群类型");
					if(getLevel!=null) {
						for (int i = 0; i < getLevel.length; i++) {
							msg = msg.replace("[本群类型]",groupInfo.getLevel()==null?"":groupInfo.getLevel()+"");
						}
					}
				}
				
			}
			return msg;
		} catch (Exception e) {
			return msg;
		}
		
	}

	public static String broadcastGroup(String msg, String group, MsgSender msgSender) {
		try {
			if(msg.indexOf("群人数")!=-1||msg.indexOf("群成员上限")!=-1||msg.indexOf("本群类型")!=-1) {
				GroupInfo groupInfo = msgSender.GETTER.getGroupInfo(group);
				if(groupInfo.getMaxMember()!=null&&groupInfo.getMaxMember()!=0) {
					String[] getMemberNum = getNumber(msg, "\\[群人数");
					if(getMemberNum!=null) {
						for (int i = 0; i < getMemberNum.length; i++) {
							msg = msg.replace("[群人数]",groupInfo.getMemberNum()==null?"":groupInfo.getMemberNum()+"");
						}
					}
					
					String[] getMaxMember = getNumber(msg, "\\[群成员上限");
					if(getMaxMember!=null) {
						for (int i = 0; i < getMaxMember.length; i++) {
							msg = msg.replace("[群成员上限]",groupInfo.getMaxMember()==null?"":groupInfo.getMaxMember()+"");
						}
					}
					
					String[] getLevel = getNumber(msg, "\\[本群类型");
					if(getLevel!=null) {
						for (int i = 0; i < getLevel.length; i++) {
							msg = msg.replace("[本群类型]",groupInfo.getLevel()==null?"":groupInfo.getLevel()+"");
						}
					}
					String[] getName = getNumber(msg, "\\[群名称");
					if(getName!=null) {
						for (int i = 0; i < getName.length; i++) {
							msg = msg.replace("[群名称]",groupInfo.getName()==null?"":groupInfo.getName()+"");
						}
					}
					String[] getCode = getNumber(msg, "\\[群号");
					if(getCode!=null) {
						for (int i = 0; i < getCode.length; i++) {
							msg = msg.replace("[群号]",groupInfo.getCode()==null?"":groupInfo.getCode()+"");
						}
					}
					String[] getOwnerQQ = getNumber(msg, "\\[群主");
					if(getOwnerQQ!=null) {
						for (int i = 0; i < getOwnerQQ.length; i++) {
							msg = msg.replace("[群主]",groupInfo.getOwnerQQ()==null?"":groupInfo.getOwnerQQ()+"");
						}
					}
					String[] AtgetOwnerQQ = getNumber(msg, "\\[艾特群主");
					if(AtgetOwnerQQ!=null) {
						for (int i = 0; i < AtgetOwnerQQ.length; i++) {
							msg = msg.replace("[艾特群主]","[CQ:at,qq="+groupInfo.getOwnerQQ()==null?"":groupInfo.getOwnerQQ()+"]");
						}
					}
					
				}
				
			}
			return msg;
		} catch (Exception e) {
			return msg;
		}
	}
}
