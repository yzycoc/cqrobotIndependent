package tgc.edu.yzy.custom;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.forte.qqrobot.beans.messages.msgget.GroupMsg;
import com.forte.qqrobot.beans.messages.msgget.PrivateMsg;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.bean.OpenLayout;
import tgc.edu.yzy.cocApi.custom.ClansClanTagCustom;
import tgc.edu.yzy.cocApi.custom.ClansPlayersTagCustom;
import tgc.edu.yzy.cocApi.custom.CocApiAndCqCustom;
import tgc.edu.yzy.cocApi.custom.ImageClanAll;
import tgc.edu.yzy.cocApi.custom.ImagePlayers;
import tgc.edu.yzy.cocApi.custom.ImageYq;
import tgc.edu.yzy.cocApi.custom.NameClans;
import tgc.edu.yzy.cocApi.custom.QueryPlayersTagCustom;
import tgc.edu.yzy.cocApi.custom.TxtCoc;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.ClansClanTag;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.MemberList;
import tgc.edu.yzy.cocApi.entity.ClansPlayersTag.ClansPlayersTag;
import tgc.edu.yzy.cocApi.entity.QueryPlayers.QueryClan;
import tgc.edu.yzy.cocApi.entity.yuqing.YuQing;
import tgc.edu.yzy.jdbc.CacheMap;
import tgc.edu.yzy.jdbc.XjMysqlJdbc;

public class CocCustom {
	/***
	 * 中心数据数据库缓存
	 */
	public static CacheMap map = new CacheMap();
	
	/****
	 * 查询村庄 ——————图片版本
	 * 
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void ImagePlayersTag(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		boolean restrictH = QqCqcustom.restrict(msg.getQQ());
		if(restrictH) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\n解除时间："+TimeUtils.endMin(15));
			return;
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 请等待");
		ImagePlayers Image= new ImagePlayers();
		String remsg = Image.get(tag);
		if(remsg==null) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/clanAll/player"+tag+".jpg]");
		}else {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+remsg);
		}
	}
	/****
	 * 查询村庄
	 * 
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void PlayersTag(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		boolean restrictH = QqCqcustom.restrict(msg.getQQ());
		if(restrictH) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\n解除时间："+TimeUtils.endMin(15));
			return;
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 请等待");
		ClansPlayersTag findAll = ClansPlayersTagCustom.findAll(tag);
		String gomsg = "";
		if (findAll.getDataisnull()) {
			String image = "";
			if(findAll.getLeague()!=null&&findAll.getLeague().getIconUrls()!=null&&findAll.getLeague().getIconUrls().getSmall()!=null) {
				image = "[CQ:image,file=" + findAll.getLeague().getIconUrls().getSmall() + "]";
			}
			gomsg += UserPlayersGo(findAll, image,cqCodeUtil,msg,tag);
			msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),gomsg);
		}else {
			gomsg += findAll.getError();
			msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ()) + gomsg);
		}
	}
	/***
	 * 查询部落
	 * 
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */  
	public static void ClanTag(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		boolean restrictH = QqCqcustom.restrict(msg.getQQ());
		if(restrictH) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\n解除时间："+TimeUtils.endMin(15));
			return;
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 请等待");
		ClansClanTag findAll = ClansClanTagCustom.findAll(tag);
		String gomsg = " ";
		if (findAll.getDataisnull()) {
			String image = "";
			if (findAll.getBadgeUrls()!=null && findAll.getBadgeUrls().getSmall()!=null &&Integer.parseInt(findAll.getClanLevel())<20) {
				image = "[CQ:image,file=" + findAll.getBadgeUrls().getSmall() + "]";
			}
			gomsg += UserClanGo(findAll);
			msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), image+cqCodeUtil.getCQCode_At(msg.getQQ())+ gomsg);
		} else {
			gomsg += findAll.getError();
			msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ()) + gomsg);
		}
	}



	/***
	 * 普通用户 ---- 村庄
	 *  数据合成
	 * @param findAll
	 * @param image
	 * @param msg2 
	 * @param cqCodeUtil 
	 * @param tag 
	 * @return
	 */
	private static String UserPlayersGo(ClansPlayersTag findAll, String image, CQCodeUtil cqCodeUtil, GroupMsg msg2, String tag) {
		String max = "";
		if (findAll.getTownHallLevel() != null || findAll.getBuilderHallLevel() != null) {
			String getTownHallWeaponLevel = "";
			if (findAll.getTownHallWeaponLevel() != null) {
				getTownHallWeaponLevel = "(" + findAll.getTownHallWeaponLevel() + "星)";
			}
			if (findAll.getTownHallLevel() != null && findAll.getBuilderHallLevel() == null) {

				max = "[CQ:emoji,id=127751]" + findAll.getTownHallLevel() + "本" + getTownHallWeaponLevel + "\n";
			}
			if (findAll.getBuilderHallLevel() != null && findAll.getTownHallLevel() == null) {
				max = " [CQ:emoji,id=127747]" + findAll.getBuilderHallLevel() + "本\n";
			} else {
				max = "[CQ:emoji,id=127751]" + findAll.getTownHallLevel() + "本" + getTownHallWeaponLevel
						+ "  [CQ:emoji,id=127747]" + findAll.getBuilderHallLevel() + "本\n";
			}
		}
		String legendTrophies = "";
		if (findAll.getLegendStatistics() != null) {
			legendTrophies = findAll.getLegendStatistics().getLegendTrophies();
			if (legendTrophies != null) {
				legendTrophies = "传奇[CQ:emoji,id=127941]" + legendTrophies;
			} else {
				legendTrophies = "";
			}
		}
		String gototype = "";
		if (findAll.getGototype() == null) {
			gototype = "";
		} else {
			gototype = "\n" + findAll.getGototype();
		}
		String clanName = "";
		if (findAll.getClan() != null) {
			clanName = findAll.getClan().getName() != null
					? (findAll.getClan().getName() + "[" + findAll.getClan().getClanLevel() + "级]")
					: "无部落";
		} else {
			clanName = "无部落";
		}
		String url = DlCustom.dl("http://yzycoc.com/qq/cocApi/playercoc?tag="+tag);
		String role = findAll.getRole() != null ? findAll.getRole() : "无部落";
		String msg = image +"\n● "+ findAll.getName() + "\n" + "[CQ:emoji,id=127991]" + findAll.getTag() + "\n"
				+ "[CQ:emoji,id=127984]" + clanName + "\n"
				 /*+ "职位："+cocTag+"\n"+ */
				+ max + "[CQ:emoji,id=127993]" + CocApiAndCqCustom.CocRole(role) + "[CQ:face,id=144]"
				+ findAll.getExpLevel() + "级\n" + "[CQ:emoji,id=127942]" + findAll.getTrophies() + legendTrophies + "\n"
				+ "[CQ:emoji,id=9876]" + findAll.getAttackWins() + "场 [CQ:emoji,id=128737]" + findAll.getDefenseWins()
				+ "场" + "\n● 捐:" + findAll.getDonations() + " ● 收:" + findAll.getDonationsReceived() + "\n● 历史最高：\n"
				+ "[CQ:emoji,id=127751][CQ:emoji,id=127942]" + findAll.getBestTrophies()
				+ "[CQ:emoji,id=127747][CQ:emoji,id=127942]" + findAll.getBestVersusTrophies()
				+ "\n● 详细信息\n[CQ:emoji,id=127758] "+url+ gototype;
		return msg;
	}

	/***
	 * 普通用户 --- 部落
	 * 
	 * @param findAll
	 * @return
	 */
	public static String UserClanGo(ClansClanTag list) {
		String msg = "";
		msg = "\n[CQ:emoji,id=9876]" + list.getName() + "\n" + "[CQ:emoji,id=127991]" + list.getTag() + "["
				+ list.getClanLevel() + "级]" + "\n[CQ:emoji,id=128694]现已有" + list.getMembers() + "人\n" + "[CQ:emoji,id=127751][CQ:emoji,id=127942]："
				+ list.getClanPoints() + "\n" + "[CQ:emoji,id=127747][CQ:emoji,id=127942]：" + list.getClanVersusPoints();
		/** 统计COC奖杯情况 **/
		String tro[][]= {{"传奇","0"},{"泰坦","0"},{"冠军","0"},{"大师","0"},{"水晶","0"},{"金杯","0"},{"银杯","0"},{"铜杯","0"},{"无杯","0"}};
		List<MemberList> memberList = list.getMemberList();
		for (MemberList m : memberList) {
			String trophies = CocApiAndCqCustom.trophies(m.getTrophies());
			for (int i = 0; i < tro.length; i++) {
				if(tro[i][0].equals(trophies)) {
					tro[i][1] = Integer.parseInt(tro[i][1]) + 1 + "";
					break;
				}
			}
		}
		String trophie = "";
		for (int i = 0; i < tro.length; i++) {
			if(!tro[i][1].equals("0")) {
				trophie +="\n● "+tro[i][0]+":"+tro[i][1]+"人";
			}
		}
		//接口异常提示
		String gototype = "";
		if (list.getGototype() == null) {
			gototype = "";
		} else {
			gototype = "\n" + list.getGototype();
		}
		String url =  DlCustom.dl("http://yzycoc.com/qq/cocApi/clancoc?tag="+list.getTag().substring(1, list.getTag().length()));
		msg +=trophie+"\n● 详细信息\n[CQ:emoji,id=127758] "+url
				+ gototype;
		
		return msg;
	}
	/***
	 * 查询部落配置
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void QueryClanTag(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		try {
			QueryClan getmembers = QueryPlayersTagCustom.getmembers(tag);
			if(getmembers!=null) {
				//查询到信息，接下来返回结果给用户
				boolean main = CocImageClan.Query(getmembers,tag);
				if (main) {
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=部落配置"+tag+".png]");
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ()) +" 图片合成失败");
				}
			}else {
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ()) +" 无法查询【#"+tag+"】的部落信息");
			}
		} catch (Exception e) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ()) +" 查询异常，通知作者！");
		}
		
	}
	
	public static void ClanAll(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		ImageClanAll Image= new ImageClanAll();
		String string = Image.type(tag,true);
		if(string==null) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/clanAll/ClanAll"+tag+"]");
		}else {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+string);
		}
	}
	/***
	 *  内测部落配置查询
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void ClanAllstart(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		String string = new ImageClanAll().type(tag,false);
		if(string==null) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/clanAll/start"+tag+"]");
		}else {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+string);
		}
	}
	
	public static void ClanAllprivate(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		tag = tag.replaceAll("","");
		if(tag.length()<5||tag.length()>18) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(),"请输入正确的标签。");
			return ;
		}
		String string = new ImageClanAll().type(tag,true);
		if(string==null) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:image,file=image/clanAll/ClanAll"+tag+"]");
		}else {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),string);
		}
	}
	/***
	 *  内测部落配置查询
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void ClanAllstartprivate(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		tag = tag.replaceAll("","");
		if(tag.length()<5||tag.length()>18) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(),"请输入正确的标签。");
			return ;
		}
		
		String string = new ImageClanAll().type(tag,false);//getstart
		if(string==null) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:image,file=image/clanAll/start"+tag+"]");
		}else {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), string);
		}
	}
	///*
	///
	///*       																	私聊一块     *//
	///

	/***
	 * 查询部落
	 * 
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void ClanTag(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		tag = tag.replaceAll("","");
		if(tag.length()<5||tag.length()>18) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(),"请输入正确的标签。");
			return ;
		}
		msgsender.SENDER.sendPrivateMsg(msg.getQQ(),"正在联网查询[#"+tag+"]部落信息，请等待！");
		ClansClanTag findAll = ClansClanTagCustom.findAll(tag);
		String gomsg = " ";
		if (findAll.getDataisnull()) {
			String image = "";
			image = "[CQ:image,file=" + findAll.getBadgeUrls().getSmall() + "]";
			gomsg += UserClanGo(findAll);
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), image+cqCodeUtil.getCQCode_At(msg.getQQ())+ gomsg);
		} else {
			gomsg += findAll.getError();
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), cqCodeUtil.getCQCode_At(msg.getQQ()) + gomsg);
		}
	}
	/****
	 * 查询村庄----------图片
	 * 
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void ImagePlayersTag(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		tag = tag.replaceAll("","");
		if(tag.length()<5||tag.length()>18) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(),"请输入正确的标签。");
			return ;
		}
		msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),"正在联网查询[#"+tag+"]村庄信息，请等待！");
		ImagePlayers Image = new ImagePlayers();
		String remsg = Image.get(tag);
		if(remsg==null) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:image,file=image/clanAll/player"+tag+".jpg]");
		}else {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), remsg);
		}
	}
	/****
	 * 查询村庄
	 * 
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void PlayersTag(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),"正在联网查询[#"+tag+"]村庄信息，请等待！");
		ClansPlayersTag findAll = ClansPlayersTagCustom.findAll(tag);
		String gomsg = "";
		if (findAll.getDataisnull()) {
			String image = "";
			try {
				String small = findAll.getLeague().getIconUrls().getSmall();
				image = "[CQ:image,file=" + small + "]";
			} catch (Exception e) {
			}
			gomsg += UserPlayersGo(findAll, image,cqCodeUtil,null, tag);
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),gomsg);
		}else {
			gomsg += findAll.getError();
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),gomsg);
		}
	}
	/***
	 * 查询部落配置
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void QueryClanTag(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		try {
			QueryClan getmembers = QueryPlayersTagCustom.getmembers(tag);
			if(getmembers!=null) {
				//查询到信息，接下来返回结果给用户
				boolean main = CocImageClan.Query(getmembers,tag);
				if (main) {
					msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),"[CQ:image,file=部落配置"+tag+".png]");
				}else {
					msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), cqCodeUtil.getCQCode_At(msg.getQQ()) +" 图片合成失败");
				}
			}else {
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), cqCodeUtil.getCQCode_At(msg.getQQ()) +" 无法查询【#"+tag+"】的部落信息");
			}
		} catch (Exception e) {
			e.printStackTrace();
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), cqCodeUtil.getCQCode_At(msg.getQQ()) +" 查询异常，无法获取到部落信息，请等待片刻后重新查询，或者审查标签是否准确后重新查询！");
		}
	}
	
	

	public static String OpenLayout(OpenLayout open) {
		int x=1+(int)(Math.random()*2);
		String url = "";
		switch (x) {
		case 1:
			url = oneHtml(open.getUrl());
			break;
		case 2:
			url = twoHtml(open.getUrl());
			break;
		default:
			break;
		}
		String re = "[CQ:face,id=190] 编号："+open.getNumber()+"\n[CQ:emoji,id=127758]"+url+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark();
		return re;
	}

	private static String twoHtml(String url) {
		Map<String,Object> data1 = new HashMap<String,Object>();
		data1.put("qqdrsign", "0149b");
		data1.put("url", url);
		String url2 = url;
		for (int i = 0; i < 15; i++) {
			try {
	        	url2 = BackEndHttpRequest.getURL("http://sa.sogou.com/gettiny", data1, "GET");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        return url2;
	}

	private static String oneHtml(String url) {
		Map<String,Object> data1 = new HashMap<String,Object>();
		data1.put("key", "5d8b0e118e676d74690e5635@227a486b2b57e1f423e7609b9c72d7ee");
		data1.put("expireDate", "2035-03-31");
		data1.put("url", url);
		String url2 = url;
		for (int i = 0; i < 15; i++) {
			try {
	        	url2 = BackEndHttpRequest.getURL("http://suo.im/api.htm", data1, "GET");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        return url2;
	}
	
	/***
	 * 查询联赛统计的
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void leagueinfo(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		boolean restrictc = QqCqcustom.restrict(msg.getQQ());
		if(restrictc) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\n解除时间："+TimeUtils.endMin(15));
			return;
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 正在获取，请等待...");
		String file = map.get("lstj"+tag);
		//判断这张图片的缓存文件是否还可以发送，如果可以，直接发送，如果不可以，网络获取
		if(file==null) {
			Map<String,Object> data = new HashMap<>();
			data.put("method", "leagueinfo");
			data.put("info", "#"+tag);
			data.put("auth", "6lKeYId6QFFWCO8SC3W5U6V0");
			String reUrl[] = HttpSx.getURL("http://107.172.140.107:59547/api", data, "GET");
			if("1".equals(reUrl[0])) {
				//1 请求正常 返回的为图片地址
				String image = HttpSx.getImage(reUrl[1], "lstj");
				if(image==null) {
					//把查询记录存入到数据库中，保存5分钟
					map.save("lstj"+tag, reUrl[1], TimeUtils.endMin(5));
					//移动文件
					TxtCoc.toTxt(reUrl[1],BaseConfiguration.getCqPath(),"lstj");
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/lstj/"+reUrl[1]+"]");
					return ;
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+image);
					return ;
				}
			} else if("-1".equals(reUrl[0])) {
				msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 查询失败，OOS异常。");
				return ;
			}else {
				msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" "+reUrl[1]);
				return ;
			}
		}else {
			System.out.println("调用缓存"+file);
			TxtCoc.toTxt(file,BaseConfiguration.getCqPath(),"lstj");
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/lstj/"+file+"]");
			return ;
		}
	}
	/****
	 * 部落对战
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void Clanwar(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		boolean restrictc = QqCqcustom.restrict(msg.getQQ());
		if(restrictc) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\n解除时间："+TimeUtils.endMin(15));
			return;
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 正在获取，请等待...");
		String file = map.get("bldz"+tag);
		//判断这张图片的缓存文件是否还可以发送，如果可以，直接发送，如果不可以，网络获取
		if(file==null) {
			Map<String,Object> data = new HashMap<>();
			data.put("method", "clanwar");
			data.put("info", "#"+tag);
			data.put("auth", "6lKeYId6QFFWCO8SC3W5U6V0");
			String reUrl[] = HttpSx.getURL("http://107.172.140.107:59547/api", data, "GET");
			if("1".equals(reUrl[0])) {
				//1 请求正常 返回的为图片地址
				String image = HttpSx.getImage(reUrl[1], "bldz");
				if(image==null) {
					//把查询记录存入到数据库中，保存5分钟
					map.save("bldz"+tag, reUrl[1], TimeUtils.endMin(5));
					//移动文件
					TxtCoc.toTxt(reUrl[1],BaseConfiguration.getCqPath(),"bldz");
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/bldz/"+reUrl[1]+"]");
					return ;
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+image);
					return ;
				}
			} else if("-1".equals(reUrl[0])) {
				msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 查询失败，OOS异常。");
				return ;
			}else {
				msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" "+reUrl[1]);
				return ;
			}
		}else {
			System.out.println("调用缓存"+file);
			TxtCoc.toTxt(file,BaseConfiguration.getCqPath(),"bldz");
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/bldz/"+file+"]");
			return ;
		}
	}
	
	/** 联赛场次 **/
	private static final Map<String, String> cc = new HashMap<>();
	static{
		cc.put("1", "1");cc.put("一", "1");cc.put("第一场", "1");cc.put("第1场", "1");
		cc.put("2", "2");cc.put("二", "2");cc.put("第二场", "2");cc.put("第2场", "2");
		cc.put("3", "3");cc.put("三", "3");cc.put("第三场", "3");cc.put("第3场", "3");
		cc.put("4", "4");cc.put("四", "4");cc.put("第四场", "4");cc.put("第4场", "4");
		cc.put("5", "5");cc.put("五", "5");cc.put("第五场", "5");cc.put("第5场", "5");
		cc.put("6", "6");cc.put("六", "6");cc.put("第六场", "6");cc.put("第6场", "6");
		cc.put("7", "7");cc.put("七", "7");cc.put("第七场", "7");cc.put("第7场", "7");
		cc.put("8", "8");cc.put("八", "8");cc.put("第八场", "8");cc.put("第8场", "8");
	}
	/***
	 * 联赛对局统计
	 * @param split
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void goCocUrlLs(String[] split, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		boolean restrict = QqCqcustom.groupPb(msg.getQQ());
		if(restrict) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\n解除时间："+TimeUtils.endMin(15));
			return ;
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 正在获取，请等待...");
		String string = cc.get(split[2]);
		if(string==null) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 指令错误，请输入：\n"+split[0]+"#"+split[1]+"#第X场");
			return ;
		}
		String tag = split[1];
		String file = map.get("lscc"+tag);
		//判断这张图片的缓存文件是否还可以发送，如果可以，直接发送，如果不可以，网络获取
		if(file==null) {
			Map<String,Object> data = new HashMap<>();
			data.put("method", "leaguewar");
			data.put("info", "#"+tag);
			data.put("war", string);
			data.put("auth", "6lKeYId6QFFWCO8SC3W5U6V0");
			String reUrl[] = HttpSx.getURL("http://107.172.140.107:59547/api", data, "GET");
			if("1".equals(reUrl[0])) {
				//1 请求正常 返回的为图片地址
				String image = HttpSx.getImage(reUrl[1], "lscc");
				if(image==null) {
					//把查询记录存入到数据库中，保存5分钟
					map.save("bldz"+tag, reUrl[1], TimeUtils.endMin(5));
					//移动文件
					TxtCoc.toTxt(reUrl[1],BaseConfiguration.getCqPath(),"lscc");
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/lscc/"+reUrl[1]+"]");
					return ;
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+image);
					return ;
				}
			} else if("-1".equals(reUrl[0])) {
				msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" 查询失败，OOS异常。");
				return ;
			}else {
				msgsender.SENDER.sendGroupMsg(msg.getGroup() ,cqCodeUtil.getCQCode_At(msg.getQQ())+" "+reUrl[1]);
				return ;
			}
		}else {
			System.out.println("调用缓存"+file);
			TxtCoc.toTxt(file,BaseConfiguration.getCqPath(),"lscc");
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:image,file=image/lscc/"+file+"]");
			return ;
		}
	}
 /** 私聊 **/
	public static void leagueinfoprivate(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		
		msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 正在获取，请等待...");
		String file = map.get("lstj"+tag);
		//判断这张图片的缓存文件是否还可以发送，如果可以，直接发送，如果不可以，网络获取
		if(file==null) {
			Map<String,Object> data = new HashMap<>();
			data.put("method", "leagueinfo");
			data.put("info", "#"+tag);
			data.put("auth", "6lKeYId6QFFWCO8SC3W5U6V0");
			String reUrl[] = HttpSx.getURL("http://107.172.140.107:59547/api", data, "GET");
			if("1".equals(reUrl[0])) {
				//1 请求正常 返回的为图片地址
				String image = HttpSx.getImage(reUrl[1], "lstj");
				if(image==null) {
					//把查询记录存入到数据库中，保存5分钟
					map.save("lstj"+tag, reUrl[1], TimeUtils.endMin(5));
					//移动文件
					TxtCoc.toTxt(reUrl[1],BaseConfiguration.getCqPath(),"lstj");
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:image,file=image/lstj/"+reUrl[1]+"]");
					return ;
				}else {
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+image);
					return ;
				}
			} else if("-1".equals(reUrl[0])) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 查询失败，OOS异常。");
				return ;
			}else {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" "+reUrl[1]);
				return ;
			}
		}else {
			System.out.println("调用缓存"+file);
			TxtCoc.toTxt(file,BaseConfiguration.getCqPath(),"lstj");
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:image,file=image/lstj/"+file+"]");
			return ;
		}
	}
/***
 * 部落对战
 * @param tag
 * @param msg
 * @param cqCodeUtil
 * @param msgsender
 */
	public static void Clanwarprivate(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 正在获取，请等待...");
		String file = map.get("bldz"+tag);
		//判断这张图片的缓存文件是否还可以发送，如果可以，直接发送，如果不可以，网络获取
		if(file==null) {
			Map<String,Object> data = new HashMap<>();
			data.put("method", "clanwar");
			data.put("info", "#"+tag);
			data.put("auth", "6lKeYId6QFFWCO8SC3W5U6V0");
			String reUrl[] = HttpSx.getURL("http://107.172.140.107:59547/api", data, "GET");
			if("1".equals(reUrl[0])) {
				//1 请求正常 返回的为图片地址
				String image = HttpSx.getImage(reUrl[1], "bldz");
				if(image==null) {
					//把查询记录存入到数据库中，保存5分钟
					map.save("bldz"+tag, reUrl[1], TimeUtils.endMin(5));
					//移动文件
					TxtCoc.toTxt(reUrl[1],BaseConfiguration.getCqPath(),"bldz");
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:image,file=image/bldz/"+reUrl[1]+"]");
					return ;
				}else {
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), cqCodeUtil.getCQCode_At(msg.getQQ())+" "+image);
					return ;
				}
			} else if("-1".equals(reUrl[0])) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 查询失败，OOS异常。");
				return ;
			}else {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" "+reUrl[1]);
				return ;
			}
		}else {
			System.out.println("调用缓存"+file);
			TxtCoc.toTxt(file,BaseConfiguration.getCqPath(),"bldz");
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:image,file=image/bldz/"+file+"]");
			return ;
		}
	}

	public static void goCocUrlLsprivatem(String[] split, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 正在获取，请等待...");
		String string = cc.get(split[2]);
		if(string==null) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 指令错误，请输入：\n"+split[0]+"#"+split[1]+"#第X场");
			return ;
		}
		String tag = split[1];
		String file = map.get("lscc"+tag);
		//判断这张图片的缓存文件是否还可以发送，如果可以，直接发送，如果不可以，网络获取
		if(file==null) {
			Map<String,Object> data = new HashMap<>();
			data.put("method", "leaguewar");
			data.put("info", "#"+tag);
			data.put("war", string);
			data.put("auth", "6lKeYId6QFFWCO8SC3W5U6V0");
			String reUrl[] = HttpSx.getURL("http://107.172.140.107:59547/api", data, "GET");
			if("1".equals(reUrl[0])) {
				//1 请求正常 返回的为图片地址
				String image = HttpSx.getImage(reUrl[1], "lscc");
				if(image==null) {
					//把查询记录存入到数据库中，保存5分钟
					map.save("bldz"+tag, reUrl[1], TimeUtils.endMin(5));
					//移动文件
					TxtCoc.toTxt(reUrl[1],BaseConfiguration.getCqPath(),"lscc");
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:image,file=image/lscc/"+reUrl[1]+"]");
					return ;
				}else {
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" "+image);
					return ;
				}
			} else if("-1".equals(reUrl[0])) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 查询失败，OOS异常。");
				return ;
			}else {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(),cqCodeUtil.getCQCode_At(msg.getQQ())+" "+reUrl[1]);
				return ;
			}
		}else {
			System.out.println("调用缓存"+file);
			TxtCoc.toTxt(file,BaseConfiguration.getCqPath(),"lscc");
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:image,file=image/lscc/"+file+"]");
			return ;
		}
	}
		
	/***
	 * 查询部落信息，并返回信息
	 * 
	 * @param substring  查询的内容
	 * @param tag        查询的标签
	 * @param msg        群监听器
	 * @param cqCodeUtil 特殊代码合成器
	 * @param msgsender
	 * @return
	 */
	public static String goCocUrlsss(String substring, String tag, GroupMsg msg, CQCodeUtil cqCodeUtil,
			MsgSender msgsender) {
		switch (substring) {
		case "查询部落":
			ClanTag(tag, msg, cqCodeUtil, msgsender);
			break;
		case "查询村庄":
			PlayersTag(tag, msg, cqCodeUtil, msgsender);
			break;
		case "村庄":
			ImagePlayersTag(tag, msg, cqCodeUtil, msgsender);
			break;
		case "部落":
			ClanTag(tag, msg, cqCodeUtil, msgsender);
			break;
		case "查询部落配置":
			ClanAll(tag, msg, cqCodeUtil, msgsender);
			break;
		default:
			msgsender.SENDER.sendPrivateMsg("936642284", " 查询出现错误，出现在代码CocCustom 44行，找不对对应属性！");
			break;
		}
		return null;
	}
	/***
	 * 查询部落名 私聊
	 * @param string
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void NameClanTag(String tag, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		NameClans clans = new NameClans();
		String string = clans.get(tag);
		msgsender.SENDER.sendPrivateMsg(msg.getQQ(), string);
	}
	/***
	 * 查询玩家名  私聊
	 * @param string
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void NamePlayers(String string, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		tgc.edu.yzy.cocApi.custom.NamePlayers player  = new tgc.edu.yzy.cocApi.custom.NamePlayers();
		String string2 = player.get(string);
		msgsender.SENDER.sendPrivateMsg(msg.getQQ(), string2);
	}
	/***
	 * 查询玩家名 群发送
	 * @param string
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void NamePlayers(String string, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		tgc.edu.yzy.cocApi.custom.NamePlayers player  = new tgc.edu.yzy.cocApi.custom.NamePlayers();
		String string2 = player.get(string);
		msgsender.SENDER.sendGroupMsg(msg.getGroup(), string2);
	}
	/***
	 * 查询部落名 群发送
	 * @param tag
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	public static void NameClanTag(String tag, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		NameClans clans = new NameClans();
		String string = clans.get(tag);
		msgsender.SENDER.sendGroupMsg(msg.getGroup(), string);
	}
	/***
	 * 获取鱼情
	 * @return 
	 */
	public static boolean getYuqing() {
		String string = ImageClanAll.cache.get("Yuqing");
		if(string!=null) {
			return true;
		}
		try {
			long startTime=System.currentTimeMillis();
			RedisUtil redis = SpringContextUtil.getBean(RedisUtil.class);
			YuQing YQ = (YuQing)redis.get("Yuqing");
			if(YQ!=null) {
				//处理生成图片
				ImageYq ImageYq= new ImageYq();
				ImageYq.getImage(YQ);
				System.out.println("缓存数据生成图片花费"+(System.currentTimeMillis()-startTime)+"ms");
				ImageClanAll.cache.putPlusSeconds("Yuqing", "0", redis.getExpire("Yuqing"));
			}else {
				//获取需要的数据
				Integer sum = 1;
				while (YQ==null&&sum<=3) {
					YQ = getYuQingEntity();
					sum++;
				}
				long currentTimeMillis = System.currentTimeMillis();
				//生成图片
				if(YQ==null) {
					return false;
				}
				ImageYq ImageYq= new ImageYq();
				ImageYq.getImage(YQ);
				System.out.println("数据获取并生成图片花费"+(System.currentTimeMillis()-startTime)+"ms"+"\t获取数据耗时："+(currentTimeMillis-startTime)+"\t生成图片耗时："+(System.currentTimeMillis()-currentTimeMillis));
				ImageClanAll.cache.putPlusMinutes("Yuqing", "0", 3);
				redis.set("Yuqing", YQ, 180);
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	
	
	
	
	
	private static YuQing getYuQingEntity() {
		YuQing y = new YuQing();
		String url = "";
		try {
			Map<String,Object> data = new HashMap<String,Object>();
			url = BackEndHttpRequest.getURL("http://www.clashofclansforecaster.com/STATS.json", data, "GET");
		} catch (Exception e) {
			return null;
		}
		if(url=="") {
			return null;
		}
		JSONObject json = JSON.parseObject(url); 
		JSONObject jsonx = json.getJSONObject("currentLoot");
		Double pf = json.getDouble("lootIndexString");
		Integer sout =pf.intValue();
		String yqjy = new XjMysqlJdbc().getYqjy(String.valueOf(sout));
		y.setYj(yqjy);
		String valueOf = String.valueOf(pf);
		if("10.0".equals(valueOf)) {
			y.setPf("10");//当前鱼情评分
		}else {
			y.setPf(valueOf);
		}
		y.setWordZlp(jsonx.getString("lootMinutes"));//每分钟有效的战利品數
		y.setWordBh(jsonx.getString("lootMinuteChange"));//此刻的净变化:
		
		Integer totalPlayers = jsonx.getInteger("totalPlayers");//总玩家
		Integer playersOnline = jsonx.getInteger("playersOnline");//在线玩家
		Integer playersOnlineChange = jsonx.getInteger("playersOnlineChange");//在线人数变化情况
		
		y.setWordZx(String.valueOf(playersOnline));//在线
		y.setWordZxBh(String.valueOf(playersOnlineChange));//在线变化趋势
		y.setWordLx(String.valueOf(totalPlayers-playersOnline));//离线 
		y.setWordLxBH(String.valueOf(Math.negateExact(playersOnlineChange)));//离线变化趋势
		y.setWordHd(jsonx.getString("shieldedPlayers"));//有护盾
		y.setWordHdBh(jsonx.getString("shieldedPlayersChange"));//有护盾变化趋势
		y.setWordKgj(jsonx.getString("attackablePlayers"));//可攻击
		y.setWordKgjBh(jsonx.getString("attackablePlayersChange"));//可攻击变化趋势
		
		JSONArray object = (JSONArray) json.getJSONArray("regionStats");
		for (int i = 0; i < object.size(); i++) {
			JSONArray jsonArray = object.getJSONArray(i);
			String C_N = jsonArray.getString(1);
			if(C_N.equals("China")) {
				Integer CN_Sum = jsonArray.getInteger(5);//总人数
				Integer Cn_Zx = jsonArray.getInteger(6);//在线玩家
				Integer Cn_Zx_bh = jsonArray.getInteger(7);//在线玩家变化趋势
				y.setCnZlp(jsonArray.getString(8));//每分钟变化趋势
				y.setCnBh(jsonArray.getString(9));//每分钟战利品变化趋势
				y.setCnZx(String.valueOf(Cn_Zx));//中国在线
				y.setCnZxbh(String.valueOf(Cn_Zx_bh));//中国玩家在线变化趋势
				y.setCnLx(String.valueOf(CN_Sum-Cn_Zx));//中国离线
				y.setCnLXBh(String.valueOf(Math.negateExact(Cn_Zx_bh)));//中国离线玩家变化
				y.setCnYhd(jsonArray.getString(10));//中国有护盾
				y.setCnYhdBh(jsonArray.getString(11));//中国有护盾变化趋势
				y.setCnKgj(jsonArray.getString(12));//中国可攻击人数
				y.setCnKgjBh(jsonArray.getString(13));//中国可进攻ATTCKABLE浮动
			}
		}
		//获取情况
		JSONObject jsonObject = json.getJSONObject("forecastMessages");
		String string = jsonObject.getString("chinese-simp");
		String[] toyuqing = CocCustom.toyuqing(string);
		
		y.setTs1(toyuqing[0]);//当前状态提示
		y.setTs2(toyuqing[1]);//提示状态1
		y.setTs3(toyuqing[2]);//提示状态2
		y.setTime(TimeUtils.getStringDate("HH时mm分"));//获取时间
		y.setJy(jsonx.getString(""));//打鱼的建议
		
		return y;
	}
	
	
	/*private static String[] yq = {"",""};
	public static String goyuqing() {
		try {
			Map<String,Object> data = new HashMap<String,Object>();
			String url = "";
			try {
				url = BackEndHttpRequest.getURL("http://www.clashofclansforecaster.com/STATS.json", data, "GET");
			} catch (Exception e) {
				return null;
			}
			if(url=="") {
				return null;
			}
			JSONObject jsonx = JSON.parseObject(url); 
			JSONObject loot = jsonx.getJSONObject("currentLoot");
			Integer totalPlayers = loot.getInteger("totalPlayers");//总玩家
			Integer playersOnline = loot.getInteger("playersOnline");//在线玩家
			Integer integer = loot.getInteger("playersOnlineChange");//在线人数变化情况
			Integer integer2 = loot.getInteger("shieldedPlayers");//有护盾的玩家
			Integer integer3 = loot.getInteger("shieldedPlayersChange");//有护盾的玩家的变化
			Integer integer4 = loot.getInteger("attackablePlayers");//可攻击的玩家
			Integer integer5 = loot.getInteger("attackablePlayersChange");//可攻击玩家的变化
			JSONArray object = (JSONArray) jsonx.getJSONArray("regionStats").get(0);
			Integer integer6 = object.getInteger(6);//中国人数
			Integer integer7 = object.getInteger(7);//中国人数变化
			String format = String.format("%.1f", (float) playersOnline / (float) totalPlayers * 100);
			String format2 = String.format("%.1f", (float) (totalPlayers-playersOnline) / (float) totalPlayers * 100);
			String format3 = String.format("%.1f", (float) integer2 / (float) totalPlayers * 100);
			String format4 = String.format("%.1f", (float) integer4 / (float) totalPlayers * 100);
			String format6 = String.format("%.1f", (float) integer6 / (float) totalPlayers * 100);
			String zx = "\n"+(integer>0?"↑":"↓")+"在    线:"+playersOnline+"("+format+"%)";
			String lx = "\n"+(integer>0?"↓":"↑")+"离    线:"+(totalPlayers-playersOnline)+"("+format2+"%)";
			String hd = "\n"+(integer3>0?"↑":"↓")+"有护盾:"+integer2+"("+format3+"%)";
			String kgj ="\n"+(integer5>0?"↑":"↓")+"可攻击:"+integer4+"("+format4+"%)";
			String zrs ="\n"+(integer7>0?"↑":"↓")+"[CQ:emoji,id=127464][CQ:emoji,id=127475]在线:"+integer6+"("+format6+"%)";
			//获取情况
			JSONObject jsonObject = jsonx.getJSONObject("forecastMessages");
			String string = jsonObject.getString("chinese-simp");
			String toyuqing = CocCustom.toyuqing(string).toString();
			String toubu = "[CQ:emoji,id=127758]COC全球玩家状态";
			String yu  = yq[0];
			String urltm ="";
			if(yu != "") {
				long datadifference = TimeUtils.datadifference(yu);
				if(datadifference < 21600) {
					urltm = yq[1];
				}else {
					urltm = DlCustom.dl("http://www.clashofclansforecaster.com/?lang=chinese-simp");
					yq[0] = TimeUtils.getStringDate();
					yq[1] = urltm;
				}
			}else {
				urltm = DlCustom.dl("http://www.clashofclansforecaster.com/?lang=chinese-simp");
				yq[0] = TimeUtils.getStringDate();
				yq[1] = urltm;
			}
			String tohtml = "\n[CQ:emoji,id=127758]COC预报详情地址\n"+urltm;
			return toubu+zx+lx+hd+kgj+zrs+toyuqing+tohtml;
		} catch (Exception e) {
		}
		return null;
	}*/

	/***
	 * 修改提示信息
	 * @param html
	 * @return
	 */
	private static String[] toyuqing(String html) {
		String[] remsg = {"","",""};
		String[] split = html.split("<b>");
		String regEx="\\D";
		for (int o = 0; o < split.length; o++) {
			String string = split[o];
			if(string.indexOf("</b>")!=-1) {
				String[] split2 = string.split("小时");
				String substring = "";
				if(string.indexOf("分钟")!=-1) {
					for (int i = 0; i < split2.length; i++) {
						Pattern p=Pattern.compile(regEx);
						Matcher m=p.matcher(split2[i]);
						String result=m.replaceAll("").trim();
				        Character ch=result.charAt(0);
				        substring += split2[i].substring(split2[i].indexOf(ch), split2[i].indexOf(ch)+2);
				        if(split2.length==2) {
				        	if(i==0) {
				        		substring +="小时";
				        	}else {
				        		substring +="分钟";
				        	}
				        }
				        if(split2.length==1) {
				        	substring +="分钟";
				        }
					}
				}else {
					substring +="不久之";
				}
				
				String satate = string.substring(0, string.indexOf("</b>"));
				substring = substring.replaceAll(" ", "");
				if(o == 1) {
					remsg[0] = "现在鱼情状态为 "+satate+" ，将持续 "+substring+" 。";
				}else if(o==2) {
					remsg[1] = "预计 "+substring+" 后状态转为 "+satate+"  。";
				}else if(o==3) {
					remsg[2] = "预计 "+substring+" 后状态转为 "+satate+" 。";
				}
			}
		}
		return remsg;
	}
}
