package tgc.edu.yzy.custom.entity.goTulin;

public class SelfInfo {
	private Location location;

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public SelfInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SelfInfo(Location location) {
		super();
		this.location = location;
	}
	
}
