package tgc.edu.yzy.custom;



public class AjaxResult {
	private Boolean success;
	private String msg;
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public AjaxResult(String msg) {
		super();
		this.success=true;
		this.msg = msg;
	}
	
	public AjaxResult(Boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}
	

	
}
