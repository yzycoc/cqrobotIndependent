package tgc.edu.yzy.custom.entity.goTulin;

public class GoTulin {
	private String reqType = "0";
	private Perception perception;
	private UserInfo userInfo;
	public String getReqType() {
		return reqType;
	}
	public void setReqType(String reqType) {
		this.reqType = reqType;
	}
	public Perception getPerception() {
		return perception;
	}
	public void setPerception(Perception perception) {
		this.perception = perception;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
	@Override
	public String toString() {
		return "GoTulin [reqType=" + reqType + ", perception=" + perception + ", userInfo=" + userInfo
				+ ", getReqType()=" + getReqType() + ", getPerception()=" + getPerception() + ", getUserInfo()="
				+ getUserInfo() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
}
