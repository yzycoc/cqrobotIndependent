package tgc.edu.yzy.custom.entity.coc;

public class Bests {
	private ClanPoints clanPoints;
	private WlanVersusPoints clanVersusPoints;
	private WarWinStreak warWinStreak;
	public ClanPoints getClanPoints() {
		return clanPoints;
	}
	public void setClanPoints(ClanPoints clanPoints) {
		this.clanPoints = clanPoints;
	}
	public WlanVersusPoints getClanVersusPoints() {
		return clanVersusPoints;
	}
	public void setClanVersusPoints(WlanVersusPoints clanVersusPoints) {
		this.clanVersusPoints = clanVersusPoints;
	}
	public WarWinStreak getWarWinStreak() {
		return warWinStreak;
	}
	public void setWarWinStreak(WarWinStreak warWinStreak) {
		this.warWinStreak = warWinStreak;
	}
	
}
