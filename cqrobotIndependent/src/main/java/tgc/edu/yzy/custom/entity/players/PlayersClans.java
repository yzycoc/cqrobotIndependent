package tgc.edu.yzy.custom.entity.players;

import java.util.ArrayList;
import java.util.List;

import tgc.edu.yzy.custom.entity.coc.Info;

public class PlayersClans {
	private String tag;
	private String name;
	private String characterId;
	private String townHallLevel;
	private String townHallWeaponLevel;
	private String builderHallLevel;
	private String expLevel;
	private String trophies;
	private String leagueId;
	private String role;//权限
	private String donations;
	private String donationsReceived;
	private String versusTrophies;
	private String attackWins;
	private String defenseWins;
	private String lastUpdatedFromPlayer;
	private String versusBattleWins;
	private String warStars;
	private String rank;
	private String previousRank;
	private String townhallarmonlevel;
	private String legendTrophies;
	private String playerHistoryStartDate;
	private String bestTrophies;
	private String bestVersusTrophies;
	private Info info;
	private List<Achievements> achievements = new ArrayList<>();
	private List<Troops> troops =new ArrayList<>(); 
	private List<Spells> spells =new ArrayList<>(); 
	private List<Heroes> heroes =new ArrayList<>(); 
	private String dateAdded;
	private Clan clan;
	private BestDonations bestDonations;
	//失败
	private String statusCode;
	private String error;
	private String message;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCharacterId() {
		return characterId;
	}
	public void setCharacterId(String characterId) {
		this.characterId = characterId;
	}
	public String getTownHallLevel() {
		return townHallLevel;
	}
	public void setTownHallLevel(String townHallLevel) {
		this.townHallLevel = townHallLevel;
	}
	public String getTownHallWeaponLevel() {
		return townHallWeaponLevel;
	}
	public void setTownHallWeaponLevel(String townHallWeaponLevel) {
		this.townHallWeaponLevel = townHallWeaponLevel;
	}
	public String getBuilderHallLevel() {
		return builderHallLevel;
	}
	public void setBuilderHallLevel(String builderHallLevel) {
		this.builderHallLevel = builderHallLevel;
	}
	public String getExpLevel() {
		return expLevel;
	}
	public void setExpLevel(String expLevel) {
		this.expLevel = expLevel;
	}
	public String getTrophies() {
		return trophies;
	}
	public void setTrophies(String trophies) {
		this.trophies = trophies;
	}
	public String getLeagueId() {
		return leagueId;
	}
	public void setLeagueId(String leagueId) {
		this.leagueId = leagueId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDonations() {
		return donations;
	}
	public void setDonations(String donations) {
		this.donations = donations;
	}
	public String getDonationsReceived() {
		return donationsReceived;
	}
	public void setDonationsReceived(String donationsReceived) {
		this.donationsReceived = donationsReceived;
	}
	public String getVersusTrophies() {
		return versusTrophies;
	}
	public void setVersusTrophies(String versusTrophies) {
		this.versusTrophies = versusTrophies;
	}
	public String getAttackWins() {
		return attackWins;
	}
	public void setAttackWins(String attackWins) {
		this.attackWins = attackWins;
	}
	public String getDefenseWins() {
		return defenseWins;
	}
	public void setDefenseWins(String defenseWins) {
		this.defenseWins = defenseWins;
	}
	public String getLastUpdatedFromPlayer() {
		return lastUpdatedFromPlayer;
	}
	public void setLastUpdatedFromPlayer(String lastUpdatedFromPlayer) {
		this.lastUpdatedFromPlayer = lastUpdatedFromPlayer;
	}
	public String getVersusBattleWins() {
		return versusBattleWins;
	}
	public void setVersusBattleWins(String versusBattleWins) {
		this.versusBattleWins = versusBattleWins;
	}
	public String getWarStars() {
		return warStars;
	}
	public void setWarStars(String warStars) {
		this.warStars = warStars;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getPreviousRank() {
		return previousRank;
	}
	public void setPreviousRank(String previousRank) {
		this.previousRank = previousRank;
	}
	public String getTownhallarmonlevel() {
		return townhallarmonlevel;
	}
	public void setTownhallarmonlevel(String townhallarmonlevel) {
		this.townhallarmonlevel = townhallarmonlevel;
	}
	public String getLegendTrophies() {
		return legendTrophies;
	}
	public void setLegendTrophies(String legendTrophies) {
		this.legendTrophies = legendTrophies;
	}
	public String getPlayerHistoryStartDate() {
		return playerHistoryStartDate;
	}
	public void setPlayerHistoryStartDate(String playerHistoryStartDate) {
		this.playerHistoryStartDate = playerHistoryStartDate;
	}
	public String getBestTrophies() {
		return bestTrophies;
	}
	public void setBestTrophies(String bestTrophies) {
		this.bestTrophies = bestTrophies;
	}
	public String getBestVersusTrophies() {
		return bestVersusTrophies;
	}
	public void setBestVersusTrophies(String bestVersusTrophies) {
		this.bestVersusTrophies = bestVersusTrophies;
	}
	
	public Info getInfo() {
		return info;
	}
	public void setInfo(Info info) {
		this.info = info;
	}
	public List<Achievements> getAchievements() {
		return achievements;
	}
	public void setAchievements(List<Achievements> achievements) {
		this.achievements = achievements;
	}
	public List<Troops> getTroops() {
		return troops;
	}
	public void setTroops(List<Troops> troops) {
		this.troops = troops;
	}
	public List<Spells> getSpells() {
		return spells;
	}
	public void setSpells(List<Spells> spells) {
		this.spells = spells;
	}
	public List<Heroes> getHeroes() {
		return heroes;
	}
	public void setHeroes(List<Heroes> heroes) {
		this.heroes = heroes;
	}
	public String getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(String dateAdded) {
		this.dateAdded = dateAdded;
	}
	public Clan getClan() {
		return clan;
	}
	public void setClan(Clan clan) {
		this.clan = clan;
	}
	public BestDonations getBestDonations() {
		return bestDonations;
	}
	public void setBestDonations(BestDonations bestDonations) {
		this.bestDonations = bestDonations;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
