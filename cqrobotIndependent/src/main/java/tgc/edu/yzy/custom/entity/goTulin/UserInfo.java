package tgc.edu.yzy.custom.entity.goTulin;

public class UserInfo {
	private String apiKey;
	private String userId;
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public UserInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserInfo(String apiKey, String userId) {
		super();
		this.apiKey = apiKey;
		this.userId = userId;
	}
	
}
