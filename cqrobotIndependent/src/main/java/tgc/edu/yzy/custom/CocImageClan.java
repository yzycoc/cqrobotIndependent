package tgc.edu.yzy.custom;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import tgc.edu.yzy.cocApi.custom.CocApiAndCqCustom;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.ClansClanTag;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.MemberList;
import tgc.edu.yzy.cocApi.entity.QueryPlayers.QueryClan;
import tgc.edu.yzy.cocApi.entity.QueryPlayers.QueryPlayers;

public class CocImageClan {

	public static boolean main(ClansClanTag clan) {
		//获取高度
		Integer[] a =widthAndheight(clan);
		BufferedImage image = new BufferedImage(a[0], a[1], BufferedImage.TYPE_INT_RGB);
		
		Graphics g = image.createGraphics();
		//BufferedImage image = TxtCoc.getImage("Legend Leagu1e", "https://api-assets.clashofclans.com/leagues/72/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png");
		//BufferedImage image = TxtCoc.getImage(clan.getBadgeUrls().getMedium(), clan.getBadgeUrls().getMedium());
		Graphics2D dg = (Graphics2D) image.createGraphics();
		dg.setColor(Color.WHITE);//设置笔刷白色
		dg.fillRect(0,0,a[0], a[1]);//填充整个屏幕  
		g.setColor(Color.black);
		g.drawString("部落名：   "+clan.getName(), 0, 20);
		g.drawString("部落标签：   "+clan.getTag(), 400, 20);
		g.drawString("部落加入方式：   "+CocApiAndCqCustom.CocTpe(clan.getType()), 0, 40);
		g.drawString("部落等级：   "+clan.getClanLevel(), 400, 40);
		g.drawString("部落人数：   "+clan.getMembers(), 0, 60);
		g.drawString("战争频率：   "+CocApiAndCqCustom.CocWarFrequency(clan.getWarFrequency()), 400, 60);
		g.drawString("部落积分：   "+clan.getClanPoints(), 0, 80); 
		g.drawString("建筑大师积分：   "+clan.getClanVersusPoints(), 400, 80);
		List<MemberList> memberList = clan.getMemberList();
		int w = 120;
		for (MemberList m : memberList) {
			g.drawString("奖杯 "+m.getTrophies()
					+ "  职位 " + CocApiAndCqCustom.CocRole(m.getRole())
					+ "  名称 " + m.getName()
					+ "  等级 "+m.getExpLevel()
					+ "  捐兵 "+m.getDonations()
					+ "  收兵 "+m.getDonationsReceived()
					+ "  夜世界奖杯 "+m.getVersusTrophies()
					+ "  部落排名 "+m.getClanRank()
					, 0, w);
			w = w +20;
		}
		Graphics g2 = image.createGraphics();
		g2.setColor(Color.red);
		g2.drawString("详细信息已通过打包到网站私聊发送给您！如需接收，请添加为好友！", 0, a[1] -30);
		try {
			//ImageIO.write(image, "png", new File(TimingOnes.getCqrobot()+"部落"+clan.getTag()+".png"));
			ImageIO.write(image, "png", new File(BaseConfiguration.getCqPath()+"\\data\\image\\部落"+clan.getTag()+".png"));
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	/***
	 * 获取图片高度
	 * @param clan
	 * @return a[0] 高度 a[1]宽度
	 */
	private static Integer[] widthAndheight(ClansClanTag clan) {
		Integer a[] = new Integer[2];
		a[0] = 800;
		int size = clan.getMemberList().size();
		
		a[1] = 160 + (size*20);
		return a;
	}

	public static boolean Query(QueryClan getmembers, String tag) {
		Integer[] a =widthAndheightQuery(getmembers.getQuery());
		//首先是获取12个大本营
		BufferedImage b3 = null;
		try {
			b3 = ImageIO.read(new File("C:\\cocutil\\plany.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		BufferedImage image = new BufferedImage(a[0], a[1], BufferedImage.TYPE_INT_RGB);
		Graphics2D g = (Graphics2D)image.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
		g.setColor(Color.WHITE);//设置笔刷白色
		g.fillRect(0,0,a[0], a[1]);//填充整个屏幕  
		g.drawImage(b3, 0, 100, 349, 448, null);
		g.setColor(Color.blue);
		Font font = new Font("微软雅黑",Font.CENTER_BASELINE,15);
		g.setFont(font);
		//处理每个大本营个数
		int z3 = 0;
		int z4 = 0;
		int z5 = 0;
		int z6 = 0;
		int z7 = 0;
		int z8 = 0;
		int z9 = 0;
		int z10 = 0;
		int z11 = 0;
		int z12 = 0;
		int z122 = 0;
		int z123 = 0;
		int z124 = 0;
		int z125 = 0;
		int e13 = 0;
		int y2 = 0;
		int y3 = 0;
		int y4 = 0;
		int y5 = 0;
		int y6 = 0;
		int y7 = 0;
		int y8 = 0;
		int y9 = 0;
		List<QueryPlayers> query = getmembers.getQuery();
		for (QueryPlayers q : query) {
			switch (q.getTownHallLevel()) {
			case 13:
				e13++;
				break;
			case 12:
				switch (q.getTownHallWeaponLevel()) {
				case 1:
					z12 ++;
					break;
				case 2:
					z122 ++;
					break;
				case 3:
					z123 ++;
					break;
				case 4:
					z124 ++;
					break;
				case 5:
					z125 ++;
					break;
				default:
					break;
				}
				break;
			case 11:
				z11 ++;
				break;
			case 10:
				z10 ++;
				break;
			case 9:
				z9 ++;
				break;
			case 8:
				z8 ++;
				break;
			case 7:
				z7 ++;
				break;
			case 6:
				z6 ++;
				break;
			case 5:
				z5 ++;
				break;
			case 4:
				z4 ++;
				break;
			case 3:
				z3 ++;
				break;
			default:
				break;
			}
			Integer versusTrophies = q.getVersusTrophies()==null?2:q.getVersusTrophies();
			switch (versusTrophies) {
			case 2:
				y2++;
				break;
			case 3:
				y3++;
				break;
			case 4:
				y4++;
				break;
			case 5:
				y5++;
				break;
			case 6:
				y6++;
				break;
			case 7:
				y7++;
				break;
			case 8:
				y8++;
				break;
			case 9:
				y9++;
				break;
			default:
				break;
			}
		}
		
		g.drawString(getmembers.getName() +"   ["+getmembers.getTag()+"]", 30, 28);
		g.drawString(getmembers.getLocationname(), 30, 49);
		String is = getmembers.getIsWarLogPublic()==false?"未公开":"公开";
		g.drawString("部落战 "+is, 200, 49);
		g.drawString("部落大本营分布情况( "+getmembers.getNumber()+" 人)", 30, 70);
		g.setColor(Color.black);
		g.drawString("13本"+e13+"个", 30, 110);
		g.setColor(Color.yellow);
		g.drawString("新模板正在制作中，请耐心等待更新！", 30, 93);
		g.setColor(Color.black);
		font = new Font("Stencil Std",Font.BOLD,27);
		g.setFont(font);
		//12本5星
		
		
		g.drawString(z125+"", z125>9?33:38, 189);
		
		g.drawString(z124+"", z124>9?98:103, 189);
		
		g.drawString(z123+"",z123>9?167:172, 189);
		
		g.drawString(z122+"", z122>9?230:235, 189);
		
		g.drawString(z12+"", z12>9?293:298, 189);
		

		g.drawString(z11+"", z11>9?33:38, 269);
		
		g.drawString(z10+"", z10>9?98:103, 269);
		
		g.drawString(z9+"", z9>9?168:173, 269);
		
		g.drawString(z8+"", z9>8?230:235, 269);
		
		g.drawString(z7+"", z9>7?293:298, 269);
		
		g.drawString(z6+"", z9>6?33:38, 352);
		
		g.drawString(z5+"", z9>5?108:113, 352);
		
		g.drawString(z4+"", z9>4?173:178, 352);
		
		g.drawString(z3+"", z9>3?250:255, 352);
		
		
		
		g.drawString(y9+"", y9>9?33:38, 457);
		g.drawString(y8+"", y8>9?108:113, 457);
		g.drawString(y7+"", y7>9?173:178, 457);
		g.drawString(y6+"", y6>9?249:254, 457);
		
		g.drawString(y5+"", y5>9?33:38, 537);
		g.drawString(y4+"", y4>9?109:113, 537);
		g.drawString(y3+"", y3>9?173:178, 537);
		g.drawString(y2+"", y2>9?249:254, 537);
		/*g.setColor(Color.black);
		Font font = new Font("楷体",Font.BOLD,30);
		g.setFont(font);
		g.drawString("#"+tag+"部落的大本营分布情况("+getmembers.size()+"人)", 10, 50);
		
		//这里查询出对应的大本营的个数
        Map<String,Integer> map = new HashMap<>();
		for (QueryPlayers q : getmembers) {
			Integer i = 1; //定义一个计数器，用来记录重复数据的个数
            if(map.get(q.getTownHallLevel()+"本") != null){
                i=map.get(q.getTownHallLevel()+"本")+1;
            }
            map.put(q.getTownHallLevel()+"本",i);
		}
		Integer h  = 100;*/
		
		try {
			//ImageIO.write(image, "png", new File(TimingOnes.getCqrobot()+"部落"+clan.getTag()+".png"));
			ImageIO.write(image, "png", new File(BaseConfiguration.getCqPath()+"\\data\\image\\部落配置"+tag+".png"));
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	/**'
	 * 获取图片高度 宽度
	 * @param getmembers
	 * @return
	 */
	private static Integer[] widthAndheightQuery(List<QueryPlayers> getmembers) {
		Integer a[] = new Integer[2];
		a[0] = 349;
		//int size = getmembers.size();
		
		a[1] = 580;
		return a;
	}

}
