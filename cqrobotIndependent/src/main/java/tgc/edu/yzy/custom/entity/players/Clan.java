package tgc.edu.yzy.custom.entity.players;

public class Clan {
	private String tag;
	private String name;
	private String badge;
	private String role;
	private String rank;
	private String previousRank;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBadge() {
		return badge;
	}
	public void setBadge(String badge) {
		this.badge = badge;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getPreviousRank() {
		return previousRank;
	}
	public void setPreviousRank(String previousRank) {
		this.previousRank = previousRank;
	}
	
}
