package tgc.edu.yzy.custom;

import com.forte.qqrobot.beans.messages.msgget.GroupMemberIncrease;
import com.forte.qqrobot.beans.messages.msgget.GroupMemberReduce;
import com.forte.qqrobot.beans.messages.msgget.GroupMsg;
import com.forte.qqrobot.beans.messages.msgget.PrivateMsg;
import com.forte.qqrobot.beans.messages.result.GroupInfo;
import com.forte.qqrobot.beans.types.CQCodeTypes;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

public class GroupHtmlAndQqService {
	/***
	 * 应答
	 * @param response
	 * @param cqCodeUtil
	 * @param msg
	 * @return
	 */
	public static String getMsghtml(String response, CQCodeUtil cqCodeUtil, GroupMsg msg,MsgSender msgsender) {
		response = HtmlAndQq.htmltoCq(response);
		response =  HtmlAndQq.htmltoUser(response,cqCodeUtil,msg.getQQ());
		response =  HtmlAndQq.athtmltoUser(response,cqCodeUtil,msg.getQQ());
		response = HtmlAndQq.htmlgroup(response,msgsender,msg.getQQ(),msg.getGroup());
		response = HtmlAndQq.GroupNumber(response,msg.getGroup(),msgsender);
		response = imageAll(response,cqCodeUtil,msg.getGroup());
		return response;
	}
	/***
	 * 退群
	 * @param inputs
	 * @param cqCodeUtil
	 * @param msg
	 * @return
	 */
	public static String groupMemberReduce(String inputs, CQCodeUtil cqCodeUtil, GroupMemberReduce msg,MsgSender msgsender) {
		//对时间变量的处理
		inputs =  HtmlAndQq.htmltoCq(inputs);
		inputs =  HtmlAndQq.htmltoUser(inputs,cqCodeUtil,msg.getBeOperatedQQ());
		inputs = HtmlAndQq.GroupNumber(inputs,msg.getGroup(),msgsender);
		inputs = imageAll(inputs,cqCodeUtil,msg.getGroup());
		return inputs;
	}
	/***
	 * 	进群欢迎语
	 * @param msg2 
	 * @param cqCodeUtil 
	 * @return
	 */
	public static String msggotograde(String msg, CQCodeUtil cqCodeUtil, GroupMemberIncrease msg2,MsgSender msgsender) {
		//对时间变量的处理
		msg =  HtmlAndQq.htmltoCq(msg);
		//艾特用户
		msg =  HtmlAndQq.athtmltoUser(msg,cqCodeUtil,msg2.getBeOperatedQQ());
		//获取用户的name
		msg =  HtmlAndQq.htmltoUser(msg,cqCodeUtil,msg2.getBeOperatedQQ());
		
		msg = HtmlAndQq.htmlgroup(msg,msgsender,msg2.getBeOperatedQQ(),msg2.getGroup());
		msg = HtmlAndQq.GroupNumber(msg,msg2.getGroup(),msgsender);
		msg = imageAll(msg,cqCodeUtil,msg2.getGroup());
		return msg;
	}
	/***
	 * 	进群修改群名片
	 * @param msg2 
	 * @param cqCodeUtil 
	 * @return
	 */
	public static String mingpian(String msg, CQCodeUtil cqCodeUtil, GroupMemberIncrease msg2,MsgSender msgsender) {
		//对时间变量的处理
		msg =  HtmlAndQq.htmltoCq(msg);
		msg = HtmlAndQq.htmlStrangerInfo(msg,msgsender,msg2.getBeOperatedQQ(),msg2.getGroup());
		//获取用户的name
		msg =  HtmlAndQq.htmltoUser(msg,cqCodeUtil,msg2.getBeOperatedQQ());
		msg = HtmlAndQq.GroupNumber(msg,msg2.getGroup(),msgsender);
		return msg;
	}
	/****
	 * 非会员用户，取消其发送的权力
	 * @return
	 */
	public static String imageAll(String msg,CQCodeUtil cqCodeUtil,String username) {
		
		return msg;
	}
	public static String getMsghtml(String response, CQCodeUtil cqCodeUtil, PrivateMsg msg, MsgSender msgsender) {
		response = HtmlAndQq.htmltoCq(response);
		response =  HtmlAndQq.htmltoUser(response,cqCodeUtil,msg.getQQ());
		response =  HtmlAndQq.athtmltoUser(response,cqCodeUtil,msg.getQQ());
		response = htmlgroup(response,msgsender,msg.getQQ());
		response = GroupNumber(response);
		return response;
	}
	
	private static String GroupNumber(String msg) {
		String[] getMemberNum = getNumber(msg, "\\[群人数");
		if(getMemberNum!=null) {
			for (int i = 0; i < getMemberNum.length; i++) {
				msg = msg.replace("[群人数]","");
			}
		}
		
		String[] getMaxMember = getNumber(msg, "\\[群成员上限");
		if(getMaxMember!=null) {
			for (int i = 0; i < getMaxMember.length; i++) {
				msg = msg.replace("[群成员上限]","");
			}
		}
		
		String[] getLevel = getNumber(msg, "\\[本群类型");
		if(getLevel!=null) {
			for (int i = 0; i < getLevel.length; i++) {
				msg = msg.replace("[本群类型]","");
			}
		}
		return msg;
	}	
	public static String htmlgroup(String msg, MsgSender msgsender, String string2) {
		String[] Code = getNumber(msg, "\\[群号");
		if(Code!=null) {
			for (int i = 0; i < Code.length; i++) {
				msg = msg.replace("[群号]",string2==null?"":string2);
			}
		}
		String[] getName = getNumber(msg, "\\[qq昵称");
		if(getName!=null) {
			for (int i = 0; i < getName.length; i++) {
				msg = msg.replace("[qq昵称]","");
			}
		}
		String[] getCard = getNumber(msg, "\\[群名片");
		if(getCard!=null) {
			for (int i = 0; i < getCard.length; i++) {
				msg = msg.replace("[群名片]","");
			}
		}
		String[] getSex = getNumber(msg, "\\[性别");
		if(getSex!=null) {
			for (int i = 0; i < getSex.length; i++) {
				msg = msg.replace("[性别]","");
			}
			
		}
		
		String[] getCity = getNumber(msg, "\\[所在城市");
		if(getCity!=null) {
			for (int i = 0; i < getCity.length; i++) {
				msg = msg.replace("[所在城市]","");
			}
		}
		
		String[] getPowerType = getNumber(msg, "\\[权限类型");
		if(getPowerType!=null) {
			for (int i = 0; i < getPowerType.length; i++) {
				msg = msg.replace("[权限类型]","");
			}
		}
		
		String[] getExTitle = getNumber(msg, "\\[专属头衔");
		if(getExTitle!=null) {
			for (int i = 0; i < getExTitle.length; i++) {
				msg = msg.replace("[专属头衔]","");
			}
		}
		
		String[] getLevelName = getNumber(msg, "\\[群成员等级名称");
		if(getLevelName!=null) {
			for (int i = 0; i < getLevelName.length; i++) {
				msg = msg.replace("[群成员等级名称]","");
			}
		}
		
		String[] getHeadImgUrl = getNumber(msg, "\\[头像地址");
		if(getHeadImgUrl!=null) {
			for (int i = 0; i < getHeadImgUrl.length; i++) {
				msg = msg.replace("[头像地址]","");
			}
		}
		return msg;
	}
	private static String[] getNumber(String msg, String string) {
		String[] split = msg.split(string);
		if(split.length>1) {
			//将所有图片的文件名取出
			String a[] = new String[split.length-1];
			for (int i = 1; i < split.length; i++) {
				a[i-1] = split[i].substring(0, split[i].indexOf("]"));
			}
			return a;
		}
		return null;
	}
}
