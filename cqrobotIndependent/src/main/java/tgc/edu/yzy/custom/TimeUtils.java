package tgc.edu.yzy.custom;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public class TimeUtils {
	/**
	 * 获取当前系统时间
	 * 
	 * @return
	 */
	public static Date getNowTime() {
		LocalDate date = LocalDate.now();
		Date date1 = Date.valueOf(date);
		return date1;
	}

	/**
	 * 
	 * 获取现在时间
	 * 
	 * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
	 */
	public static String getStringDate() {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(time);
		return dateString;
	}
	/**
	 * 
	 * 获取现在时间
	 * @param type 数据类型
	 * @return返回字符串格式 yyyy-MM-dd HH:mm:ss
	 */
	public static String getStringDate(String type) {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat(type);
		String dateString = formatter.format(time);
		return dateString;
	}
	/**
	 * 
	 * 获取现在时间
	 * 
	 * @return返回字符串格式 ddHHmm
	 */
	public static String getStringDates() {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("ddHHmm");
		String dateString = formatter.format(time);
		return dateString;
	}
	/**
	 * 
	 * 获取时间
	 * 
	 * @param time 填入格式：HH:mm:ss
	 * @return返回date HH:mm:ss
	 */
	public static java.util.Date getTime(String time) {
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		java.util.Date date = null;
		try {
			date = formatter.parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 
	 * 获取现在时间
	 * 
	 * @return返回字符串格式 yyyy-MM-dd
	 */
	public static String getStringTime() {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(time);
		return dateString;
	}

	/**
	 * 
	 * 获取现在时间
	 * 
	 * @return返回字符串格式 HH:mm:ss
	 */
	public static String getTime() {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String dateString = formatter.format(time);
		return dateString;
	}

	/**
	 * 获取当前系统时间一年后的时间
	 * 
	 * @return
	 */
	public static Date getDeadline() {
		LocalDate date = LocalDate.now();
		LocalDate date1 = date.plusYears(1);
		Date date2 = Date.valueOf(date1);
		return date2;
	}

	/***
	 * 判断时间是否为之后时间 与现在时间比较
	 * 
	 * @param time 被比较时间
	 * @return
	 */

	public static boolean AfterTime(Date time) {
		LocalDate date = LocalDate.now();
		Date date1 = Date.valueOf(date);
		if (time.after(date1)) {
			return true;
		} else {

			return false;
		}
	}

	/***
	 * 判断时间是否为之前时间 与现在时间比较
	 * 
	 * @param time 被比较时间
	 * @return
	 */

	public static boolean BeforeTime(Date time) {
		LocalDate date = LocalDate.now();
		Date date1 = Date.valueOf(date);
		if (time.before(date1)) {
			return true;
		} else {

			return false;
		}
	}

	/***
	 * 
	 * @param time 时间String
	 * @return 星期 String
	 */
	public static String WeekDay(String time) {
		try {
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date = format.parse(time);
			Instant instant = date.toInstant();
			ZoneId zoneId = ZoneId.systemDefault();
			LocalDate localDate = instant.atZone(zoneId).toLocalDate();
			DayOfWeek day = localDate.getDayOfWeek();
			String WeekDay = "";

			switch (day) {

			case MONDAY:

				WeekDay = "星期一";

				break;

			case FRIDAY:

				WeekDay = "星期五";

				break;

			case SATURDAY:

				WeekDay = "星期六";

				break;

			case SUNDAY:

				WeekDay = "星期日";

				break;

			case THURSDAY:

				WeekDay = "星期四";

				break;

			case TUESDAY:

				WeekDay = "星期二";

				break;

			case WEDNESDAY:

				WeekDay = "星期三";

				break;

			}

			return WeekDay;
		} catch (ParseException e) {
			return "报错了";
		}
	}

	
	/****
	 * 判读时间段为什么时候
	 * 返回 上午 下午 晚上
	 * @param time 起始时间
	 * @param endtime	结束时间
	 */
	public static String getDay(String time, String endtime) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		try {
			java.util.Date datetime = formatter.parse(time);
			java.util.Date dateendtime = formatter.parse(endtime);
			if (datetime.after(getTime("05:59:59")) && datetime.before(getTime("12:00:01")) && dateendtime.after(getTime("05:59:59")) && dateendtime.before(getTime("12:00:01"))) {
				return "上午";
			}else if(datetime.after(getTime("11:59:59")) && datetime.before(getTime("18:00:01")) && dateendtime.after(getTime("11:59:59")) && dateendtime.before(getTime("18:00:01"))) {
				return "下午";
			}else if((datetime.after(getTime("19:59:59")) && datetime.before(getTime("23:59:59")) && dateendtime.after(getTime("19:59:59")) && dateendtime.before(getTime("23:59:59")))) {
				return "晚上";
			}else {
				return "全天";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "全天";
	}
	
	/****
	 * 当前时间于string 之间相差多少秒
	 * string < 当前时间 返回正数
	 * @param string	传入需要对比的时间
	 * @return
	 */
	public static long datadifference(String string) {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = df.format(time);//当前时间
		java.util.Date now;
		try {
			now = df.parse(string);
			java.util.Date date=df.parse(format);
			long l=(date.getTime()-now.getTime())/1000;
			return l;
		} catch (ParseException e) {
			e.printStackTrace();
			return -1;
		}
		
		
	}
	/****
	 * string 时间 和 当前时间差多少
	 * string > 当前时间
	 * @param string	传入需要对比的时间
	 * @return
	 */
	public static long timedifference(String string) {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = df.format(time);//当前时间
		java.util.Date now;
		try {
			now = df.parse(string);
			java.util.Date date=df.parse(format);
			long l=(now.getTime()-date.getTime())/1000;
			return l;
		} catch (ParseException e) {
			e.printStackTrace();
			return -1;
		}
		
		
	}
	/****
	 * 获取当前时间后一天的时间
	 * @return
	 */
	public static String endDd() {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 1);
		return sf.format(c.getTime());
	}
	/****
	 * 获取当前时间后X S的时间
	 * @return
	 */
	public static String endDd(int sum) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, sum);
		return sf.format(c.getTime());
	}
	
	/****
	 * 获取当前时间后一天的时间
	 * @return
	 */
	public static String endDdHhmmss() {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 1);
		return sf.format(c.getTime());
	}
	/****
	 * 获取当前时间后一天的时间
	 * @return
	 */
	public static String endDdMM() {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, 180);
		return sf.format(c.getTime());
	}

	public static String endMin(int date) {
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, date);
		return sf.format(c.getTime());
	}

	public static String endMinTime(int date) {
		SimpleDateFormat sf = new SimpleDateFormat("HH:mm:ss");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, date);
		return sf.format(c.getTime());
	}
	/***
	 * 获取年月日最简组合
	 * @return
	 */
	public static String getAbcdefg() {
		java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
		int parseInt = Integer.parseInt(formatter.format(time));
		parseInt = parseInt - 200115;
		return AZ(parseInt);
	}
	
	public static String AZ(int num){
		String tcMsg = "";
		char sl = 0;
		if (-1 < num && num < 10) {
			tcMsg = "" + num;
		} else if (9 < num && num < 36) {
			sl = (char) (num - 42 + (int) 'a');
			tcMsg = "" + sl;
		}else if(num >= 36){
			int yushu = num/36 - 1;
			int ys = num%36;
			tcMsg = AZ(yushu)+AZ(ys);
		}
		return tcMsg;
	}
	public static String CalculateTime(String time) {
		if(time==null) {
			return "—";
		}
		long nowTime = System.currentTimeMillis(); // 获取当前时间的毫秒数
		String msg = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 指定时间格式
		java.util.Date setTime = null; // 指定时间
		try {
			setTime = sdf.parse(time); // 将字符串转换为指定的时间格式
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long reset = setTime.getTime(); // 获取指定时间的毫秒数
		long dateDiff = nowTime - reset;
		if (dateDiff < 0) {
			msg = "输入的时间不对";
		} else {
			long dateTemp1 = dateDiff / 1000; // 秒
			long dateTemp2 = dateTemp1 / 60; // 分钟
			long dateTemp3 = dateTemp2 / 60; // 小时
			long dateTemp4 = dateTemp3 / 24; // 天数
			long dateTemp5 = dateTemp4 / 30; // 月数
			long dateTemp6 = dateTemp5 / 12; // 年数
			if (dateTemp6 > 0) {
				msg = dateTemp6 + "年前";
			} else if (dateTemp5 > 0) {
				msg = dateTemp5 + "个月前";
			} else if (dateTemp4 > 0) {
				msg = dateTemp4 + "天前";
			} else if (dateTemp3 > 0) {
				msg = dateTemp3 + "小时前";
			} else if (dateTemp2 > 0) {
				msg = dateTemp2 + "分钟前";
			} else if (dateTemp1 > 0) {
				msg = "刚刚";
			}
		}
		return msg;
	}
	/**
     * 将秒数转换为日时分秒，
     * @param second
     * @return
     */
    public static String secondToTime(long second){
        long days = second / 86400;            //转换天数
        second = second % 86400;            //剩余秒数
        long hours = second / 3600;            //转换小时
        second = second % 3600;                //剩余秒数
        long minutes = second /60;            //转换分钟
        second = second % 60;                //剩余秒数
        if(days>30) {
        	 return days + "天";
        }else if(days>0){
            return days + "天" + hours + "小时" + minutes + "分" + second + "秒";
        }else if(hours>0){
            return hours + "小时" + minutes + "分" + second + "秒";
        }else if(minutes>0){
            return minutes + "分" + second + "秒";
        }else{
            return second + "秒";
        }
        
    }
    
    /**
     * 将日期转换为日时分秒
     * @param date
     * @return
     */
    public static String dateToTime(String date, String dateStyle){
        SimpleDateFormat format = new SimpleDateFormat(dateStyle);
        try {
        	java.util.Date oldDate = format.parse(date);
            long time = oldDate.getTime();                    //输入日期转换为毫秒数
            long nowTime = System.currentTimeMillis();        //当前时间毫秒数
            long second = nowTime - time;                    //二者相差多少毫秒
            second = second / 1000;                            //毫秒转换为妙
            long days = second / 86400;
            second = second % 86400;
            long hours = second / 3600;
            second = second % 3600;
            long minutes = second /60;
            second = second % 60;
            if(days>0){
                return days + "天" + hours + "小时" + minutes + "分" + second + "秒";
            }else{
                return hours + "小时" + minutes + "分" + second + "秒";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    /***
     * 获取距离今天结束 还剩余多长时间
     * @return
     */
    public static Integer getRemainSecondsOneDay() {
    	java.util.Date currentDate = new java.util.Date();
        LocalDateTime midnight = LocalDateTime.ofInstant(currentDate.toInstant(),
                ZoneId.systemDefault()).plusDays(1).withHour(0).withMinute(0)
                .withSecond(0).withNano(0);
        LocalDateTime currentDateTime = LocalDateTime.ofInstant(currentDate.toInstant(),
                ZoneId.systemDefault());
        long seconds = ChronoUnit.SECONDS.between(currentDateTime, midnight);
        return (int) seconds;
    }

}
