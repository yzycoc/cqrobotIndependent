package tgc.edu.yzy.bean;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/****
 * 配置表信息 
 * @author 936642284
 *
 */
@Data
@TableName("qq_robot_set")
public class QqRobotSet implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	private String createName; // 更新人
	private String createDate; // 更新时间
	/** 配置编码格式，如果不配置默认为UTF-8格式 **/
	private String encode;
	
	//配置酷Q的根路径地址，建议使用绝对路径，假如不配置则会导致部分功能无法使用
	private String cqPath;
	
	//配置机器人的QQ昵称
	private String localqqcode;
	
	
	//配置机器人的QQ号，如果不配置则@Filter注解将无法正常获取at信息
	private String localqqnick;
	
	//服务器地址
	private String linkIp;
	//配置java服务器的监听端口，默认为15514
	private Integer javaPort;
	//配置java服务器的请求路径，默认为/coolq 
	private String serverPath;
	//配置酷Q端的端口地址，默认为8877
	private Integer serverPort;
	//设置重连次数
	private Integer backlog;
	private String api;
    private String administrator;
    
	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getAdministrator() {
		return administrator;
	}

	public void setAdministrator(String administrator) {
		this.administrator = administrator;
	}

	public Integer getJavaPort() {
		return javaPort;
	}

	public void setJavaPort(Integer javaPort) {
		this.javaPort = javaPort;
	}

	public Integer getServerPort() {
		return serverPort;
	}

	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}

	public String getServerPath() {
		return serverPath;
	}

	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}

	public String getEncode() {
		return encode;
	}

	public void setEncode(String encode) {
		this.encode = encode;
	}

	public String getCqPath() {
		return cqPath;
	}

	public void setCqPath(String cqPath) {
		this.cqPath = cqPath;
	}

	

	public String getLocalqqcode() {
		return localqqcode;
	}

	public void setLocalqqcode(String localqqcode) {
		this.localqqcode = localqqcode;
	}

	public String getLocalqqnick() {
		return localqqnick;
	}

	public void setLocalqqnick(String localqqnick) {
		this.localqqnick = localqqnick;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLinkIp() {
		return linkIp;
	}

	public void setLinkIp(String linkIp) {
		this.linkIp = linkIp;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Integer getBacklog() {
		return backlog;
	}

	public void setBacklog(Integer backlog) {
		this.backlog = backlog;
	}

	
}
