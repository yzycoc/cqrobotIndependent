package tgc.edu.yzy.bean;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
@Data
@TableName("open_layout")
public class OpenLayout  implements Serializable{
	private static final long serialVersionUID = 1L;
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	private String imageUrl;//图片地址
	private String url;//阵型地址
	private String type;//阵型类型
	private String remark;//底部 方便给版权的
	private String number;//编号
	private String createName; // 更新人
	private String createDate; // 更新时间
	private String dlurl;//短链的
	private String dldate;
	
	public String getDlurl() {
		return dlurl;
	}
	public void setDlurl(String dlurl) {
		this.dlurl = dlurl;
	}
	public String getDldate() {
		return dldate;
	}
	public void setDldate(String dldate) {
		this.dldate = dldate;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "OpenLayout [id=" + id + ", imageUrl=" + imageUrl + ", url=" + url + ", type=" + type + ", remark="
				+ remark + ", number=" + number + ", createName=" + createName + ", createDate=" + createDate
				+ ", dlurl=" + dlurl + ", dldate=" + dldate + "]";
	}
	
}
