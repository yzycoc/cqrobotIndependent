package tgc.edu.yzy.bean;

import java.io.*;
import com.baomidou.mybatisplus.annotation.*;

@TableName("lexicon")
public class Lexicon implements Serializable
{
    private static final long serialVersionUID = 1L;
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String antistop;
    private String response;
    private String type;
    private String number;
    private Boolean isuser;
    private Integer sum;
    private String createName;
    private String createDate;
    
    public String getCreateName() {
        return this.createName;
    }
    
    public void setCreateName(final String createName) {
        this.createName = createName;
    }
    
    public String getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(final String createDate) {
        this.createDate = createDate;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    public static long getSerialversionuid() {
        return 1L;
    }
    
    public String getAntistop() {
        return this.antistop;
    }
    
    public void setAntistop(final String antistop) {
        this.antistop = antistop;
    }
    
    public String getResponse() {
        return this.response;
    }
    
    public void setResponse(final String response) {
        this.response = response;
    }
    
    public String getType() {
        return this.type;
    }
    
    public void setType(final String type) {
        this.type = type;
    }
    
    public String getNumber() {
        return this.number;
    }
    
    public void setNumber(final String number) {
        this.number = number;
    }
    
    public Boolean getIsuser() {
        return this.isuser;
    }
    
    public void setIsuser(final Boolean isuser) {
        this.isuser = isuser;
    }
    
    public Integer getSum() {
        return this.sum;
    }
    
    public void setSum(final Integer sum) {
        this.sum = sum;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Lexicon)) {
            return false;
        }
        final Lexicon other = (Lexicon)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$id = this.getId();
        final Object other$id = other.getId();
        Label_0065: {
            if (this$id == null) {
                if (other$id == null) {
                    break Label_0065;
                }
            }
            else if (this$id.equals(other$id)) {
                break Label_0065;
            }
            return false;
        }
        final Object this$antistop = this.getAntistop();
        final Object other$antistop = other.getAntistop();
        Label_0102: {
            if (this$antistop == null) {
                if (other$antistop == null) {
                    break Label_0102;
                }
            }
            else if (this$antistop.equals(other$antistop)) {
                break Label_0102;
            }
            return false;
        }
        final Object this$response = this.getResponse();
        final Object other$response = other.getResponse();
        Label_0139: {
            if (this$response == null) {
                if (other$response == null) {
                    break Label_0139;
                }
            }
            else if (this$response.equals(other$response)) {
                break Label_0139;
            }
            return false;
        }
        final Object this$type = this.getType();
        final Object other$type = other.getType();
        Label_0176: {
            if (this$type == null) {
                if (other$type == null) {
                    break Label_0176;
                }
            }
            else if (this$type.equals(other$type)) {
                break Label_0176;
            }
            return false;
        }
        final Object this$number = this.getNumber();
        final Object other$number = other.getNumber();
        Label_0213: {
            if (this$number == null) {
                if (other$number == null) {
                    break Label_0213;
                }
            }
            else if (this$number.equals(other$number)) {
                break Label_0213;
            }
            return false;
        }
        final Object this$isuser = this.getIsuser();
        final Object other$isuser = other.getIsuser();
        Label_0250: {
            if (this$isuser == null) {
                if (other$isuser == null) {
                    break Label_0250;
                }
            }
            else if (this$isuser.equals(other$isuser)) {
                break Label_0250;
            }
            return false;
        }
        final Object this$sum = this.getSum();
        final Object other$sum = other.getSum();
        Label_0287: {
            if (this$sum == null) {
                if (other$sum == null) {
                    break Label_0287;
                }
            }
            else if (this$sum.equals(other$sum)) {
                break Label_0287;
            }
            return false;
        }
        final Object this$createName = this.getCreateName();
        final Object other$createName = other.getCreateName();
        Label_0324: {
            if (this$createName == null) {
                if (other$createName == null) {
                    break Label_0324;
                }
            }
            else if (this$createName.equals(other$createName)) {
                break Label_0324;
            }
            return false;
        }
        final Object this$createDate = this.getCreateDate();
        final Object other$createDate = other.getCreateDate();
        if (this$createDate == null) {
            if (other$createDate == null) {
                return true;
            }
        }
        else if (this$createDate.equals(other$createDate)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof Lexicon;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $id = this.getId();
        result = result * 59 + (($id == null) ? 43 : $id.hashCode());
        final Object $antistop = this.getAntistop();
        result = result * 59 + (($antistop == null) ? 43 : $antistop.hashCode());
        final Object $response = this.getResponse();
        result = result * 59 + (($response == null) ? 43 : $response.hashCode());
        final Object $type = this.getType();
        result = result * 59 + (($type == null) ? 43 : $type.hashCode());
        final Object $number = this.getNumber();
        result = result * 59 + (($number == null) ? 43 : $number.hashCode());
        final Object $isuser = this.getIsuser();
        result = result * 59 + (($isuser == null) ? 43 : $isuser.hashCode());
        final Object $sum = this.getSum();
        result = result * 59 + (($sum == null) ? 43 : $sum.hashCode());
        final Object $createName = this.getCreateName();
        result = result * 59 + (($createName == null) ? 43 : $createName.hashCode());
        final Object $createDate = this.getCreateDate();
        result = result * 59 + (($createDate == null) ? 43 : $createDate.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Lexicon(id=" + this.getId() + ", antistop=" + this.getAntistop() + ", response=" + this.getResponse() + ", type=" + this.getType() + ", number=" + this.getNumber() + ", isuser=" + this.getIsuser() + ", sum=" + this.getSum() + ", createName=" + this.getCreateName() + ", createDate=" + this.getCreateDate() + ")";
    }
}
