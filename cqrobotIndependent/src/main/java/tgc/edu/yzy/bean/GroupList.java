package tgc.edu.yzy.bean;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("group_list")
public class GroupList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	private String name;//群名称
	private String groupcode;//群号
	private String isfalsetrue;//是否在群
	private String isadmin;//是否群管理
	private String headUrl;//
	private String level;//群等级
	private String opentype;//加群方式
	private String adminlist;//管理员列表
	private String board;//群最新公告
	private String createtime;//建群时间
	private String comoleteintro;//群介绍
	private String maxnumber;//群上限人数
	private String number;//当前群人数
	private String ownerqq;//群主QQ
	private String pos;//群地址坐标
	private String tags;//群标签
	private String endtime;//最后使用机器人的时间
	public String getIsadmin() {
		return isadmin;
	}

	public void setIsadmin(String isadmin) {
		this.isadmin = isadmin;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getOpentype() {
		return opentype;
	}

	public void setOpentype(String opentype) {
		this.opentype = opentype;
	}

	public String getAdminlist() {
		return adminlist;
	}

	public void setAdminlist(String adminlist) {
		this.adminlist = adminlist;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getComoleteintro() {
		return comoleteintro;
	}

	public void setComoleteintro(String comoleteintro) {
		this.comoleteintro = comoleteintro;
	}

	public String getMaxnumber() {
		return maxnumber;
	}

	public void setMaxnumber(String maxnumber) {
		this.maxnumber = maxnumber;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getOwnerqq() {
		return ownerqq;
	}

	public void setOwnerqq(String ownerqq) {
		this.ownerqq = ownerqq;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroupcode() {
		return groupcode;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getIsfalsetrue() {
		return isfalsetrue;
	}

	public String getHeadUrl() {
		return headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	public void setIsfalsetrue(String isfalsetrue) {
		this.isfalsetrue = isfalsetrue;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	
	
	
}
