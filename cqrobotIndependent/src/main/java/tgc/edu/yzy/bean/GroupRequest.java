package tgc.edu.yzy.bean;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

@Data
@TableName("group_request")
public class GroupRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	private String groupadmin;//群管理员
	private String groupadminstartor;//群主
	private String groupname;//群名称
	private String flag;//邀请唯一标志
	private String qq;//邀请人QQ
	private String number;//群人数
	private String maxnumber;//群用户上限
	private String groupcode;//群号
	private String board;//群简介
	private String type;//状态
	private String createName; // 更新人
	private String createDate; // 更新时间
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getGroupadmin() {
		return groupadmin;
	}

	public void setGroupadmin(String groupadmin) {
		this.groupadmin = groupadmin;
	}

	public String getGroupadminstartor() {
		return groupadminstartor;
	}

	public void setGroupadminstartor(String groupadminstartor) {
		this.groupadminstartor = groupadminstartor;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getMaxnumber() {
		return maxnumber;
	}

	public void setMaxnumber(String maxnumber) {
		this.maxnumber = maxnumber;
	}


	public String getGroupcode() {
		return groupcode;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "GroupRequest [id=" + id + ", groupadmin=" + groupadmin + ", groupadminstartor=" + groupadminstartor
				+ ", groupname=" + groupname + ", flag=" + flag + ", qq=" + qq + ", number=" + number + ", maxnumber="
				+ maxnumber + ", groupcode=" + groupcode + ", board=" + board + ", type=" + type + ", createName="
				+ createName + ", createDate=" + createDate + "]";
	}
	
	
	
}
