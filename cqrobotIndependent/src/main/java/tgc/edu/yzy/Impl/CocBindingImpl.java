package tgc.edu.yzy.Impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import tgc.edu.yzy.bean.CocBinding;
import tgc.edu.yzy.mapper.CocBindingMapper;
import tgc.edu.yzy.service.CocBindingService;
@Service(value="CocBindingService")
public class CocBindingImpl extends ServiceImpl<CocBindingMapper, CocBinding> implements CocBindingService{

}
