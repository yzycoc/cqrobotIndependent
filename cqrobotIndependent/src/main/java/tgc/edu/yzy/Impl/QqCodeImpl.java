package tgc.edu.yzy.Impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import tgc.edu.yzy.bean.QqCode;
import tgc.edu.yzy.mapper.QqCodeMapper;
import tgc.edu.yzy.service.QqCodeService;
@Service(value="QqCodeService")
public class QqCodeImpl extends ServiceImpl<QqCodeMapper, QqCode> implements QqCodeService{

}
