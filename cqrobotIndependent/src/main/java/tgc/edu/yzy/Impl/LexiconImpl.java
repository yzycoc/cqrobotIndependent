package tgc.edu.yzy.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import tgc.edu.yzy.bean.Lexicon;
import tgc.edu.yzy.mapper.LexiconMapper;
import tgc.edu.yzy.service.LexiconService;
@Service(value="LexiconService")
public class LexiconImpl extends ServiceImpl<LexiconMapper, Lexicon> implements LexiconService{
	@Autowired
	private LexiconMapper dao;
	public void deleteAll() {
		dao.deleteAll();
	}

}
