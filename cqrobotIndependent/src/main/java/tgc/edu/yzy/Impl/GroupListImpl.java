package tgc.edu.yzy.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import tgc.edu.yzy.bean.GroupList;
import tgc.edu.yzy.mapper.GroupListMapper;
import tgc.edu.yzy.service.GroupListService;
@Service(value="GroupListService")
public class GroupListImpl extends ServiceImpl<GroupListMapper, GroupList> implements GroupListService{
	@Autowired
	private GroupListMapper dao;
	public void updateAll() {
		dao.updateAll();
	}
	

}
