package tgc.edu.yzy.Impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import tgc.edu.yzy.bean.GroupRequest;
import tgc.edu.yzy.mapper.GroupRequestMapper;
import tgc.edu.yzy.service.GroupRequestService;
@Service(value="GroupRequestService")
public class GroupRequestImpl extends ServiceImpl<GroupRequestMapper, GroupRequest> implements GroupRequestService{

}
