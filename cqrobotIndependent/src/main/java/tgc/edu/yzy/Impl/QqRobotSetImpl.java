package tgc.edu.yzy.Impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import tgc.edu.yzy.bean.QqRobotSet;
import tgc.edu.yzy.mapper.QqRobotSetMapper;
import tgc.edu.yzy.service.QqRobotSetService;


@Service(value="QqRobotSetService")
public class QqRobotSetImpl extends ServiceImpl<QqRobotSetMapper, QqRobotSet> implements QqRobotSetService{ 

}
