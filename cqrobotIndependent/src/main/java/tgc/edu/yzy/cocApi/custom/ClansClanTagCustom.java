package tgc.edu.yzy.cocApi.custom;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.BeanUtils;

import tgc.edu.yzy.cocApi.entity.ClansClanTag.ClansClanTag;
import tgc.edu.yzy.custom.BackEndHttpRequest;
import tgc.edu.yzy.custom.entity.coc.Clans;

/***
 * 获取部落信息
 * @author 936642284
 *
 */
public class ClansClanTagCustom {

	/****
	 * https://api.clashofclans.com/v1/clans/%23PGUPYL8J
	 * 获取这个部落的信息 内容包括 成员信息
	 * 	tag 游戏标签 不带# 号
	 * @author 936642284
	 * @return dataisnull true 有数据 false 无数据
	 * @return error 无数据返回的内容
	 * @return gototype 有数据返回"历史数据"
	 */
	public static ClansClanTag findAll(String tag) {
		if(tag.length()<5) {
			ClansClanTag clannull = new ClansClanTag();
			clannull.setDataisnull(false);
			clannull.setError("无法查询[#"+tag+"]相关的部落信息！\n因为低于5位标签！如果这是你真实的标签，请联系作者进行校验修改！");
			return clannull;
		}else {
			tag = tag.replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "").replaceAll("#", "");
			String regex = "^[a-z0-9A-Z]+$";
			boolean matches = tag.matches(regex);
			if(!matches) {
				ClansClanTag clannull = new ClansClanTag();
				clannull.setDataisnull(false);
				clannull.setError("无法查询[#"+tag+"]相关的部落信息！\n因为标签内容不规范！如果这是你真实的标签，请联系作者进行校验修改！");
				return clannull;
			}
		}
		String url = "https://api.clashofclans.com/v1/clans/%23"+tag;
		ClansClanTag clan = new ClansClanTag();
		try {
			//String sendGetCoc =BackEndHttpRequest.sendGet("http://yaozhenyong.xyz:8855/coctest?tag="+tag, "");
			String sendGetCoc = BackEndHttpRequest.sendGetCoc(url);
			switch (sendGetCoc) {
			case "400":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("无法查询到[#"+tag+"]相关的部落信息！\n请核对标签，推荐直接进入游戏复制！");
					return clannull;
					
				}else {
					clan.setDataisnull(true);
				}
				break;
			case "403":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("COCAPI出现问题，请反馈作者密钥出现问题！");
					return clannull;
				}else {
					clan.setDataisnull(true);
				}
				break;
			case "404":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("无法查询到[#"+tag+"]相关的部落信息！\n请核对标签，推荐直接进入游戏复制！");
					return clannull;
				}else {
					clan.setDataisnull(true);
				}
				break;
			case "429":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("COCAPI出现问题，请反馈作者密钥已被截流！");
					return clannull;
				}else {
					clan.setDataisnull(true);
				}
				break;
			case "500":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clashofstatsHttp = getClashofstatsHttp(tag);
					if(clashofstatsHttp!=null) {
						clashofstatsHttp.setGototype("官方接口维护中，此数据为第三方数据！");
						clashofstatsHttp.setDataisnull(true);
						return clashofstatsHttp;
					}
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("查询接口出现错误，请等待官方恢复后再次查询！");
					return clannull;
				}else {
					clan.setDataisnull(true);
				}
				break;
			case "503":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clashofstatsHttps = getClashofstatsHttp(tag);
					if(clashofstatsHttps!=null) {
						clashofstatsHttps.setGototype("官方接口维护中，此数据为第三方数据！");
						clashofstatsHttps.setDataisnull(true);
						return clashofstatsHttps;
					}
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("COC服务器正在维护，请等待维护结束后查询！");
					return clannull;
				}else {
					clan.setDataisnull(true);
				}
				break;
			case "200":
				clan = getSql(tag);
				if(clan==null) {
					ClansClanTag clannull = new ClansClanTag();
					clannull.setDataisnull(false);
					clannull.setError("无法查询到[#"+tag+"]相关的部落信息！\n请核对标签，推荐直接进入游戏复制！");
					return clannull;
				}else {
					clan.setDataisnull(true);
				}
				break;
			default:
				/** 成功返回数据 **/
				ObjectMapper mapper = new ObjectMapper();
				clan = mapper.readValue(sendGetCoc,new TypeReference<ClansClanTag>() { });
				//保存COCtext文件
				TxtCoc.coc(sendGetCoc,"部落"+tag);
				clan.setDataisnull(true);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			clan.setDataisnull(false);
			clan.setError("无法查询到[#"+tag+"]相关的部落信息！请反馈作者！\n请核对标签，推荐直接进入游戏复制！");
		}
		return clan;
	}
	
	//在数据库查询缓存数据
	/***
	 * 
	 * @param string 部落标签
	 * @return
	 */
	private static ClansClanTag getSql(String string) {
		try {
			String get = TxtCoc.getCoc("部落"+string);
			if(get.length()>19) {
				String time = get.substring(0, 19);
				String post = get.substring(19, get.length());
				ObjectMapper mapper = new ObjectMapper();
				ClansClanTag clan = mapper.readValue(post,new TypeReference<ClansClanTag>() { });
				clan.setGototype("\n官方服务器维护，此数据获取时间为："+time);
				return clan;
			}
			return null;
		} catch (Exception e) {
			System.out.println("处理数据库的JSON数据出错---------------------获取部落信息！");
			return null;
		} 
	}
	/***
	 * 链接服务器！
	 * @param tag
	 */
	private static ClansClanTag getClashofstatsHttp(String tag) {
		String url = "https://api.clashofstats.com/clans/"+tag;
		String sendGet = BackEndHttpRequest.sendGet(url, "");
		ObjectMapper mapper = new ObjectMapper();
		try {
			ClansClanTag clannull = new ClansClanTag();
			Clans list = mapper.readValue(sendGet,new TypeReference<Clans>() { });
			BeanUtils.copyProperties(list, clannull,"id");
			return clannull;
		}  catch (Exception e) {
			return null;
		}
	}
}
