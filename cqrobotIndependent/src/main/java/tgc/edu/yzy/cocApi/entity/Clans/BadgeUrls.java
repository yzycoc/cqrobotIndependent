package tgc.edu.yzy.cocApi.entity.Clans;

public class BadgeUrls {
	private String small;
	private String large;
	private String medium;
	/***
	 * 小图片
	 * @return
	 */
	public String getSmall() {
		return small;
	}
	public void setSmall(String small) {
		this.small = small;
	}
	/***
	 * 
	 * @return
	 */
	public String getLarge() {
		return large;
	}
	public void setLarge(String large) {
		this.large = large;
	}
	/***
	 * 大图片
	 * @return
	 */
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
}
