package tgc.edu.yzy.cocApi.entity.ClansClanTag;
/***
 * 获取用户主世界图标
 * @author 936642284
 *
 */
public class League {
	private String id;
	private String name;//对应的奖杯"Legend League","Silver League II",
	private IconUrls iconUrls;//主世界图标
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public IconUrls getIconUrls() {
		return iconUrls;
	}
	public void setIconUrls(IconUrls iconUrls) {
		this.iconUrls = iconUrls;
	}
	
}
