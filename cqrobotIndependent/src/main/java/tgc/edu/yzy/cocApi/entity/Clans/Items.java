package tgc.edu.yzy.cocApi.entity.Clans;


public class Items {
	private String tag;
	private String name;
	private String inviteOnly;
	private Location location;
	private BadgeUrls badgeUrls;
	private String clanLevel;
	private String clanPoints;
	private String clanVersusPoints;
	private String requiredTrophies;
	private String warFrequency;
	private String warWinStreak;
	private String warWins;
	private String warTies;
	private String warLosses;
	private String isWarLogPublic;
	private String members;
	/**
	 * 返回的标签
	 * @return
	 */
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	/***
	 * 返回的游戏名
	 * @return
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInviteOnly() {
		return inviteOnly;
	}
	public void setInviteOnly(String inviteOnly) {
		this.inviteOnly = inviteOnly;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public BadgeUrls getBadgeUrls() {
		return badgeUrls;
	}
	public void setBadgeUrls(BadgeUrls badgeUrls) {
		this.badgeUrls = badgeUrls;
	}
	public String getClanLevel() {
		return clanLevel;
	}
	public void setClanLevel(String clanLevel) {
		this.clanLevel = clanLevel;
	}
	public String getClanPoints() {
		return clanPoints;
	}
	public void setClanPoints(String clanPoints) {
		this.clanPoints = clanPoints;
	}
	public String getClanVersusPoints() {
		return clanVersusPoints;
	}
	public void setClanVersusPoints(String clanVersusPoints) {
		this.clanVersusPoints = clanVersusPoints;
	}
	public String getRequiredTrophies() {
		return requiredTrophies;
	}
	public void setRequiredTrophies(String requiredTrophies) {
		this.requiredTrophies = requiredTrophies;
	}
	public String getWarFrequency() {
		return warFrequency;
	}
	public void setWarFrequency(String warFrequency) {
		this.warFrequency = warFrequency;
	}
	public String getWarWinStreak() {
		return warWinStreak;
	}
	public void setWarWinStreak(String warWinStreak) {
		this.warWinStreak = warWinStreak;
	}
	public String getWarWins() {
		return warWins;
	}
	public void setWarWins(String warWins) {
		this.warWins = warWins;
	}
	public String getWarTies() {
		return warTies;
	}
	public void setWarTies(String warTies) {
		this.warTies = warTies;
	}
	public String getWarLosses() {
		return warLosses;
	}
	public void setWarLosses(String warLosses) {
		this.warLosses = warLosses;
	}
	public String getIsWarLogPublic() {
		return isWarLogPublic;
	}
	public void setIsWarLogPublic(String isWarLogPublic) {
		this.isWarLogPublic = isWarLogPublic;
	}
	public String getMembers() {
		return members;
	}
	public void setMembers(String members) {
		this.members = members;
	}
	
}
