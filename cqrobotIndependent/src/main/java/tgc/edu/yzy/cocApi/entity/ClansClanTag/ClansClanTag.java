package tgc.edu.yzy.cocApi.entity.ClansClanTag;

import java.util.ArrayList;
import java.util.List;

import tgc.edu.yzy.cocApi.entity.Clans.BadgeUrls;
import tgc.edu.yzy.cocApi.entity.Clans.Location;
import tgc.edu.yzy.custom.CocForm;
/****
 * https://api.clashofclans.com/v1/clans/%23PGUPYL8J
 * 获取这个部落的信息 内容包括 成员信息
 * @author 936642284
 *
 */
public class ClansClanTag extends CocForm{
	private String tag;//游戏标签
	private String name;//游戏名
	private String type;//游戏加入方式
	private String description;//游戏公告
	private String clanLevel;//部落等级
	private String clanPoints;//部落积分
	private String clanVersusPoints;//部落夜世界积分
	private String requiredTrophies;//请求需要的奖杯
	private String warFrequency;//战争频率
	private String warWinStreak;//部落战连胜
	private String warWins;//部落胜场
	private String warTies;//戰爭平局
	private String warLosses;//部落战败
	private Boolean isWarLogPublic;//是否公开部落战日志
	private Location location;//部落位置
	private BadgeUrls badgeUrls;//部落logo
	private String members;//部落人数
	private List<MemberList> memberList = new ArrayList<>();//部落成员List
	private  List<Labels> labels = new ArrayList<>();//部落成员List
	public String getWarTies() {
		return warTies;
	}
	public void setWarTies(String warTies) {
		this.warTies = warTies;
	}
	public String getWarLosses() {
		return warLosses;
	}
	public void setWarLosses(String warLosses) {
		this.warLosses = warLosses;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getClanLevel() {
		return clanLevel;
	}
	public void setClanLevel(String clanLevel) {
		this.clanLevel = clanLevel;
	}
	public String getClanPoints() {
		return clanPoints;
	}
	public void setClanPoints(String clanPoints) {
		this.clanPoints = clanPoints;
	}
	public String getClanVersusPoints() {
		return clanVersusPoints;
	}
	public void setClanVersusPoints(String clanVersusPoints) {
		this.clanVersusPoints = clanVersusPoints;
	}
	public String getRequiredTrophies() {
		return requiredTrophies;
	}
	public void setRequiredTrophies(String requiredTrophies) {
		this.requiredTrophies = requiredTrophies;
	}
	public String getWarFrequency() {
		return warFrequency;
	}
	public void setWarFrequency(String warFrequency) {
		this.warFrequency = warFrequency;
	}
	public String getWarWinStreak() {
		return warWinStreak;
	}
	public void setWarWinStreak(String warWinStreak) {
		this.warWinStreak = warWinStreak;
	}
	public String getWarWins() {
		return warWins;
	}
	public void setWarWins(String warWins) {
		this.warWins = warWins;
	}
	public Boolean getIsWarLogPublic() {
		return isWarLogPublic;
	}
	public void setIsWarLogPublic(Boolean isWarLogPublic) {
		this.isWarLogPublic = isWarLogPublic;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public BadgeUrls getBadgeUrls() {
		return badgeUrls;
	}
	public void setBadgeUrls(BadgeUrls badgeUrls) {
		this.badgeUrls = badgeUrls;
	}
	public String getMembers() {
		return members;
	}
	public void setMembers(String members) {
		this.members = members;
	}
	public List<MemberList> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<MemberList> memberList) {
		this.memberList = memberList;
	}
	public List<Labels> getLabels() {
		return labels;
	}
	public void setLabels(List<Labels> labels) {
		this.labels = labels;
	}
	
}
