package tgc.edu.yzy.cocApi.custom;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
 
public class MyFont {
	
	//filepath字体文件的路径
	public static java.awt.Font getSelfDefinedFont(String filepath){
        java.awt.Font font = null;
        File file = new File(filepath);
        try{
            font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, file);
            font = font.deriveFont(java.awt.Font.PLAIN, 40);
        }catch (FontFormatException e){
            return null;
        }catch (FileNotFoundException e){
            return null;
        }catch (IOException e){
            return null;
        }
        return font;
    }
	/***
	 * HoboStd
	 * @return
	 */
	public static java.awt.Font getHoboStd(Integer size){
        java.awt.Font font = null;
        File file = new File("C:\\cocutil\\font\\HoboStd.otf");
        try{
            font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, file);
            font = font.deriveFont(java.awt.Font.BOLD,size);
        }catch (FontFormatException e){
            return null;
        }catch (FileNotFoundException e){
            return null;
        }catch (IOException e){
            return null;
        }
        return font;
    }
	/***
	 * STENCIL STD
	 * @return
	 */
	public static java.awt.Font getStencilStd(){
        java.awt.Font font = null;
        File file = new File("C:\\cocutil\\font\\StencilStd.otf");
        try{
            font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, file);
            font = font.deriveFont(java.awt.Font.BOLD, 55);
        }catch (FontFormatException e){
            return null;
        }catch (FileNotFoundException e){
            return null;
        }catch (IOException e){
            return null;
        }
        return font;
    }
 
}