package tgc.edu.yzy.cocApi.custom;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import net.coobird.thumbnailator.Thumbnails;
import tgc.edu.yzy.cocApi.entity.yuqing.YuQing;
import tgc.edu.yzy.custom.BaseConfiguration;

/***
 * 鱼情
 * @author 936642284
 *
 */
public class ImageYq {
	
	private static BufferedImage up = null;
	private static BufferedImage down = null;
	static {
		try {
			up = ImageIO.read(new File("C:\\cocutil\\matter\\up.png"));
			down = ImageIO.read(new File("C:\\cocutil\\matter\\down.png"));//buffer.get("cocheader");
		} catch (IOException e) {
			e.printStackTrace();
		}//buffer.get("cocheader");
		
	}
	
	
	public void getImage(YuQing yQ) {
		Graphics2D g =  null;
		try {
			BufferedImage Image = ImageIO.read(new File("C:\\cocutil\\matter\\cocyq.jpg"));//buffer.get("cocheader");
			g = (Graphics2D)Image.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			g.setFont(MyFont.getStencilStd());
			g.setColor(Color.black);
			//世界
			g.drawString(yQ.getWordZlp(), 621, 500);
			g.drawString(yQ.getWordZx(), 347, 714);
			g.drawString(yQ.getWordLx(), 347, 828);
			g.drawString(yQ.getWordHd(), 406, 935);
			g.drawString(yQ.getWordKgj(), 406, 1042);
			//中国
			g.drawString(yQ.getCnZlp(), 2035, 458);
			
			g.drawString(yQ.getCnZx(), 1760, 680);
			g.drawString(yQ.getCnLx(), 1760, 787);
			g.drawString(yQ.getCnYhd(), 1830, 893);
			g.drawString(yQ.getCnKgj(), 1830, 1003);
			
			//百分比
			g.setFont(new Font("Myriad Pro Cond",Font.PLAIN,40));
			String txt = "";
			Integer bfb = 35;
			txt = yQ.wgetWordZxBfb();
			g.drawString(txt, 347+(NameWidth(yQ.getWordZx())*bfb), 714);
			txt = yQ.wgetWordLxBfb();
			g.drawString(txt, 347+(NameWidth(yQ.getWordLx())*bfb), 828);
			txt = yQ.wgetWordHdBfb();
			g.drawString(txt, 406+(NameWidth(yQ.getWordHd())*bfb), 937);
			txt = yQ.wgetWordKgjBfb();
			g.drawString(txt, 406+(NameWidth(yQ.getWordKgj())*bfb), 1042);
			
			txt = yQ.wgetCnZxBfb();
			g.drawString(txt, 1760+(NameWidth(yQ.getCnZx())*bfb), 675);
			txt = yQ.wgetCnLxBfb();
			g.drawString(txt, 1760+(NameWidth(yQ.getCnLx())*bfb), 777);
			txt = yQ.wgetCnYhdBfb();
			g.drawString(txt, 1830+(NameWidth(yQ.getCnYhd())*bfb), 893);
			txt = yQ.wgetCnKgjBfb();
			g.drawString(txt, 1830+(NameWidth(yQ.getCnKgj())*bfb), 1003);
			//获取时间
			g.setFont(new Font("微软雅黑",Font.BOLD,65));
			g.drawString(yQ.getTime(), 2474, 1313);
			g.setFont(new Font("微软雅黑",Font.BOLD,45));
			g.drawString(yQ.getTs1(), 215, 1467);
			g.drawString(yQ.getTs2(), 215, 1645);
			g.drawString(yQ.getTs3(), 215, 1821);
			
			//设置变化趋势
			g.setFont(MyFont.getHoboStd(55));
			//世界的
			String wordBh = yQ.getWordBh();
			quShi(g,wordBh);
			g.drawString(wordBh, 723, 605);
			BufferedImage buff = getBuff(wordBh);
			g.drawImage(buff, 650, 535, 60, 100, null);
			//中国的
			String cnBh = yQ.getCnBh();
			quShi(g,cnBh);
			g.drawString(cnBh, 2125, 571);
			buff = getBuff(cnBh);
			g.drawImage(buff, 2064, 505, 60, 100, null);
			
			g.setFont(MyFont.getHoboStd(45));
			//全世界
			int whiht = 1054;
			int heigth = 719;
			cnBh = yQ.getWordZxBh();
			quShi(g,cnBh);
			g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			
			heigth+=110;
			cnBh = yQ.getWordLxBH();
			quShi(g,cnBh);
			//g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			
			heigth+=110;
			cnBh = yQ.getWordHdBh();
			quShi(g,cnBh);
			g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			
			heigth+=110;
			cnBh = yQ.getWordKgjBh();
			quShi(g,cnBh);
			g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			g.drawImage(buff, 2000, 1765, 60, 100, null);
			/**
			 * 中国
			 */
			whiht = 2485;
			heigth = 670;
			
			cnBh = yQ.getCnZxbh();
			quShi(g,cnBh);
			g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			
			heigth+=110;
			cnBh = yQ.getCnLXBh();
			quShi(g,cnBh);
			//g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			
			heigth+=110;
			cnBh = yQ.getCnYhdBh();
			quShi(g,cnBh);
			g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			
			heigth+=110;
			cnBh = yQ.getCnKgjBh();
			quShi(g,cnBh);
			g.drawString(cnBh,whiht , heigth);
			buff = getBuff(cnBh);
			g.drawImage(buff, whiht-65, heigth-80, 60, 100, null);
			//评分
			ColorZs(g,yQ.getPf());
			
			
			g.setFont(new Font("微软雅黑",Font.BOLD,60));
			g.setColor(Color.black);
			String[] dyyj = Dyyj(yQ.getYj(),10);
			for (int i = 0; i < dyyj.length; i++) {
				if(i==0) {
					g.drawString(dyyj[i], 2474, 1437);
				}else {
					g.drawString(dyyj[i], 2150, 1437+(i*65));
				}
			}
			System.out.println("生成完毕");
			/*ImageIO.write(Image, "png", new File("G:\\酷Q\\酷Q Air\\新建文件夹\\"+TimeUtils.getStringDates()+".png"));*/
			Thumbnails.of(Image).outputFormat("jpg").scale(1f).outputQuality(0.45f).toFile(
					new File(BaseConfiguration.getCqPath()+"\\data\\image\\yuqing.jpg"));
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			g.dispose();
		}
	}
	/***
	 * 趋势颜色变化
	 * @param g 
	 * @param wordBh 
	 */
	private static void quShi(Graphics2D g, String wordBh) {
		try {
			double parseDouble = Double.parseDouble(wordBh);
			if(parseDouble>=0) {
				g.setColor(new Color(0,88,30));
			}else {
				g.setColor(new Color(132,38,30));
			}
		} catch (Exception e) {
			g.setColor(Color.black);
		}
	}
	/***
	 * 获取上升下降的图标
	 * @param txt
	 * @return
	 */
	private static BufferedImage getBuff(String txt) {
		try {
			double parseDouble = Double.parseDouble(txt);
			if(parseDouble>0) {
				return up;
			}else {
				return down;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	/***
	 * 求文字长度
	 * @param dondate
	 * @return
	 */
	private static int NameWidth(String dondate) {
		int dondatelength = 0;
		dondatelength = dondate.length();
		
		return dondatelength;
	}
	
	/***
	 * 打鱼意见
	 * 分割字符长度
	 * @param number
	 * @return 
	 */
	private static String[] Dyyj(String number,Integer sum) {
		if(number==null) {
			String[] rems = {""};
			return rems;
		}
		Integer size =  (number.length()-5)/sum + 2;
		String[] remsg =  new String[size];
		Integer min = 0,max = 5;
		for (int i = 0; i < remsg.length; i++) {
			if(i==0) {
				if(number.length()>5) {
					remsg[i] = number.substring(min, max);
				}else {
					remsg[i] = number.substring(min, number.length());
				}
				min = 5;
				max+=sum;
			}else {
				if(max>number.length()) {
					remsg[i] = number.substring(min, number.length());
				}else {
					remsg[i] = number.substring(min, max);
				}
				min =  max;
				max+=sum;
			}
			
		}
		return remsg;
	}
	/***
	 * 颜色换色
	 * @param g
	 * @param number
	 */
	private static void ColorZs(Graphics2D g,String number) {
		g.setFont(new Font("STENCIL STD",Font.BOLD,280));
		double parseDouble = Double.parseDouble(number);
		if(parseDouble>=6) {
			g.setColor(new Color(0,88,30));//绿色
		}else if(parseDouble>=3&&parseDouble<6) {
			g.setColor(new Color(255,115,0));//橘黄色
		}else {
			g.setColor(new Color(132,38,30));//红色
		}
		if(number.length()==1) {
			g.drawString(number, 1700, 1739);
		}else if(number.length()==2) {
			g.drawString(number, 1610, 1739);
		}else{
			g.drawString(number, 1570, 1739);
		}
	}

}
