package tgc.edu.yzy.cocApi.entity.ClansClanTag;

public class IconUrls {
	private String small;//中
	private String tiny;//迷你
	private String medium;//大
	/***
	 * 中
	 * @return
	 */
	public String getSmall() {
		return small;
	}
	public void setSmall(String small) {
		this.small = small;
	}
	/***
	 * 迷你
	 * @return
	 */
	public String getTiny() {
		return tiny;
	}
	public void setTiny(String tiny) {
		this.tiny = tiny;
	}
	/***
	 * 大
	 * @return
	 */
	public String getMedium() {
		return medium;
	}
	public void setMedium(String medium) {
		this.medium = medium;
	}
	
}
