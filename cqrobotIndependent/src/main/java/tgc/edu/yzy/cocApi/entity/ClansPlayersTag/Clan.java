package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

import tgc.edu.yzy.cocApi.entity.Clans.BadgeUrls;

/***
 * 相关的部落信息
 * @author 936642284
 *
 */
public class Clan {
	private String tag;//部落标签
	private String name;//部落名称
	private String clanLevel;//部落等级
	private BadgeUrls badgeUrls;//部落图标
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClanLevel() {
		return clanLevel;
	}
	public void setClanLevel(String clanLevel) {
		this.clanLevel = clanLevel;
	}
	public BadgeUrls getBadgeUrls() {
		return badgeUrls;
	}
	public void setBadgeUrls(BadgeUrls badgeUrls) {
		this.badgeUrls = badgeUrls;
	}
	
}
