package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

public class LegendStatistics {
	private String legendTrophies;//传说奖杯
	private BestSeason bestSeason;//主世界最高奖杯
	private CurrentSeason currentSeason;
	private PreviousVersusSeason previousVersusSeason;
	private BestVersusSeason bestVersusSeason;
	private PreviousSeason previousSeason;
	public PreviousSeason getPreviousSeason() {
		return previousSeason;
	}
	public void setPreviousSeason(PreviousSeason previousSeason) {
		this.previousSeason = previousSeason;
	}
	public String getLegendTrophies() {
		return legendTrophies;
	}
	public void setLegendTrophies(String legendTrophies) {
		this.legendTrophies = legendTrophies;
	}
	public BestSeason getBestSeason() {
		return bestSeason;
	}
	public void setBestSeason(BestSeason bestSeason) {
		this.bestSeason = bestSeason;
	}
	public CurrentSeason getCurrentSeason() {
		return currentSeason;
	}
	public void setCurrentSeason(CurrentSeason currentSeason) {
		this.currentSeason = currentSeason;
	}
	public PreviousVersusSeason getPreviousVersusSeason() {
		return previousVersusSeason;
	}
	public void setPreviousVersusSeason(PreviousVersusSeason previousVersusSeason) {
		this.previousVersusSeason = previousVersusSeason;
	}
	public BestVersusSeason getBestVersusSeason() {
		return bestVersusSeason;
	}
	public void setBestVersusSeason(BestVersusSeason bestVersusSeason) {
		this.bestVersusSeason = bestVersusSeason;
	}
	
}
