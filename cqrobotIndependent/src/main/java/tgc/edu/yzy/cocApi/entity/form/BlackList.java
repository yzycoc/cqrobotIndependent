package tgc.edu.yzy.cocApi.entity.form;

public class BlackList {
	private String qqcode;
	private String date;
	public String getQqcode() {
		return qqcode;
	}
	public void setQqcode(String qqcode) {
		this.qqcode = qqcode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
