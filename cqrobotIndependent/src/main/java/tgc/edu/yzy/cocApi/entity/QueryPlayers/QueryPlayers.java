package tgc.edu.yzy.cocApi.entity.QueryPlayers;

public class QueryPlayers {
	private String tag;
	private String name;
	private Integer townHallLevel;//大本营等级
	private Integer townHallWeaponLevel;//获取的12本防御等级
	private Integer expLevel;//经验等级
	private Integer versusTrophies;//夜世界
	public Integer getVersusTrophies() {
		return versusTrophies;
	}
	public void setVersusTrophies(Integer versusTrophies) {
		this.versusTrophies = versusTrophies;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getTownHallLevel() {
		return townHallLevel;
	}
	public void setTownHallLevel(Integer townHallLevel) {
		this.townHallLevel = townHallLevel;
	}
	public Integer getTownHallWeaponLevel() {
		return townHallWeaponLevel;
	}
	public void setTownHallWeaponLevel(Integer townHallWeaponLevel) {
		this.townHallWeaponLevel = townHallWeaponLevel;
	}
	public Integer getExpLevel() {
		return expLevel;
	}
	public void setExpLevel(Integer expLevel) {
		this.expLevel = expLevel;
	}
	
}
