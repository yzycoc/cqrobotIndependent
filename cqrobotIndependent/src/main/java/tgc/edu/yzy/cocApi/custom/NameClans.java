package tgc.edu.yzy.cocApi.custom;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import tgc.edu.yzy.custom.BackEndHttpRequest;
import tgc.edu.yzy.custom.Https;

public class NameClans {
	
	/***
	 * 查询部落
	 * @param name
	 * @return
	 */
	public String get(String name) {
		try {
			if(name.length()<3) {
				return "搜索部落名必须3个字";
			}
			Map<String, String> map = new HashMap<String, String>();
			map.put("name", name);
			String urlEncode = BackEndHttpRequest.urlEncode(map);
			String url = "https://api.clashofclans.com/v1/clans?"+urlEncode+"&limit=5";
			
			/*Map<String, String> maps = new HashMap<String, String>();
			maps.put("url", url);
			
			String httpsGetTxt = Https.httpsGetTxt("http://47.94.135.148:8853/coc", maps);*/
			//String sendGet = BackEndHttpRequest.sendPost("http://47.94.135.148:8853/coc", url);
			String httpsGetTxt = BackEndHttpRequest.sendGetCoc(url);
			
			JSONObject playersdata = JSON.parseObject(httpsGetTxt);
			JSONArray items = playersdata.getJSONArray("items");
			if(items==null||items.size()==0) {
				return "无法查询到名称为："+name+" 的部落。";
			}else {
				return result(items,name);
			}
		} catch (Exception e) {
			return "无法查询到名称为："+name+" 的部落。";
		}
	}
	private String result(JSONArray items, String name2) {
		StringBuffer result = new StringBuffer(); 
		result.append("≮-"+name2+" 查询反馈-≯");
		for (int i = 0; i < items.size(); i++) {
			JSONObject jsonObject = items.getJSONObject(i);
			String name = jsonObject.getString("name");
			result.append("\n\n●"+name);
			String tag = jsonObject.getString("tag");
			result.append("\n●"+tag);
			result.append("\n●"+jsonObject.getString("clanLevel")+"级|"+jsonObject.getString("members")+"人|"+jsonObject.getString("clanPoints"));
		}
		return result.toString();
	}
}
