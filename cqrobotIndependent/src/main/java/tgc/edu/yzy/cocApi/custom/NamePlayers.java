package tgc.edu.yzy.cocApi.custom;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import tgc.edu.yzy.custom.Https;

public class NamePlayers {
	/***
	 * 查询 玩家
	 * @param name
	 * @return
	 */
	public String get(String name) {
		try {
			Map<String, String> map = new HashMap<>();
			map.put("q", name);
			map.put("page", "0");
			map.put("nameEquality", "true");
			String httpsGetTxt = Https.httpsGetTxt("https://api.clashofstats.com/search/players", map);
			
			JSONObject playersdata = JSON.parseObject(httpsGetTxt);
			JSONArray items = playersdata.getJSONArray("items");
			if(items==null||items.size()==0) {
				return "无法查询到昵称为："+name+" 的玩家。";
			}else {
				return result(items);
			}
		} catch (Exception e) {
			return "无法查询到昵称为："+name+" 的玩家。";
		}
		
	}
	/***
	 * 处理 列表信息  然后返回
	 * @param items
	 */
	private String result(JSONArray items) {
		StringBuffer result = new StringBuffer(); 
		result.append("≮-数据来源clashofstats-≯");
		int size = items.size();
		if(size>5) {
			size = 5;
			result.append("\n获取"+items.size()+"+数据，仅显示前5条！");
		}
		for (int i = 0; i < size; i++) {
			JSONObject Play = items.getJSONObject(i);
			String name = Play.getString("name");//玩家名称
			String clanName = Play.getString("clanName");//部落名称
			String trophies = Play.getString("trophies");//部落等级
			String tag = Play.getString("tag");
			result.append("\n\n●昵称："+name);
			result.append("\n●标签："+tag);
			if(StringUtils.isNotEmpty(trophies)&&!"0".equals(trophies)) {
				result.append("\n●奖杯："+trophies);
			}
			if(StringUtils.isNotEmpty(clanName)) {
				result.append("\n●部落："+clanName);
			}
			
		}
		return result.toString();
	}
	
	
}
