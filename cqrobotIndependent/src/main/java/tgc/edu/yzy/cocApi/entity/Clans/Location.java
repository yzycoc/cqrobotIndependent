package tgc.edu.yzy.cocApi.entity.Clans;
/***
 * 玩家所在位置
 * @author 936642284
 *
 */
public class Location {
	private String id;
	private String name;
	private String isCountry;
	private String countryCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	/***
	 * 地理位置
	 * @return China
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/***
	 * 是否真实
	 * @return true false
	 */
	public String getIsCountry() {
		return isCountry;
	}
	public void setIsCountry(String isCountry) {
		this.isCountry = isCountry;
	}
	/***
	 * 国家语言的代码
	 * @return CN
	 */
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
}
