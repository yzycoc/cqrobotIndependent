package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

import java.util.ArrayList;
import java.util.List;

import tgc.edu.yzy.cocApi.entity.ClansClanTag.Labels;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.League;
import tgc.edu.yzy.custom.CocForm;

public class ClansPlayersTag extends CocForm{
	private String tag;//标签
	private String name;//名称
	private String townHallLevel;//大本营1-12本等级
	private String townHallWeaponLevel;//大本营武器等级
	private String expLevel;//经验等级
	private String trophies;//现主世界奖杯
	private String bestTrophies;//主世界历史最高奖杯
	private String warStars;//战争的胜利之星
	private String attackWins;//进攻胜利场次
	private String defenseWins;//防御胜利场次
	private String builderHallLevel;//建筑大师基地大本营等级
	private String versusTrophies;//夜世界奖杯数
	private String bestVersusTrophies;//夜世界奖杯数 最高纪录
	private String versusBattleWins;//夜世界对抗赛胜场次
	private String role;//所在部落权限 首领副首领等
	private String donations;//捐兵
	private String donationsReceived;//收兵
	private Clan clan;//所在部落的信息
	private League league;//个人奖杯等级和图片
	private LegendStatistics legendStatistics;//传说统计
	private List<Achievements> achievements = new ArrayList<>();//获得的成就achievements
	private String versusBattleWinCount;//对战胜率
	private List<Troops> troops = new ArrayList<>();//军队等级
	private List<Heroes> heroes= new ArrayList<>();//英雄的等级
	private List<Spells> spells= new ArrayList<>();;//各种法术的等级
	private  List<Labels> labels = new ArrayList<>();//部落成员List
	public List<Labels> getLabels() {
		return labels;
	}
	public void setLabels(List<Labels> labels) {
		this.labels = labels;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTownHallLevel() {
		return townHallLevel;
	}
	public void setTownHallLevel(String townHallLevel) {
		this.townHallLevel = townHallLevel;
	}
	public String getTownHallWeaponLevel() {
		return townHallWeaponLevel;
	}
	public void setTownHallWeaponLevel(String townHallWeaponLevel) {
		this.townHallWeaponLevel = townHallWeaponLevel;
	}
	public String getExpLevel() {
		return expLevel;
	}
	public void setExpLevel(String expLevel) {
		this.expLevel = expLevel;
	}
	public String getTrophies() {
		return trophies;
	}
	public void setTrophies(String trophies) {
		this.trophies = trophies;
	}
	public String getBestTrophies() {
		return bestTrophies;
	}
	public void setBestTrophies(String bestTrophies) {
		this.bestTrophies = bestTrophies;
	}
	public String getWarStars() {
		return warStars;
	}
	public void setWarStars(String warStars) {
		this.warStars = warStars;
	}
	public String getAttackWins() {
		return attackWins;
	}
	public void setAttackWins(String attackWins) {
		this.attackWins = attackWins;
	}
	public String getDefenseWins() {
		return defenseWins;
	}
	public void setDefenseWins(String defenseWins) {
		this.defenseWins = defenseWins;
	}
	public String getBuilderHallLevel() {
		return builderHallLevel;
	}
	public void setBuilderHallLevel(String builderHallLevel) {
		this.builderHallLevel = builderHallLevel;
	}
	public String getVersusTrophies() {
		return versusTrophies;
	}
	public void setVersusTrophies(String versusTrophies) {
		this.versusTrophies = versusTrophies;
	}
	public String getBestVersusTrophies() {
		return bestVersusTrophies;
	}
	public void setBestVersusTrophies(String bestVersusTrophies) {
		this.bestVersusTrophies = bestVersusTrophies;
	}
	public String getVersusBattleWins() {
		return versusBattleWins;
	}
	public void setVersusBattleWins(String versusBattleWins) {
		this.versusBattleWins = versusBattleWins;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDonations() {
		return donations;
	}
	public void setDonations(String donations) {
		this.donations = donations;
	}
	public String getDonationsReceived() {
		return donationsReceived;
	}
	public void setDonationsReceived(String donationsReceived) {
		this.donationsReceived = donationsReceived;
	}
	public Clan getClan() {
		return clan;
	}
	public void setClan(Clan clan) {
		this.clan = clan;
	}
	public League getLeague() {
		return league;
	}
	public void setLeague(League league) {
		this.league = league;
	}
	public LegendStatistics getLegendStatistics() {
		return legendStatistics;
	}
	public void setLegendStatistics(LegendStatistics legendStatistics) {
		this.legendStatistics = legendStatistics;
	}
	
	public String getVersusBattleWinCount() {
		return versusBattleWinCount;
	}
	public void setVersusBattleWinCount(String versusBattleWinCount) {
		this.versusBattleWinCount = versusBattleWinCount;
	}
	
	public List<Achievements> getAchievements() {
		return achievements;
	}
	public void setAchievements(List<Achievements> achievements) {
		this.achievements = achievements;
	}
	public List<Troops> getTroops() {
		return troops;
	}
	public void setTroops(List<Troops> troops) {
		this.troops = troops;
	}
	public List<Heroes> getHeroes() {
		return heroes;
	}
	public void setHeroes(List<Heroes> heroes) {
		this.heroes = heroes;
	}
	public List<Spells> getSpells() {
		return spells;
	}
	public void setSpells(List<Spells> spells) {
		this.spells = spells;
	}
	
	
	
	
	
	
}
