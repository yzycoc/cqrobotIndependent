package tgc.edu.yzy.cocApi.entity.ClansClanTag;

public class MemberList {
	private String tag;//玩家游戏标签
	private String name;//玩家游戏名称
	private String role;//玩家游戏职位
	private String expLevel;//经验等级
	private String trophies;//主世界奖杯
	private String versusTrophies;//夜世界奖杯
	private String clanRank;//部落排名
	private String previousClanRank;//以前的排名
	private String donations;//捐兵
	private String donationsReceived;//收兵
	private League league;
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getExpLevel() {
		return expLevel;
	}
	public void setExpLevel(String expLevel) {
		this.expLevel = expLevel;
	}
	public String getTrophies() {
		return trophies;
	}
	public void setTrophies(String trophies) {
		this.trophies = trophies;
	}
	public String getVersusTrophies() {
		return versusTrophies;
	}
	public void setVersusTrophies(String versusTrophies) {
		this.versusTrophies = versusTrophies;
	}
	public String getClanRank() {
		return clanRank;
	}
	public void setClanRank(String clanRank) {
		this.clanRank = clanRank;
	}
	public String getPreviousClanRank() {
		return previousClanRank;
	}
	public void setPreviousClanRank(String previousClanRank) {
		this.previousClanRank = previousClanRank;
	}
	public String getDonations() {
		return donations;
	}
	public void setDonations(String donations) {
		this.donations = donations;
	}
	public String getDonationsReceived() {
		return donationsReceived;
	}
	public void setDonationsReceived(String donationsReceived) {
		this.donationsReceived = donationsReceived;
	}
	public League getLeague() {
		return league;
	}
	public void setLeague(League league) {
		this.league = league;
	}
	
}
