package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

public class Heroes {
	private String name;//王的名称
	private String level;//自己王的等级
	private String maxLevel;//英雄最高等级
	private String village;//位置 home家乡，builderBase建造基地
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getMaxLevel() {
		return maxLevel;
	}
	public void setMaxLevel(String maxLevel) {
		this.maxLevel = maxLevel;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	
	
	
}
