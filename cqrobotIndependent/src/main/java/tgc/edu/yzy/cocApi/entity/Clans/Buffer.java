package tgc.edu.yzy.cocApi.entity.Clans;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Buffer {
	private BufferedImage buffer;

	public BufferedImage getBuffer() {
		if(buffer==null) {
			try {
				buffer =  ImageIO.read(new File("C:\\cocutil\\matter\\badgeUrlsClan.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return buffer;
	}

	public void setBuffer(BufferedImage buffer) {
		this.buffer = buffer;
	}
	
}
