package tgc.edu.yzy.cocApi.custom;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.forte.qqrobot.utils.BaseLocalThreadPool;

import tgc.edu.yzy.cocApi.entity.QueryPlayers.QueryClan;
import tgc.edu.yzy.cocApi.entity.QueryPlayers.QueryPlayers;
import tgc.edu.yzy.custom.BackEndHttpRequest;

/***
 * 查询部落成员分布情况
 * @author 936642284
 *
 */
public class QueryPlayersTagCustom {
	
	public static QueryClan getmembers(String tag) {
		String url = "https://api.clashofclans.com/v1/clans/%23"+tag;
		String sendGet ="";
		JSONArray jsonx = null;
		QueryClan clan  = null;
		if(sendGet==""||sendGet==null) {//https://api.clashofclans.com/v1/clans/%23  http://yaozhenyong.xyz:8855/coctest?tag=
			//url = "http://yaozhenyong.xyz:8855/coctest?tag="+tag;
			String sendGetCoc = BackEndHttpRequest.sendGetCoc(url);
			if(sendGetCoc.length()<4) {
				return null;
			}
			JSONObject j = JSON.parseObject(sendGetCoc); 
			clan = new QueryClan();
			clan.setNumber(j.getString("members"));
			clan.setIsWarLogPublic(j.getBoolean("isWarLogPublic"));
			clan.setTag(j.getString("tag"));
			clan.setName(j.getString("name"));
			String string = "未设置";
			try {
				string = j.getJSONObject("location").getString("name");
				clan.setLocationname(string);
			} catch (Exception e) {
				clan.setLocationname("未设置");
			}
			jsonx = j.getJSONArray("memberList");
		}
		if(jsonx==null||jsonx.size()<1) {
			return null;
		}
		List<QueryPlayers> query = new ArrayList<>();
		Executor threadPool = BaseLocalThreadPool.getThreadPool();
		CountDownLatch count = new CountDownLatch(jsonx.size());
		for (int i = 0; i < jsonx.size(); i++) { 
			QueryPlayers player = new QueryPlayers();
			JSONObject jo = jsonx.getJSONObject(i);
			Integer dby = jo.getInteger("townHallLevel");//townHallWeaponLevel
			
			threadPool.execute(()->{
				//取出村庄信息 
				//jo.put("townHallLevel", "12");
				if(dby==null) {//https://api.clashofclans.com/v1/players/%23 http://yaozhenyong.xyz:8855/coc?tag=
					String urlplay = "https://api.clashofclans.com/v1/players/%23"+jo.getString("tag").substring(1, jo.getString("tag").length());
					String sendGetCoc = BackEndHttpRequest.sendGetCoc(urlplay);
					JSONObject jos =  JSON.parseObject(sendGetCoc);
					player.setVersusTrophies(jos.getInteger("builderHallLevel"));
					player.setName(jos.getString("name"));
					player.setTag(jos.getString("tag"));
					player.setExpLevel(jos.getInteger("expLevel"));
					player.setTownHallLevel(jos.getInteger("townHallLevel"));
					player.setTownHallWeaponLevel(jos.getInteger("townHallWeaponLevel"));
					query.add(player);
				}
                count.countDown();
			});
		}
		try {
			count.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		clan.setQuery(query);
		return clan;
	}
	
}
