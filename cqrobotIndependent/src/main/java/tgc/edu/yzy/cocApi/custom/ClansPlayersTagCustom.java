package tgc.edu.yzy.cocApi.custom;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.BeanUtils;

import tgc.edu.yzy.cocApi.entity.Clans.BadgeUrls;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.ClansClanTag;
import tgc.edu.yzy.cocApi.entity.ClansPlayersTag.Clan;
import tgc.edu.yzy.cocApi.entity.ClansPlayersTag.ClansPlayersTag;
import tgc.edu.yzy.custom.BackEndHttpRequest;
import tgc.edu.yzy.custom.entity.players.PlayersClans;

public class ClansPlayersTagCustom {
	
	
	/***
	 * https://api.clashofclans.com/v1/players/%23
	 * 获取玩家的标签
	 * @param tag 玩家标签
	 * @return dataisnull true 有数据 false 无数据
	 * @return error 无数据返回的内容
	 * @return Gototype 有数据返回"历史数据"
	 */
	public static ClansPlayersTag findAll(String tag) {
		if(tag.length()<5) {
			ClansPlayersTag clannull = new ClansPlayersTag();
			clannull.setDataisnull(false);
			clannull.setError("无法查询[#"+tag+"]相关的玩家信息！\n因为低于5位标签！如果这是你真实的标签，请联系作者进行修改！");
			return clannull;
		}else {
			tag = tag.replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "").replaceAll("#", "");
			String regex = "^[a-z0-9A-Z]+$";
			boolean matches = tag.matches(regex);
			if(!matches) {
				ClansPlayersTag clannull = new ClansPlayersTag();
				clannull.setDataisnull(false);
				clannull.setError("无法查询[#"+tag+"]相关的玩家信息！\n因为标签内容不规范！如果这是你真实的标签，请联系作者进行校验修改！");
				return clannull;
			}
		}
		String url = "https://api.clashofclans.com/v1/players/%23"+tag;
		ClansPlayersTag player = new ClansPlayersTag();
		try {
			//String sendGetCoc =BackEndHttpRequest.sendGet("http://yaozhenyong.xyz:8855/coc?tag="+tag, "");
			String sendGetCoc = BackEndHttpRequest.sendGetCoc(url);
			switch (sendGetCoc) {
			case "400":
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("无法查询到[#"+tag+"]相关的玩家信息！\n请核对标签，推荐直接进入游戏复制！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}
				break;
			case "403":
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("COCAPI出现问题，请反馈作者密钥出现问题！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}
				break;
			case "404":
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("无法查询到[#"+tag+"]相关的玩家信息！\n请核对标签，推荐直接进入游戏复制！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}
				break;
			case "429":
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("COCAPI出现问题，请反馈作者密钥已被截流！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}
				break;
			case "500":
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clashofstatsHttp = getClashofstatsHttp(tag);
					if(clashofstatsHttp!=null) {
						clashofstatsHttp.setGototype("官方接口异常，此数据为第三方数据！");
						clashofstatsHttp.setDataisnull(true);
						return clashofstatsHttp;
					}
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("查询接口出现错误，请等待官方恢复后再次查询！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}
				break;
			case "503":
				
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clashofstatsHttps = getClashofstatsHttp(tag);
					if(clashofstatsHttps!=null) {
						clashofstatsHttps.setGototype("官方接口维护中，此数据为第三方数据！");
						clashofstatsHttps.setDataisnull(true);
						return clashofstatsHttps;
					}
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("COC服务器正在维护，请等待维护结束后查询！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}

				break;
			case "200":
				player = getSql(tag);
				if(player==null) {
					ClansPlayersTag clannull = new ClansPlayersTag();
					clannull.setDataisnull(false);
					clannull.setError("无法查询到[#"+tag+"]相关的部落信息！\n请核对标签，推荐直接进入游戏复制！");
					return clannull;
				}else {
					player.setDataisnull(true);
				}
				break;

			default:
				ObjectMapper mapper = new ObjectMapper();
				player = mapper.readValue(sendGetCoc,new TypeReference<ClansPlayersTag>() { });
				//保存COCtext文件
				TxtCoc.coc(sendGetCoc,"村庄"+tag);
				player.setDataisnull(true);
				break;
			}
		} catch (Exception e) {
			player.setDataisnull(false);
			player.setError("无法查询到[#"+tag+"]相关的玩家信息！请反馈作者！\n请核对标签，推荐直接进入游戏复制！");
		}
		
		return player;
	}

	/****
	 * 查询本地数据库
	 * @param string
	 * @return
	 */
	private static ClansPlayersTag getSql(String string) {
		try {
			String get = TxtCoc.getCoc("村庄"+string);
			if(get.length()>19) {
				String time = get.substring(0, 19);
				String post = get.substring(19, get.length());
				ObjectMapper mapper = new ObjectMapper();
				ClansPlayersTag tag =  mapper.readValue(post,new TypeReference<ClansPlayersTag>() { });
				tag.setGototype("\n● 官方服务器维护，此数据获取时间为："+time);
				return tag;
			}
			return null;
		} catch (Exception e) {
			System.out.println("处理数据库的JSON数据出错---------------------获取玩家信息！");
			return null;
		}
	}
	
	/****
	 * 当官方默认服务器关闭时
	 * 链接老 Clashofstats 服务器
	 * @param tag
	 * @return 
	 */
	private static ClansPlayersTag getClashofstatsHttp(String tag) {
		String sendGet = BackEndHttpRequest.sendGet("https://api.clashofstats.com/players/"+tag, "");
		if(sendGet!=""||sendGet!=null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ClansPlayersTag player = new ClansPlayersTag();
				PlayersClans list = mapper.readValue(sendGet,new TypeReference<PlayersClans>() { });
				BeanUtils.copyProperties(list, player,"id");
				String roleget = list.getClan()==null?"无部落":list.getClan().getRole();
				player.setRole(roleget);
				if(list.getClan()!=null) {
					Clan clan = new Clan();
					BadgeUrls badgeUrls = new BadgeUrls();
					badgeUrls.setSmall(list.getClan().getBadge());
					badgeUrls.setLarge(list.getClan().getBadge());
					badgeUrls.setMedium(list.getClan().getBadge());
					clan.setBadgeUrls(badgeUrls);
					clan.setName(list.getClan().getName());
					clan.setTag(list.getClan().getTag());
					player.setClan(clan);
				}
				return player;
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
}
