package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

public class Achievements {
	private String name;//名称 Bigger Coffers
	private String stars;//类型
	private String value;//值
	private String target;//目标
	private String info;//信息 Upgrade a Gold Storage to level 10 升级黄金储备到10级
	private String completionInfo;//最高任务 Highest Gold Storage level: 12 最高黄金储备水平:12
	private String village;//村名 home 家 builderBase夜世界
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStars() {
		return stars;
	}
	public void setStars(String stars) {
		this.stars = stars;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getCompletionInfo() {
		return completionInfo;
	}
	public void setCompletionInfo(String completionInfo) {
		this.completionInfo = completionInfo;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	
}
