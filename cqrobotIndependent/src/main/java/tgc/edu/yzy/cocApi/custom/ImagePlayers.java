package tgc.edu.yzy.cocApi.custom;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.imageio.ImageIO;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import net.coobird.thumbnailator.Thumbnails;
import tgc.edu.yzy.cocApi.entity.Clans.PlayleTroops;
import tgc.edu.yzy.cocApi.entity.image.ImagePlayersFrom;
import tgc.edu.yzy.cocApi.service.PlayersService;
import tgc.edu.yzy.controller.MyqqRobot;
import tgc.edu.yzy.custom.BackEndHttpRequest;
import tgc.edu.yzy.custom.BaseConfiguration;

/***
 * 玩家图片生成
 * @author 936642284
 *
 */
public class ImagePlayers {
	/*public static void main(String[] args) throws Exception {
		get("2PJVLLULO");
		//get("Q028VVYU");
		// 存放在二维码中的内容
		
		//String text = "http://yzycoc.com/qq/cocApi/clancoc?tag=282UJCR0L";
		// 嵌入二维码的图片路径
		//String imgPath = "C:\\cocutil\\matter\\g.jpg";
		// 生成的二维码的路径及名称
		//String destPath = "C:\\cocutil\\matter\\end\\二维码.jpg";
		//生成二维码
		//QRCodeUtil.encode(text, imgPath, destPath, true);
		// 解析二维码
		//String str = QRCodeUtil.decode(destPath);
		// 打印出解析出的内容
		//System.out.println(str);
		
	}*/
	/**
	 * 生成玩家信息 
	 * @param tag 村庄标签 不带#号
	 * @return null 生成成功
	 */
	public String get(String tag) {
		try {
			long startTime=System.currentTimeMillis();
			System.out.println("V2.0生成玩家信息图片："+tag);
			//数据准备区=tags; //
			String sendGetCoc = BackEndHttpRequest.sendGetCoc("https://api.clashofclans.com/v1/players/%23"+tag);
			//String sendGetCoc = BackEndHttpRequest.sendGet("http://47.100.197.180:8855/coc","tag="+tag);
			if(sendGetCoc.length()<6) {
				return "无法查询到玩家信息！";
			}
			long endTime = System.currentTimeMillis();
			System.out.println("V2.0玩家 获取玩家数据成功**共耗时："+(endTime-startTime)+"ms");
			//数据处理准备区 
			JSONObject playersdata = JSON.parseObject(sendGetCoc);
			PlayersService service = new PlayersService();
			ImagePlayersFrom tagData = service.TagData(playersdata);
			//部落图片合成图片
			ImageCompound(playersdata,tagData,tag);
			System.out.println("V2.0玩家 生成图片成功**共耗时："+(System.currentTimeMillis()-endTime)+"ms");
			Integer integer = MyqqRobot.Image_SUM.get("V2.0玩家");
			integer = integer==null?0:integer;
			MyqqRobot.Image_SUM.put("V2.0玩家", integer+1);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return "图片生成失败！";
		}
	}
	/***
	 * 部落图片合成
	 * @param playersdata
	 * @param tagData
	 * @param tag 
	 * @throws IOException 
	 */
	private void ImageCompound(JSONObject player, ImagePlayersFrom tagData, String tag) throws IOException {
		BufferedImage I = ImageIO.read(new File("C:\\cocutil\\matter\\playImage.png"));//buffer.get("cocheader");
		Graphics2D g = (Graphics2D)I.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
		/***
		 * 弄头部数据 玩家
		 */
		g.drawImage(tagData.getGrth(), 100, 305, 300, 300, null);
		
		g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,80));
		g.setColor(Color.white);
		String play_name = PlayersService.txt(player.getString("name"),2);
		g.drawString(play_name, 400, 435);
		String play_tag = PlayersService.txt(player.getString("tag"),2);
		g.setFont(new Font("微软雅黑",Font.ITALIC,40));
		g.drawString(play_tag, 410, 550);
		List<BufferedImage> gxbq = tagData.getGxbq();
		Integer gxbq_Image_whdth = 24;
		for (BufferedImage bufferedImage : gxbq) {
			g.drawImage(bufferedImage, gxbq_Image_whdth+100, 623, 80, 80, null);
			gxbq_Image_whdth+=100;
		}
		
		/***
		 * 弄部落信息
		 */
		g.drawString(tagData.getClanTag(), 1190, 1455);
		g.drawImage(tagData.getClan(), 870, 1230, 300, 300, null);
		String clanName = tagData.getClanName();
		if("".equals(clanName)) {
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,60));
			g.setColor(Color.gray);
			g.drawString("无部落", 1170, 1300);
		}else{
			Integer ClanName_Szie =60;
			if(clanName.length()>13) {
				ClanName_Szie = 45;
			}
			g.setFont(new Font("微软雅黑",Font.CENTER_BASELINE,ClanName_Szie));
			g.setColor(Color.BLACK);
			g.drawString(clanName, 1170, 1300);
		}
		
		g.setFont(new Font("微软雅黑",Font.PLAIN,40));
		g.setColor(Color.gray);
		g.drawString(tagData.getClanClanLevel(), 1190, 1370);
		/***
		 * 玩家个人数据
		 */
		//第一列
		g.setFont(new Font("微软雅黑",Font.BOLD,60));
		g.setColor(Color.BLACK);
		Integer whdth = 390;
		Integer heigth =1900;
		List<String> play_one = tagData.getPlay_one();
		for (int j = 0; j < play_one.size(); j++) {//-(NameWidth(play_one.get(j))*3)
			g.drawString(play_one.get(j), whdth-(NameWidth(play_one.get(j))*12),heigth);
			whdth += 590;
		}
		
		//第二列
		heigth+=120;
		whdth= 370;
		g.setColor(new Color(210,105,30));
		List<String> play_two = tagData.getPlay_two();
		for (int j = 0; j < play_two.size(); j++) {
			g.drawString(play_two.get(j), whdth,heigth);
			whdth += 590;
		}
		
		//第三四列
		heigth+=180;
		whdth= 390;
		g.setColor(Color.BLACK);
		List<String> play_three = tagData.getPlay_three();
		for (int j = 0; j < play_three.size(); j++) {
			g.drawString(play_three.get(j), whdth-(NameWidth(play_three.get(j))*14),heigth);
			whdth += 588;
			if(j==2) {
				heigth+=155;
				whdth= 390;
			}
		}
		
		//第五列
		heigth+=290;
		whdth= 390;
		g.setColor(new Color(210,105,30));
		List<String> play_five = tagData.getPlay_five();
		for (int j = 0; j < play_five.size(); j++) {
			g.drawString(play_five.get(j), whdth-(NameWidth(play_five.get(j))*12),heigth);
			whdth += 590;
		}
		//英雄数据
		Color level = Color.white;//普通等级颜色
		Color level_back = new Color(0,160,233);//普通背景颜色
		Color maxlevel = Color.white;//满级等级颜色
		Color maxlevel_back =new Color(255,140,0);//满级背景颜色
		whdth= 230;
		heigth = 2980;
		Integer txt_width = whdth+35;
		Integer txt_heigth = heigth+75;
		List<PlayleTroops> heroes = tagData.getHeroes();
		for (int j = 0; j < heroes.size(); j++) {
			PlayleTroops playleTroops = heroes.get(j);
			if(playleTroops==null) {
				g.setColor(new Color(128,128,128));
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(Color.white);
				g.drawString("无", txt_width-15, txt_heigth);
			}else if(playleTroops!=null&&playleTroops.getMax()) {
				g.setColor(maxlevel_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(maxlevel);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}else {
				g.setColor(level_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(level);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}
			whdth +=252;
			txt_width +=252;
		}
		
		whdth= 230;
		heigth = 3410;
		txt_width = whdth+35;
		txt_heigth = heigth+75;
		List<PlayleTroops> troops_home = tagData.getTroops_home();
		for (int j = 0; j < troops_home.size(); j++) {
			PlayleTroops playleTroops = troops_home.get(j);
			if(playleTroops==null) {
				g.setColor(new Color(128,128,128));
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(Color.white);
				g.drawString("无", txt_width-15, txt_heigth);
			}else if(playleTroops!=null&&playleTroops.getMax()) {
				g.setColor(maxlevel_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(maxlevel);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}else {
				g.setColor(level_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(level);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}
			whdth +=244;
			txt_width +=244;
			if((j+1)%7==0) {
				whdth= 230;
				heigth += 285;
				txt_width = whdth+35;
				txt_heigth = heigth+75;
			}
		}
		
		
		whdth= 215;
		heigth = 4690;
		txt_width = whdth+35;
		txt_heigth = heigth+75;
		List<PlayleTroops> gcd = tagData.getGcd();
		for (int j = 0; j < gcd.size(); j++) {
			PlayleTroops playleTroops = gcd.get(j);
			if(playleTroops==null) {
				g.setColor(new Color(128,128,128));
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(Color.white);
				g.drawString("无", txt_width-15, txt_heigth);
			}else if(playleTroops!=null&&playleTroops.getMax()) {
				g.setColor(maxlevel_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(maxlevel);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}else {
				g.setColor(level_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(level);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}
			whdth +=244;
			txt_width +=244;
		}
		
		whdth= 215;
		heigth = 5060;
		txt_width = whdth+35;
		txt_heigth = heigth+75;
		List<PlayleTroops> troops_builderBase = tagData.getTroops_builderBase();
		for (int j = 0; j < troops_builderBase.size(); j++) {
			PlayleTroops playleTroops = troops_builderBase.get(j);
			if(playleTroops==null) {
				g.setColor(new Color(128,128,128));
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(Color.white);
				g.drawString("无", txt_width-15, txt_heigth);
			}else if(playleTroops!=null&&playleTroops.getMax()) {
				g.setColor(maxlevel_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(maxlevel);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}else {
				g.setColor(level_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(level);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}
			whdth +=244;
			txt_width +=244;
			if((j+1)%7==0) {
				whdth= 230;
				heigth += 285;
				txt_width = whdth+35;
				txt_heigth = heigth+75;
			}
		}
		
		whdth= 225;
		heigth = 5720;
		txt_width = whdth+35;
		txt_heigth = heigth+75;
		List<PlayleTroops> spells = tagData.getSpells();
		for (int j = 0; j < spells.size(); j++) {
			PlayleTroops playleTroops = spells.get(j);
			if(playleTroops==null) {
				g.setColor(new Color(128,128,128));
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(Color.white);
				g.drawString("无", txt_width-15, txt_heigth);
			}else if(playleTroops!=null&&playleTroops.getMax()) {
				g.setColor(maxlevel_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(maxlevel);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}else {
				g.setColor(level_back);
				g.fillArc(whdth,heigth , 100, 100, 0,360);
				g.setColor(level);
				g.drawString(playleTroops.getNumber(), playleTroopsTxt(playleTroops.getNumber(),txt_width), txt_heigth);
			}
			whdth +=245;
			txt_width +=245;
			if((j+1)%7==0) {
				whdth= 230;
				heigth += 285;
				txt_width = whdth+35;
				txt_heigth = heigth+75;
			}
		}
		//6300
		//放入二维码
		g.drawImage(tagData.getErweima(), 300, 6200, 370, 370, null);
		g.drawString("扫码进入网站", 750, 6300);
		g.drawString("可进入游戏查看玩家 也可查看部落信息", 750, 6500);
		//player
		Thumbnails.of(I).outputFormat("jpg").scale(1f).outputQuality(0.45f).toFile(
				new File(BaseConfiguration.getCqPath()+"\\data\\image\\image\\clanAll\\player"+tag));
		//Thumbnails.of(I).outputFormat("jpg").scale(1f).outputQuality(0.45f).toFile(new File("G:\\酷Q\\酷Q Air\\新建文件夹\\"+TimeUtils.getTimeS()));
		g.dispose();
	}
	private int playleTroopsTxt(String txt,Integer whdth) {
		switch (txt.length()) {
		case 1:
			return whdth - 3;
		case 2:
			return whdth - 24;
		default:
			break;
		}
		return whdth;
	}
	private int NameWidth(String dondate) {
		int dondatelength = 0;
		try {
			dondatelength = dondate.getBytes("GB2312").length;
		} catch (UnsupportedEncodingException e) {
			dondatelength = dondate.length();
		}
		return dondatelength;
	}
	
	static String tags = "{\"tag\":\"#JPPYUQY2\",\"name\":\"半醒半醉半浮生\",\"townHallLevel\":13,\"townHallWeaponLevel\":5,\"expLevel\":211,\"trophies\":5223,\"bestTrophies\":5608,\"warStars\":1781,\"attackWins\":93,\"defenseWins\":2,\"builderHallLevel\":9,\"versusTrophies\":3141,\"bestVersusTrophies\":3284,\"versusBattleWins\":1598,\"role\":\"coLeader\",\"donations\":8278,\"donationsReceived\":7456,\"clan\":{\"tag\":\"#282UJCR0L\",\"name\":\"涉★外\",\"clanLevel\":12,\"badgeUrls\":{\"small\":\"https://api-assets.clashofclans.com/badges/70/XzCxkO1-8QfejywfJCyanxW1UdLBQHSq89lqlD9R5Uc.png\",\"large\":\"https://api-assets.clashofclans.com/badges/512/XzCxkO1-8QfejywfJCyanxW1UdLBQHSq89lqlD9R5Uc.png\",\"medium\":\"https://api-assets.clashofclans.com/badges/200/XzCxkO1-8QfejywfJCyanxW1UdLBQHSq89lqlD9R5Uc.png\"}},\"league\":{\"id\":29000022,\"name\":\"Legend League\",\"iconUrls\":{\"small\":\"https://api-assets.clashofclans.com/leagues/72/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png\",\"tiny\":\"https://api-assets.clashofclans.com/leagues/36/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png\",\"medium\":\"https://api-assets.clashofclans.com/leagues/288/R2zmhyqQ0_lKcDR5EyghXCxgyC9mm_mVMIjAbmGoZtw.png\"}},\"legendStatistics\":{\"legendTrophies\":1348,\"previousSeason\":{\"id\":\"2020-01\",\"rank\":230843,\"trophies\":5147},\"bestSeason\":{\"id\":\"2019-06\",\"rank\":32473,\"trophies\":5557},\"currentSeason\":{\"rank\":432,\"trophies\":5223}},\"achievements\":[{\"name\":\"Bigger Coffers\",\"stars\":3,\"value\":14,\"target\":10,\"info\":\"Upgrade a Gold Storage to level 10\",\"completionInfo\":\"Highest Gold Storage level: 14\",\"village\":\"home\"},{\"name\":\"Get those Goblins!\",\"stars\":3,\"value\":183,\"target\":150,\"info\":\"Win 150 Stars on the Campaign Map\",\"completionInfo\":\"Stars in Campaign Map: 183\",\"village\":\"home\"},{\"name\":\"Bigger & Better\",\"stars\":3,\"value\":13,\"target\":8,\"info\":\"Upgrade Town Hall to level 8\",\"completionInfo\":\"Current Town Hall level: 13\",\"village\":\"home\"},{\"name\":\"Nice and Tidy\",\"stars\":3,\"value\":6309,\"target\":500,\"info\":\"Remove 500 obstacles (trees, rocks, bushes)\",\"completionInfo\":\"Total obstacles removed: 6309\",\"village\":\"home\"},{\"name\":\"Release the Beasts\",\"stars\":3,\"value\":1,\"target\":1,\"info\":\"Unlock Dragon in the Barracks\",\"completionInfo\":null,\"village\":\"home\"},{\"name\":\"Gold Grab\",\"stars\":3,\"value\":2000000000,\"target\":100000000,\"info\":\"Steal 100000000 gold\",\"completionInfo\":\"Total Gold looted: 2000000000\",\"village\":\"home\"},{\"name\":\"Elixir Escapade\",\"stars\":3,\"value\":2000000000,\"target\":100000000,\"info\":\"Steal 100000000 elixir\",\"completionInfo\":\"Total Elixir looted: 2000000000\",\"village\":\"home\"},{\"name\":\"Sweet Victory!\",\"stars\":3,\"value\":5608,\"target\":1250,\"info\":\"Achieve a total of 1250 trophies in Multiplayer battles\",\"completionInfo\":\"Trophy record: 5608\",\"village\":\"home\"},{\"name\":\"Empire Builder\",\"stars\":3,\"value\":9,\"target\":4,\"info\":\"Upgrade Clan Castle to level 4\",\"completionInfo\":\"Current Clan Castle level: 9\",\"village\":\"home\"},{\"name\":\"Wall Buster\",\"stars\":3,\"value\":67101,\"target\":2000,\"info\":\"Destroy 2000 Walls in Multiplayer battles\",\"completionInfo\":\"Total walls destroyed: 67101\",\"village\":\"home\"},{\"name\":\"Humiliator\",\"stars\":3,\"value\":7034,\"target\":2000,\"info\":\"Destroy 2000 Town Halls in Multiplayer battles\",\"completionInfo\":\"Total Town Halls destroyed: 7034\",\"village\":\"home\"},{\"name\":\"Union Buster\",\"stars\":3,\"value\":46295,\"target\":2500,\"info\":\"Destroy 2500 Builder's Huts in Multiplayer battles\",\"completionInfo\":\"Total Builder's Huts destroyed: 46295\",\"village\":\"home\"},{\"name\":\"Conqueror\",\"stars\":3,\"value\":12564,\"target\":5000,\"info\":\"Win 5000 Multiplayer battles\",\"completionInfo\":\"Total multiplayer battles won: 12564\",\"village\":\"home\"},{\"name\":\"Unbreakable\",\"stars\":3,\"value\":5038,\"target\":5000,\"info\":\"Successfully defend against 5000 attacks\",\"completionInfo\":\"Total defenses won: 5038\",\"village\":\"home\"},{\"name\":\"Friend in Need\",\"stars\":3,\"value\":257977,\"target\":25000,\"info\":\"Donate 25000 Clan Castle capacity worth of reinforcements\",\"completionInfo\":\"Total capacity donated: 257977\",\"village\":\"home\"},{\"name\":\"Mortar Mauler\",\"stars\":3,\"value\":23882,\"target\":5000,\"info\":\"Destroy 5000 Mortars in Multiplayer battles\",\"completionInfo\":\"Total Mortars destroyed: 23882\",\"village\":\"home\"},{\"name\":\"Heroic Heist\",\"stars\":3,\"value\":19186651,\"target\":1000000,\"info\":\"Steal 1000000 Dark Elixir\",\"completionInfo\":\"Total Dark Elixir looted: 19186651\",\"village\":\"home\"},{\"name\":\"League All-Star\",\"stars\":3,\"value\":22,\"target\":1,\"info\":\"Become a Champion!\",\"completionInfo\":null,\"village\":\"home\"},{\"name\":\"X-Bow Exterminator\",\"stars\":3,\"value\":11376,\"target\":2500,\"info\":\"Destroy 2500 X-Bows in Multiplayer battles\",\"completionInfo\":\"Total X-Bows destroyed: 11376\",\"village\":\"home\"},{\"name\":\"Firefighter\",\"stars\":2,\"value\":4689,\"target\":5000,\"info\":\"Destroy 5000 Inferno Towers in Multiplayer battles\",\"completionInfo\":\"Total Inferno Towers destroyed: 4689\",\"village\":\"home\"},{\"name\":\"War Hero\",\"stars\":3,\"value\":1781,\"target\":1000,\"info\":\"Score 1000 Stars for your clan in Clan War battles\",\"completionInfo\":\"Total Stars scored for clan in Clan War battles: 1781\",\"village\":\"home\"},{\"name\":\"Treasurer\",\"stars\":3,\"value\":902907446,\"target\":100000000,\"info\":\"Collect 100000000 gold from the Treasury\",\"completionInfo\":\"Total gold collected in Clan War bonuses: 902907446\",\"village\":\"home\"},{\"name\":\"Anti-Artillery\",\"stars\":2,\"value\":1785,\"target\":2000,\"info\":\"Destroy 2000 Eagle Artilleries in Multiplayer battles\",\"completionInfo\":\"Total Eagle Artilleries destroyed: 1785\",\"village\":\"home\"},{\"name\":\"Sharing is caring\",\"stars\":2,\"value\":3136,\"target\":10000,\"info\":\"Donate 10000 spell storage capacity worth of spells\",\"completionInfo\":\"Total spell capacity donated: 3136\",\"village\":\"home\"},{\"name\":\"Keep your village safe\",\"stars\":0,\"value\":0,\"target\":1,\"info\":\"Connect your account to a social network for safe keeping.\",\"completionInfo\":\"Completed!\",\"village\":\"home\"},{\"name\":\"Master Engineering\",\"stars\":3,\"value\":9,\"target\":8,\"info\":\"Upgrade Builder Hall to level 8\",\"completionInfo\":\"Current Builder Hall level: 9\",\"village\":\"builderBase\"},{\"name\":\"Next Generation Model\",\"stars\":3,\"value\":1,\"target\":1,\"info\":\"Unlock Super P.E.K.K.A in the Builder Barracks\",\"completionInfo\":null,\"village\":\"builderBase\"},{\"name\":\"Un-Build It\",\"stars\":2,\"value\":1764,\"target\":2000,\"info\":\"Destroy 2000 Builder Halls in Versus battles\",\"completionInfo\":\"Total Builder Halls destroyed: 1764\",\"village\":\"builderBase\"},{\"name\":\"Champion Builder\",\"stars\":3,\"value\":3284,\"target\":3000,\"info\":\"Achieve a total of 3000 trophies in Versus battles\",\"completionInfo\":\"Versus Trophy record: 3284\",\"village\":\"builderBase\"},{\"name\":\"High Gear\",\"stars\":3,\"value\":3,\"target\":3,\"info\":\"Gear Up 3 buildings using the Master Builder\",\"completionInfo\":\"Total buildings geared up: 3\",\"village\":\"builderBase\"},{\"name\":\"Hidden Treasures\",\"stars\":3,\"value\":1,\"target\":1,\"info\":\"Rebuild Battle Machine\",\"completionInfo\":null,\"village\":\"builderBase\"},{\"name\":\"Games Champion\",\"stars\":2,\"value\":59840,\"target\":100000,\"info\":\"Earn 100000 points in Clan Games\",\"completionInfo\":\"Total Clan Games points: 59840\",\"village\":\"home\"},{\"name\":\"Dragon Slayer\",\"stars\":0,\"value\":0,\"target\":1,\"info\":\"Slay the Giant Dragon\",\"completionInfo\":null,\"village\":\"home\"},{\"name\":\"War League Legend\",\"stars\":2,\"value\":198,\"target\":250,\"info\":\"Score 250 Stars for your clan in War League battles\",\"completionInfo\":\"Total Stars scored for clan in War League battles: 198\",\"village\":\"home\"},{\"name\":\"Keep your village safe\",\"stars\":0,\"value\":0,\"target\":1,\"info\":\"Connect your account to Supercell ID for safe keeping.\",\"completionInfo\":\"Completed!\",\"village\":\"home\"},{\"name\":\"Well Seasoned\",\"stars\":1,\"value\":13760,\"target\":15000,\"info\":\"Earn 15000 points in Season Challenges\",\"completionInfo\":\"Total Season Challenges points: 13760\",\"village\":\"home\"},{\"name\":\"Shattered and Scattered\",\"stars\":1,\"value\":210,\"target\":400,\"info\":\"Destroy 400 Scattershots in Multiplayer battles\",\"completionInfo\":\"Total Scattershots destroyed: 210\",\"village\":\"home\"}],\"versusBattleWinCount\":1598,\"labels\":[{\"id\":57000000,\"name\":\"Clan Wars\",\"iconUrls\":{\"small\":\"https://api-assets.clashofclans.com/labels/64/ZxJp9606Vl1sa0GHg5JmGp8TdHS4l0jE4WFuil1ENvA.png\",\"medium\":\"https://api-assets.clashofclans.com/labels/128/ZxJp9606Vl1sa0GHg5JmGp8TdHS4l0jE4WFuil1ENvA.png\"}},{\"id\":57000001,\"name\":\"Clan War League\",\"iconUrls\":{\"small\":\"https://api-assets.clashofclans.com/labels/64/JOzAO4r91eVaJELAPB-iuAx6f_zBbRPCLM_ag5mpK4s.png\",\"medium\":\"https://api-assets.clashofclans.com/labels/128/JOzAO4r91eVaJELAPB-iuAx6f_zBbRPCLM_ag5mpK4s.png\"}},{\"id\":57000002,\"name\":\"Trophy Pushing\",\"iconUrls\":{\"small\":\"https://api-assets.clashofclans.com/labels/64/tINt65InVEc35rFYkxqFQqGDTsBpVRqY9K7BJf5kr4A.png\",\"medium\":\"https://api-assets.clashofclans.com/labels/128/tINt65InVEc35rFYkxqFQqGDTsBpVRqY9K7BJf5kr4A.png\"}}],\"troops\":[{\"name\":\"Barbarian\",\"level\":8,\"maxLevel\":8,\"village\":\"home\"},{\"name\":\"Archer\",\"level\":8,\"maxLevel\":8,\"village\":\"home\"},{\"name\":\"Goblin\",\"level\":7,\"maxLevel\":7,\"village\":\"home\"},{\"name\":\"Giant\",\"level\":9,\"maxLevel\":9,\"village\":\"home\"},{\"name\":\"Wall Breaker\",\"level\":8,\"maxLevel\":9,\"village\":\"home\"},{\"name\":\"Balloon\",\"level\":9,\"maxLevel\":9,\"village\":\"home\"},{\"name\":\"Wizard\",\"level\":9,\"maxLevel\":9,\"village\":\"home\"},{\"name\":\"Healer\",\"level\":6,\"maxLevel\":6,\"village\":\"home\"},{\"name\":\"Dragon\",\"level\":8,\"maxLevel\":8,\"village\":\"home\"},{\"name\":\"P.E.K.K.A\",\"level\":8,\"maxLevel\":8,\"village\":\"home\"},{\"name\":\"Minion\",\"level\":8,\"maxLevel\":8,\"village\":\"home\"},{\"name\":\"Hog Rider\",\"level\":10,\"maxLevel\":10,\"village\":\"home\"},{\"name\":\"Valkyrie\",\"level\":7,\"maxLevel\":7,\"village\":\"home\"},{\"name\":\"Golem\",\"level\":9,\"maxLevel\":9,\"village\":\"home\"},{\"name\":\"Witch\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Lava Hound\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Bowler\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Baby Dragon\",\"level\":6,\"maxLevel\":6,\"village\":\"home\"},{\"name\":\"Miner\",\"level\":6,\"maxLevel\":7,\"village\":\"home\"},{\"name\":\"Raged Barbarian\",\"level\":14,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Sneaky Archer\",\"level\":14,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Beta Minion\",\"level\":14,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Boxer Giant\",\"level\":14,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Bomber\",\"level\":13,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Super P.E.K.K.A\",\"level\":6,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Cannon Cart\",\"level\":15,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Drop Ship\",\"level\":16,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Baby Dragon\",\"level\":13,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Night Witch\",\"level\":14,\"maxLevel\":18,\"village\":\"builderBase\"},{\"name\":\"Wall Wrecker\",\"level\":3,\"maxLevel\":3,\"village\":\"home\"},{\"name\":\"Battle Blimp\",\"level\":3,\"maxLevel\":3,\"village\":\"home\"},{\"name\":\"Yeti\",\"level\":3,\"maxLevel\":3,\"village\":\"home\"},{\"name\":\"Ice Golem\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Electro Dragon\",\"level\":4,\"maxLevel\":4,\"village\":\"home\"},{\"name\":\"Stone Slammer\",\"level\":3,\"maxLevel\":3,\"village\":\"home\"},{\"name\":\"Siege Barracks\",\"level\":1,\"maxLevel\":4,\"village\":\"home\"}],\"heroes\":[{\"name\":\"Barbarian King\",\"level\":66,\"maxLevel\":70,\"village\":\"home\"},{\"name\":\"Archer Queen\",\"level\":69,\"maxLevel\":70,\"village\":\"home\"},{\"name\":\"Grand Warden\",\"level\":44,\"maxLevel\":50,\"village\":\"home\"},{\"name\":\"Battle Machine\",\"level\":17,\"maxLevel\":30,\"village\":\"builderBase\"},{\"name\":\"Royal Champion\",\"level\":9,\"maxLevel\":20,\"village\":\"home\"}],\"spells\":[{\"name\":\"Lightning Spell\",\"level\":7,\"maxLevel\":7,\"village\":\"home\"},{\"name\":\"Healing Spell\",\"level\":8,\"maxLevel\":8,\"village\":\"home\"},{\"name\":\"Rage Spell\",\"level\":6,\"maxLevel\":6,\"village\":\"home\"},{\"name\":\"Jump Spell\",\"level\":4,\"maxLevel\":4,\"village\":\"home\"},{\"name\":\"Freeze Spell\",\"level\":7,\"maxLevel\":7,\"village\":\"home\"},{\"name\":\"Poison Spell\",\"level\":6,\"maxLevel\":6,\"village\":\"home\"},{\"name\":\"Earthquake Spell\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Haste Spell\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Clone Spell\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"},{\"name\":\"Skeleton Spell\",\"level\":7,\"maxLevel\":7,\"village\":\"home\"},{\"name\":\"Bat Spell\",\"level\":5,\"maxLevel\":5,\"village\":\"home\"}]}\r\n" + 
			"";
}
