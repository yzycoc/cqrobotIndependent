package tgc.edu.yzy.cocApi.entity.ClansClanTag;


public class Labels {
	private String id;
	private String name;
	private IconUrls iconUrls;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public IconUrls getIconUrls() {
		return iconUrls;
	}
	public void setIconUrls(IconUrls iconUrls) {
		this.iconUrls = iconUrls;
	}
}
