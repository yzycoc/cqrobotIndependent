package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

public class CurrentSeason {
	private String id;//时间
	private String rank;//排名
	private String trophies;//奖杯
	public String getTrophies() {
		return trophies;
	}

	public void setTrophies(String trophies) {
		this.trophies = trophies;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}
	
	
}
