package tgc.edu.yzy.cocApi.custom;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import org.springframework.util.FileCopyUtils;

import tgc.edu.yzy.custom.BackEndHttpRequest;
import tgc.edu.yzy.custom.TimeUtils;

public class TxtCoc {
	/***
	 * 
	 * @param sendGetCoc 需要储存的内容
	 * @param string 名字
	 */
	public static void coc(String sendGetCoc, String string) {
		String path = "c:\\coc\\"+string+".json";
        File file = new File(path);
        if(!file.exists()){
            file.getParentFile().mkdirs();          
        }
        try {
			file.createNewFile();
			FileWriter fw = new FileWriter(file, true);
	        BufferedWriter bw = new BufferedWriter(fw);
	        bw.write(TimeUtils.getStringDate()+sendGetCoc);
	        bw.flush();
	        bw.close();
	        fw.close();
		} catch (IOException e) {
			System.out.println("写TXT文件报错"+string);
		}
	}
	/***
	 * 
	 * @param string 村庄或者部落标签 文件名字
	 * @return
	 */
	public static String getCoc(String string) {
		String path = "c:\\coc\\"+string+".json";
        File file = new File(path);
        if(!file.exists()){
            file.getParentFile().mkdirs();          
        }
        FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
			String str = br.readLine();
			fr.close();
			br.close();
			return str;
		} catch (Exception e) {
			System.out.println("读取文件失败");
			return "";
		}
	}

	/****
	 *	 已废弃
	 * Imagename(图片的名称)
	 * @param Imagename
	 * @return 
	 */
	public static BufferedImage getImage(String Imagename,String url) {
		Imagename = "C:\\cocutil\\" +Imagename+".png";
		BufferedImage image = null;
		File file = new File(Imagename);
		//当这个文件为空时，去网上下载下来
		if(!file.exists()){
			try {
				BackEndHttpRequest.sendImageGet(url, file);
				file = new File(Imagename);
			} catch (Exception e) {
				e.printStackTrace();
				file = new File("C:\\cocutil\\error.png");
			}
	    }
		try{
			image = ImageIO.read(file);
		}catch(Exception e){
		  System.out.println("获取图像失败"+e.toString());
		}
		return image;
	}
	/***
	 * 
	 * @param filename 文件名称
	 * @param cqPath 酷Q文件的地址
	 * @param string 
	 * @return 
	 */
	public static boolean toTxt(String filename, String cqPath, String string) {
		try {
			InputStream input = new FileInputStream("C:\\cocutil\\"+string+"\\"+filename); 
			FileOutputStream output = new FileOutputStream(cqPath+"\\data\\image\\image\\"+string+"\\"+filename); 
			FileCopyUtils.copy(input, output);
			return true;
		} catch (Exception e) {
			return false;
		}
		 
	}
	

}
