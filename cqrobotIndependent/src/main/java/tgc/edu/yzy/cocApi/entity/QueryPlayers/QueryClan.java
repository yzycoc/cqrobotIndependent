package tgc.edu.yzy.cocApi.entity.QueryPlayers;

import java.util.ArrayList;
import java.util.List;

public class QueryClan {
	private String name;
	private String tag;
	private String number;
	private Boolean isWarLogPublic;
	private String locationname;
	private List<QueryPlayers> query = new ArrayList<>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	public Boolean getIsWarLogPublic() {
		return isWarLogPublic;
	}
	public void setIsWarLogPublic(Boolean isWarLogPublic) {
		this.isWarLogPublic = isWarLogPublic;
	}
	public String getLocationname() {
		return locationname;
	}
	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}
	public List<QueryPlayers> getQuery() {
		return query;
	}
	public void setQuery(List<QueryPlayers> query) {
		this.query = query;
	}
	
}
