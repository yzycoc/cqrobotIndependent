package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

public class PreviousSeason {
	private String id;//时间
	private String rank;//排名
	private String trophies;//最高奖杯
	/***
	 * 达到的时间
	 * @return
	 */
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	/***
	 * 获得的传奇奖杯
	 * @return
	 */
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	/***
	 * 最高奖杯
	 * @return
	 */
	public String getTrophies() {
		return trophies;
	}
	public void setTrophies(String trophies) {
		this.trophies = trophies;
	}
}
