package tgc.edu.yzy.cocApi.entity.ClansPlayersTag;

public class Troops {
	private String name;//兵种名称
	private String level;//自己家的兵的等级
	private String maxLevel;//兵最高的等级
	private String Village;//家乡 home 夜世界 builderBase
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getMaxLevel() {
		return maxLevel;
	}
	public void setMaxLevel(String maxLevel) {
		this.maxLevel = maxLevel;
	}
	public String getVillage() {
		return Village;
	}
	public void setVillage(String village) {
		Village = village;
	}
	
}
