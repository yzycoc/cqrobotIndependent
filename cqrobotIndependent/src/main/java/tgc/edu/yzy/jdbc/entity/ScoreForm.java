package tgc.edu.yzy.jdbc.entity;

public class ScoreForm {
	private Integer id;
	private String createName; // 更新人
	private String createDate; // 更新时间
	private Integer number;
	private String qqNum;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getQqNum() {
		return qqNum;
	}
	public void setQqNum(String qqNum) {
		this.qqNum = qqNum;
	}
	public ScoreForm() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ScoreForm(Integer id, String createName, String createDate, Integer number, String qqNum) {
		super();
		this.id = id;
		this.createName = createName;
		this.createDate = createDate;
		this.number = number;
		this.qqNum = qqNum;
	}
	
	
}
