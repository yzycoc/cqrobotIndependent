package tgc.edu.yzy.jdbc.entity;

public class ScoureUserForm {
	private Integer id;
	private String qqnumber;//QQ号
	private String datasum;//购买数量 --- 或者购买的QQ群
	private String type;//类型
	private String createName;
	
	
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getQqnumber() {
		return qqnumber;
	}
	public void setQqnumber(String qqnumber) {
		this.qqnumber = qqnumber;
	}
	public String getDatasum() {
		return datasum;
	}
	public void setDatasum(String datasum) {
		this.datasum = datasum;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
}
