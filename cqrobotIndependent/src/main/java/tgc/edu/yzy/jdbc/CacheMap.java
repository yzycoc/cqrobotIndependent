package tgc.edu.yzy.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import tgc.edu.yzy.custom.TimeUtils;

public class CacheMap {
	public String get(String key) {
		String sql ="SELECT * from cachemap t where t.key=?";
		Connection conn = JDBC.ljsjk();
		PreparedStatement stemt;
		try {
			stemt = conn.prepareStatement(sql);
			stemt.setString(1, key);
			ResultSet rs = stemt.executeQuery();
			if(rs.next()) {
				String string = rs.getString(4);
				if(TimeUtils.timedifference(string) < 0) {
					delete(rs.getInt(1));
					stemt.close();
					conn.close();
					return null;
				}else {
					String value = rs.getString(3);
					stemt.close();
					conn.close();
					return value;
				}
			}
			stemt.close();
			conn.close();
			return null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void delete(int id) {
		String sql = "DELETE FROM cachemap WHERE id = ?";
		Connection conn = JDBC.ljsjk();
		PreparedStatement stemt;
		try {
			stemt = conn.prepareStatement(sql);
			stemt.setInt(1, id);
			stemt.execute();
			stemt.close(); 
			conn.close();                                       
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("resource")
	public void save(String key,String value,String date) {
		String fin ="SELECT * from cachemap t where t.key=?";;
		String sql = "INSERT INTO cachemap VALUES(null,?,?,?)";
		Connection conn = JDBC.ljsjk();
		PreparedStatement stemt;
		try {
			stemt = conn.prepareStatement(fin);
			stemt.setString(1, key);
			ResultSet rs = stemt.executeQuery();
			if(rs.next()) {
				sql = "UPDATE cachemap t SET t.key=?,t.value=?,t.date=?  where t.id=?";
				stemt = conn.prepareStatement(sql);
				stemt.setString(1, key);
				stemt.setString(2, value);
				stemt.setString(3, date);
				stemt.setInt(4, rs.getInt(1));
				stemt.execute();
				stemt.close();
				conn.close();
			}else {
				stemt = conn.prepareStatement(sql);
				stemt.setString(1, key);
				stemt.setString(2, value);
				stemt.setString(3, date);
				stemt.execute();
				stemt.close();
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/*public static void main(String args[]) throws InterruptedException {
		CacheMap map = new CacheMap();
		//String findByKey = map.findByKey("22");  
		String endMin = TimeUtils.endMin(1);
		map.save("123","234", endMin);
		System.out.println(map.get("123"));
		Thread.sleep(1000);
		System.out.println(map.get("123"));
	}
*/
	private void remAll() {
		String sql = "DELETE FROM cachemap WHERE date<? ";
		Connection conn = JDBC.ljsjk();
		PreparedStatement stemt;
		try {
			stemt = conn.prepareStatement(sql);
			stemt.setString(1, TimeUtils.getStringDate());
			stemt.execute();
			stemt.close(); 
			conn.close();                                       
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
