package tgc.edu.yzy.jdbc.score;

/***
 * 积分商城
 * @author 936642284
 *
 */
public class ScireStore{
	private Integer id;
	private String name;//兑换名称
	private Integer number=0;//兑换所需要的积分
	private String unit;//计量单位
	private String nameRemark;//解释
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getNameRemark() {
		return nameRemark;
	}
	public void setNameRemark(String nameRemark) {
		this.nameRemark = nameRemark;
	}
	@Override
	public String toString() {
		return "ScireStore [name=" + name + ", number=" + number + ", unit=" + unit + ", nameRemark=" + nameRemark
				+ "]";
	}
	
	
	
}
