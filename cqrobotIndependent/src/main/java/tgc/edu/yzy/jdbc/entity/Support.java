package tgc.edu.yzy.jdbc.entity;

import java.util.ArrayList;
import java.util.List;

/****
 * 打赏支持
 * @author yao936642284
 *
 */
public class Support{
	private List<String> grouplist = new ArrayList<String>();
	private List<String> qq = new ArrayList<String>();
	public List<String> getGrouplist() {
		return grouplist;
	}
	public void setGrouplist(List<String> grouplist) {
		this.grouplist = grouplist;
	}
	public List<String> getQq() {
		return qq;
	}
	public void setQq(List<String> qq) {
		this.qq = qq;
	}
	
	
}
