package tgc.edu.yzy.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import tgc.edu.yzy.bean.GroupList;

@Mapper
public interface GroupListMapper extends BaseMapper<GroupList>{
	@Select("DELETE from group_list where isfalsetrue = '离群'")
	void updateAll();

}
