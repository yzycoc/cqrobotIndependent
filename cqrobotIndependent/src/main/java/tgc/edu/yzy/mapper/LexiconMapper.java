package tgc.edu.yzy.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import tgc.edu.yzy.bean.Lexicon;
@Mapper
public interface LexiconMapper extends BaseMapper<Lexicon>{
    @Select("SELECT * FROM lexicon WHERE id = '1'")
	public Lexicon leaveJoins();
    @Select("SELECT  * FROM lexicon WHERE id IN (SELECT t.id FROM (SELECT id FROM lexicon WHERE type = '完全' AND  antistop=#{antistop} AND isuser = 1 AND number = '随机' ORDER BY RAND() LIMIT 1 ) AS t )")
	public Lexicon findByNumber(String antistop);
    @Select("DELETE FROM lexicon  WHERE create_name like '%批量导入%'")
	public void deleteAll();
}
