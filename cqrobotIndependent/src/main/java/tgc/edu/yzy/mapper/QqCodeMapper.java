package tgc.edu.yzy.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import tgc.edu.yzy.bean.QqCode;
@Mapper
public interface QqCodeMapper extends BaseMapper<QqCode>{

}
