package tgc.edu.yzy.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import tgc.edu.yzy.bean.OpenLayout;
@Mapper
public interface OpenLayoutMapper extends BaseMapper<OpenLayout>{
	@Delete("delete FROM open_layout WHERE create_name = '晴天阵型'")
	public void delete();
	@Select("SELECT  * FROM open_layout WHERE id IN (SELECT t.id FROM (SELECT id FROM open_layout  ORDER BY RAND() LIMIT 1 ) AS t )")
	public OpenLayout getSuiji();

}
