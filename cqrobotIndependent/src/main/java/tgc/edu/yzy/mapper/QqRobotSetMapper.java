package tgc.edu.yzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import tgc.edu.yzy.bean.QqRobotSet;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QqRobotSetMapper extends BaseMapper<QqRobotSet>{

}
