package tgc.edu.yzy;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.forte.qqrobot.component.forhttpapi.HttpApplication;
import com.forte.qqrobot.log.QQLog;
import com.forte.qqrobot.sender.MsgSender;

import tgc.edu.yzy.controller.MyqqRobot;
import tgc.edu.yzy.custom.BaseConfiguration;


@SpringBootApplication
public class CqrobotIndependentApplication {
	public static MsgSender msgSender;
	public static void main(String[] args) {
		QQLog.setLogBack((m,l,e)->{
			return false;
		});
		SpringApplication.run(CqrobotIndependentApplication.class, args);
		HttpApplication httpApplication= new  HttpApplication();
		httpApplication.run(new MyqqRobot());
		CqrobotIndependentApplication.msgSender = httpApplication.getMsgSender();
        final String[] array;
        final String[] a = array = new String[] { BaseConfiguration.getCqPath() + "\\data\\image\\image\\lscc", BaseConfiguration.getCqPath() + "\\data\\image\\image\\lstj", BaseConfiguration.getCqPath() + "\\data\\image\\image\\bldz", BaseConfiguration.getCqPath() + "\\data\\image\\image\\pz", "C:\\cocutil\\lscc", "C:\\cocutil\\lstj", "C:\\cocutil\\bldz", BaseConfiguration.getCqPath() + "\\data\\image\\image\\user" };
        for (final String string : array) {
            final File file = new File(string);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
	}
}
