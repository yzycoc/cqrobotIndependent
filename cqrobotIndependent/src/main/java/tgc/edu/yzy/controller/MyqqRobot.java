package tgc.edu.yzy.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.forte.qqrobot.anno.depend.AllBeans;
import com.forte.qqrobot.anno.depend.Depend;
import com.forte.qqrobot.beans.messages.result.FriendList;
import com.forte.qqrobot.beans.messages.result.GroupList;
import com.forte.qqrobot.beans.messages.result.inner.Friend;
import com.forte.qqrobot.beans.messages.result.inner.Group;
import com.forte.qqrobot.component.forhttpapi.HttpApp;
import com.forte.qqrobot.component.forhttpapi.HttpConfiguration;
import com.forte.qqrobot.component.forhttpapi.beans.response.Resp_getLoginQQInfo;
import com.forte.qqrobot.component.forhttpapi.http.QQHttpMsgSender;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.Impl.GroupRequestImpl;
import tgc.edu.yzy.Impl.OpenLayoutImpl;
import tgc.edu.yzy.bean.GroupRequest;
import tgc.edu.yzy.bean.Lexicon;
import tgc.edu.yzy.bean.QqRobotSet;
import tgc.edu.yzy.custom.BackEndHttpRequest;
import tgc.edu.yzy.custom.BaseConfiguration;
import tgc.edu.yzy.custom.CacheMap;
import tgc.edu.yzy.custom.CocCustom;
import tgc.edu.yzy.custom.QqCqcustom;
import tgc.edu.yzy.custom.SpringContextUtil;
import tgc.edu.yzy.custom.TimeUtils;
import tgc.edu.yzy.custom.UpdateGroupList;
import tgc.edu.yzy.custom.music.MusicFactory;
import tgc.edu.yzy.jdbc.XjMysqlJdbc;
import tgc.edu.yzy.service.LexiconService;
import tgc.edu.yzy.service.QqRobotSetService;

@AllBeans
public class MyqqRobot implements HttpApp{
	public static String administratorQQ = "936642284";
	public static CQCodeUtil cqCodeUtil;
	
	/***
	 * 储存的模糊词库
	 */
	public static List<Lexicon> list = new ArrayList<>();
	public LexiconService lservice = SpringContextUtil.getBean(LexiconService.class);
	//public static String clashofclansforecaster[] = {"","","1"};
	//用来储存生成了多少张图片
	public static HashMap<String, Integer> Image_SUM = new HashMap<>();
	/***
	 * QQ群调用频率
	 */
	public static CacheMap<String, Integer > groupPl = new CacheMap<>();
	
	// 保存别人拉机器人，是否是经过审核！
	public static List<String> groupList = new ArrayList<>();
	public static String URL = "";
	/***
	 * 黑名单状态
	 * 1.机器人QQ号 直接退群不提示
	 * 2.不需要理会的黑名单 二级
	 * 3.超级黑名单，提示并且退群
	 * 以及 机器人状态
	 */
	public static Map<String, Integer> myrobotqq = new HashMap<String,Integer>();
	private GroupRequestImpl grouprequest = SpringContextUtil.getBean(GroupRequestImpl.class);
	public static List<String> lexiconNo= new ArrayList<>();//防止接口嘴臭
	
	public static Boolean GROUP_GoTo = false;//QQ群自动同意
	public static Boolean PRIVATE_GoTo = true;//QQ好友自动同意
	
	//public static Boolean tulingNY = false;//QQ好友自动同意
	
	
	/***
	 * 管理员
	 */
	public static List<String> adminList = new ArrayList<>();
	static {
		adminList.add("936642284");
		adminList.add("1723046795");
		adminList.add("1783164177");
		adminList.add("1343257671");
		adminList.add("2788812663");
		adminList.add("2388793739");
	}
	
	@Override
	public void before(HttpConfiguration link) {
		/*link.setServerPort(15505);
		link.setJavaPort(15551);*/
		//直接在数据库取出配置项目
		QqRobotSetService bean = SpringContextUtil.getBean(QqRobotSetService.class);
		QqRobotSet entity = bean.getById(1);
		if(entity!=null) {
			System.out.println("数据库配置获取成功！");
			if(entity.getLocalqqnick()!=null&&entity.getLocalqqnick().length()>1) {
				link.setLocalQQNick(entity.getLocalqqnick());
				systemout("Nick",entity.getLocalqqnick());
			}
			if(entity.getLocalqqcode()!=null&&entity.getLocalqqcode().length()>1) {
				link.setLocalQQCode(entity.getLocalqqcode());
				systemout("Code",entity.getLocalqqcode());
			}
			if(entity.getCqPath()!=null&&entity.getCqPath().length()>1) {
				BaseConfiguration.setCqPath(entity.getCqPath());
				link.setCqPath(entity.getCqPath());
				systemout("CqPath",entity.getCqPath());
			}
			if(entity.getEncode()!=null&&entity.getEncode().length()>1) {
				link.setEncode(entity.getEncode());
				systemout("Encode",entity.getEncode());
			}
			if(entity.getLinkIp()!=null&&entity.getLinkIp().length()>1) {
				link.setIp(entity.getLinkIp());
				systemout("LinkIp",entity.getLinkIp());
			}
			
			if(entity.getBacklog()!=null&&entity.getBacklog()>0) {
				link.setBacklog(entity.getBacklog());
				systemout("设置重连次数Backlog",entity.getBacklog()+"");
			}
			
			if(entity.getServerPath()!=null&&entity.getServerPath().length()>1) {
				link.setServerPath(entity.getServerPath());
				systemout("配置java服务器的请求路径ServerPath",entity.getServerPath());
			}
			
			if(entity.getServerPort()!=null&&entity.getServerPort()>0) {
				link.setServerPort(entity.getServerPort());
				systemout("配置酷Q端的端口地址ServerPort默认8888:",entity.getServerPort()+"");
			}
			
			if(entity.getJavaPort()!=null&&entity.getJavaPort()>0) {
				link.setJavaPort(entity.getJavaPort());
				systemout("配置java服务器的监听端口JavaPort默认15514:",entity.getJavaPort()+"");
			}
			
			if(StringUtils.isNotEmpty(entity.getApi())) {
				BackEndHttpRequest.API = " "+entity.getApi();
				systemout("配置COC_API默认:",BackEndHttpRequest.API);
			}
			
			if(StringUtils.isNotEmpty(entity.getAdministrator())) {
				administratorQQ = entity.getAdministrator();
				systemout("管理员QQ:",administratorQQ);
			}
			
			//设置重连次数
			//link.setBacklog(6);
			//configuration.setJavaPort(15514);
			//link.setServerPath(null);
			/*link.setJavaPort();//配置java服务器的监听端口，默认为15514*/	
			//.setServerPort(9999);//配置酷Q端的端口地址，默认为8877
			}
	}
	/*FriendList friendList = sender.GETTER.getFriendList();
	System.out.println(friendList.getOriginalData());*/
	//QQHttpMsgSender msg = new QQHttpMsgSender(null);
	@Override
	public void after(CQCodeUtil cqCodeUtil, MsgSender sender) {
		//识别用户信息
		BaseConfiguration.setLocalQQCode(sender.GETTER.getLoginQQInfo().getQQ());
		//System.out.println(friendList.get);
		System.out.println("启动成功，开始初始化操作。");
		
		try {
			MyqqRobot.cqCodeUtil = cqCodeUtil;
			//初始化鱼情
			Boolean goyuqing = false;
			while (goyuqing==false) {
				goyuqing =  CocCustom.getYuqing();
			}
			System.out.println(TimeUtils.getStringDate()+"始初始化词库！");
			MyqqRobot.list = lservice.query().eq("type", "模糊").eq("isuser", "1").list();
			System.out.println(TimeUtils.getStringDate()+"始初始化阵型库！");
			OpenLayoutImpl openlayout = SpringContextUtil.getBean(OpenLayoutImpl.class);
			String updateMap = openlayout.updateMap();
			String blackList = XjMysqlJdbc.blackList();
			XjMysqlJdbc xj = new XjMysqlJdbc();
			xj.lexiconNo();
			System.out.println(TimeUtils.getStringDate()+"始初始化群人数QQ好友人数！");
			String success = QqCqcustom.success(sender,true);
			System.out.println(TimeUtils.getStringDate()+"发送初始化消息！");
			sender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ, "词库已加载完毕！\n"
					+ "全发："+MyqqRobot.list.size()+"条数据\n"
					+success+"\n"
					+ updateMap+"\n"+blackList+"\n聊天违禁词："+MyqqRobot.lexiconNo.size()+"条"+"[CQ:image,file=yuqing.jpg]");
		} catch (Exception e) {	
			sender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ, "词库加载失败");
			e.printStackTrace();
		}
		List<GroupRequest> G_R = grouprequest.query().eq("type","1").list();
		for (GroupRequest groupRequest : G_R) {
			grouprequest.removeById(groupRequest);
		}
		/*java.util.Date time = new java.util.Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH");
		String dateString = formatter.format(time);
		int parseInt = Integer.parseInt(dateString);
		if(parseInt>=18) {
			MyqqRobot.tulingNY = false;
		}*/
		
	}
	public void systemout(String code,String value) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
		System.out.println(df.format(new Date())+" QQRobot  配置属性[ "+code+" ]配置内容[ "+value+" ]");
	}
}
