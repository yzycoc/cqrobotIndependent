package tgc.edu.yzy.controller;

import tgc.edu.yzy.jdbc.*;
import com.forte.qqrobot.utils.*;
import com.forte.qqrobot.sender.*;
import tgc.edu.yzy.custom.*;
import tgc.edu.yzy.bean.*;
import tgc.edu.yzy.cocApi.entity.ClansPlayersTag.*;
import tgc.edu.yzy.cocApi.custom.*;
import tgc.edu.yzy.cocApi.entity.ClansClanTag.*;
import com.forte.qqrobot.beans.messages.msgget.*;

public class Custom
{
    public static CocBindingJDBC bdservice;
    /***
     * 查询村庄
     * @param split
     * @param msg
     * @param cqCodeUtil
     * @param msgsender
     * @param utils
     */
    public static void ClansPlayer(final String[] split, final GroupMsg msg, final CQCodeUtil cqCodeUtil, final MsgSender msgsender) {
        final boolean restrict = QqCqcustom.groupPb(msg.getQQ());
        if (restrict) {
            msgsender.SENDER.sendGroupMsg(msg.getMsg(), cqCodeUtil.getCQCode_At(msg.getQQ()) + " 检测你多次调用机器人，为降低发言频率，现15分钟不予处理你的消息！\\n解除时间：" + TimeUtils.endMinTime(15));
            return;
        }
        split[1] = split[1].toUpperCase();
        split[2] = split[2].replaceAll(" ", "").replaceAll("\t", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
        final CocBinding qq = Custom.bdservice.get(msg.getQQ(), split[2]);
        if (qq != null) {
            msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " [CQ:emoji,id=10060]绑定失败，同一个代称不能同时绑定多个标签！");
            return;
        }
        msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " 正在查询[CQ:emoji,id=127758][CQ:emoji,id=127758][CQ:emoji,id=127758]\n[#" + split[1] + "] 村庄是否真实存在;");
        final ClansPlayersTag findAll = ClansPlayersTagCustom.findAll(split[1]);
        if (findAll.getDataisnull()) {
            final CocBinding bd = new CocBinding();
            bd.setMsg(split[2]);
            bd.setQqcode(msg.getQQ());
            bd.setTag(split[1]);
            bd.setType("村庄");
            Custom.bdservice.save(bd);
            msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " \n[CQ:emoji,id=9989]以后你对我下达指令\n[CQ:emoji,id=128073]" + split[2] + "[CQ:emoji,id=128072]\n我就告诉你[CQ:emoji,id=127918]'" + findAll.getName() + "' " + findAll.getTag() + "的村庄信息！\n[CQ:face,id=183]记住了哦,这是我们两个人之间的暗号！");
        }
        else {
            msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " [CQ:emoji,id=10060]出现以下问题，导致绑定失败。\n" + findAll.getError());
        }
    }
    
    public static void ClansClan(final String[] split, final GroupMsg msg, final CQCodeUtil cqCodeUtil, final MsgSender msgsender) {
        final boolean restrict = QqCqcustom.groupPb(msg.getQQ());
        if (restrict) {
            msgsender.SENDER.sendGroupMsg(msg.getMsg(), cqCodeUtil.getCQCode_At(msg.getQQ()) + " \u68c0\u6d4b\u4f60\u591a\u6b21\u8c03\u7528\u673a\u5668\u4eba\uff0c\u4e3a\u964d\u4f4e\u53d1\u8a00\u9891\u7387\uff0c\u73b015\u5206\u949f\u4e0d\u4e88\u5904\u7406\u4f60\u7684\u6d88\u606f\uff01\n\u89e3\u9664\u65f6\u95f4\uff1a" + TimeUtils.endMinTime(15));
            return;
        }
        split[1] = split[1].toUpperCase();
        split[2] = split[2].replaceAll(" ", "").replaceAll("\t", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
        final CocBinding qq = Custom.bdservice.get(msg.getQQ(), split[2]);
        if (qq != null) {
            msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " [CQ:emoji,id=10060]\u7ed1\u5b9a\u5931\u8d25\uff0c\u540c\u4e00\u4e2a\u89e6\u53d1\u8bcd\u4e0d\u80fd\u540c\u65f6\u7ed1\u5b9a\u591a\u4e2a\u6807\u7b7e\uff01");
            return;
        }
        msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " \u6b63\u5728\u67e5\u8be2[#" + split[1] + "] \u90e8\u843d\u662f\u5426\u771f\u5b9e\u5b58\u5728");
        final ClansClanTag findAll = ClansClanTagCustom.findAll(split[1]);
        if (findAll.getDataisnull()) {
            final CocBinding bd = new CocBinding();
            bd.setMsg(split[2]);
            bd.setQqcode(msg.getQQ());
            bd.setTag(split[1]);
            bd.setType("\u90e8\u843d");
            Custom.bdservice.save(bd);
            msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " \n[CQ:emoji,id=9989] \u5df2\u6210\u529f\u7ed1\u5b9a\u89e6\u53d1\u8bcd" + split[2] + "\u4e8e\u3010" + findAll.getName() + " " + findAll.getTag() + "\u3011\u7684\u90e8\u843d\u4fe1\u606f\uff01\n[CQ:face,id=183]\u8fd9\u4ec5\u662f\u6211\u4eec\u4e24\u4e2a\u4eba\u4e4b\u95f4\u7684\u6697\u53f7\uff01");
        }
        else {
            msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQCode()) + " \n[CQ:emoji,id=10060]\u7ed1\u5b9a\u5931\u8d25\u3002\n" + findAll.getError());
        }
    }
    
    public static Boolean relieve(final String msg, final String QQcode, final String groupqq, final MsgSender msgsender) {
        final CocBinding qq = Custom.bdservice.get(QQcode, msg);
        if (qq != null) {
            Custom.bdservice.removeById(qq);
            msgsender.SENDER.sendGroupMsg(groupqq, "[CQ:at,qq=" + QQcode + "] \u5df2\u6210\u529f\u89e3\u7ed1  '" + msg + "'\uff0c\u4ee5\u540e\u8fd9\u4e2a\u6697\u53f7\u5c31\u65e0\u6548\u4e86\uff01");
            return true;
        }
        return false;
    }
    
    public static void ClansPlayer(final String[] split, final PrivateMsg msg, final CQCodeUtil cqCodeUtil, final MsgSender msgsender) {
        split[1] = split[1].toUpperCase();
        split[2] = split[2].replaceAll(" ", "").replaceAll("\t", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
        final CocBinding qq = Custom.bdservice.get(msg.getQQ(), split[2]);
        if (qq != null) {
            msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:emoji,id=10060]\u7ed1\u5b9a\u5931\u8d25\uff0c\u540c\u4e00\u4e2a\u4ee3\u79f0\u4e0d\u80fd\u540c\u65f6\u7ed1\u5b9a\u591a\u4e2a\u6807\u7b7e\uff01");
            return;
        }
        msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "\u6b63\u5728\u67e5\u8be2[CQ:emoji,id=127758][CQ:emoji,id=127758][CQ:emoji,id=127758]\n[#" + split[1] + "] \u6751\u5e84\u662f\u5426\u771f\u5b9e\u5b58\u5728");
        final ClansPlayersTag findAll = ClansPlayersTagCustom.findAll(split[1]);
        if (findAll.getDataisnull()) {
            final CocBinding bd = new CocBinding();
            bd.setMsg(split[2]);
            bd.setQqcode(msg.getQQ());
            bd.setTag(split[1]);
            bd.setType("\u6751\u5e84");
            Custom.bdservice.save(bd);
            msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "\n[CQ:emoji,id=9989]\u4ee5\u540e\u4f60\u5bf9\u6211\u4e0b\u8fbe\u6307\u4ee4\n[CQ:emoji,id=128073]" + split[2] + "[CQ:emoji,id=128072]\n\u6211\u5c31\u544a\u8bc9\u4f60[CQ:emoji,id=127918]'" + findAll.getName() + "' " + findAll.getTag() + "\u7684\u6751\u5e84\u4fe1\u606f\uff01\n[CQ:face,id=183]\u8bb0\u4f4f\u4e86\u54e6,\u8fd9\u662f\u6211\u4eec\u4e24\u4e2a\u4eba\u4e4b\u95f4\u7684\u6697\u53f7\uff01");
        }
        else {
            msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:emoji,id=10060]\u51fa\u73b0\u4ee5\u4e0b\u95ee\u9898\uff0c\u5bfc\u81f4\u7ed1\u5b9a\u5931\u8d25\u3002\n" + findAll.getError());
        }
    }
    
    public static void ClansClan(final String[] split, final PrivateMsg msg, final CQCodeUtil cqCodeUtil, final MsgSender msgsender) {
        split[1] = split[1].toUpperCase();
        split[2] = split[2].replaceAll(" ", "").replaceAll("\t", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
        final CocBinding qq = Custom.bdservice.get(msg.getQQ(), split[2]);
        if (qq != null) {
            msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:emoji,id=10060]\u7ed1\u5b9a\u5931\u8d25\uff0c\u540c\u4e00\u4e2a\u4ee3\u79f0\u4e0d\u80fd\u540c\u65f6\u7ed1\u5b9a\u591a\u4e2a\u6807\u7b7e\uff01");
            return;
        }
        msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "\u6b63\u5728\u67e5\u8be2[CQ:emoji,id=127758][CQ:emoji,id=127758][CQ:emoji,id=127758]\n[#" + split[1] + "] \u90e8\u843d\u662f\u5426\u771f\u5b9e\u5b58\u5728");
        final ClansClanTag findAll = ClansClanTagCustom.findAll(split[1]);
        if (findAll.getDataisnull()) {
            final CocBinding bd = new CocBinding();
            bd.setMsg(split[2]);
            bd.setQqcode(msg.getQQ());
            bd.setTag(split[1]);
            bd.setType("\u90e8\u843d");
            Custom.bdservice.save(bd);
            msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "\n[CQ:emoji,id=9989]\u4ee5\u540e\u4f60\u5bf9\u6211\u4e0b\u8fbe\u6307\u4ee4\n[CQ:emoji,id=128073]" + split[2] + "[CQ:emoji,id=128072]\n\u6211\u5c31\u544a\u8bc9\u4f60[CQ:emoji,id=127918]'" + findAll.getName() + "' " + findAll.getTag() + "\u7684\u90e8\u843d\u4fe1\u606f\uff01\n[CQ:face,id=183]\u8bb0\u4f4f\u4e86\u54e6,\u8fd9\u662f\u6211\u4eec\u4e24\u4e2a\u4eba\u4e4b\u95f4\u7684\u6697\u53f7\uff01");
        }
        else {
            msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "[CQ:emoji,id=10060]\u51fa\u73b0\u4ee5\u4e0b\u95ee\u9898\uff0c\u5bfc\u81f4\u7ed1\u5b9a\u5931\u8d25\u3002\n" + findAll.getError());
        }
    }
    
    public static boolean relieve(final String string, final String qqCode, final MsgSender msgsender) {
        final CocBinding qq = Custom.bdservice.get(qqCode, string);
        if (qq != null) {
            Custom.bdservice.removeById(qq);
            msgsender.SENDER.sendPrivateMsg(qqCode, "[CQ:at,qq=" + qqCode + "] \u5df2\u6210\u529f\u89e3\u7ed1  '" + string + "'\uff0c\u4ee5\u540e\u8fd9\u4e2a\u6697\u53f7\u5c31\u65e0\u6548\u4e86\uff01");
            return true;
        }
        return false;
    }
    
    public static void goCocUrlLs(final String[] split, final GroupMsg msg, final CQCodeUtil cqCodeUtil, final MsgSender msgsender) {
    }
    
    static {
        Custom.bdservice = new CocBindingJDBC();
    }
}
