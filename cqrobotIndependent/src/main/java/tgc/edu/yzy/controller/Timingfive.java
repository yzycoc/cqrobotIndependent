package tgc.edu.yzy.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.forte.qqrobot.anno.timetask.CronTask;
import com.forte.qqrobot.beans.messages.result.GroupList;
import com.forte.qqrobot.beans.messages.result.inner.Group;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.timetask.TimeJob;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.Impl.OpenLayoutImpl;
import tgc.edu.yzy.bean.Lexicon;
import tgc.edu.yzy.custom.BaseConfiguration;
import tgc.edu.yzy.custom.QqCqcustom;
import tgc.edu.yzy.custom.SpringContextUtil;
import tgc.edu.yzy.custom.TimeUtils;
import tgc.edu.yzy.jdbc.OpenLayoutJDBC;
import tgc.edu.yzy.jdbc.XjMysqlJdbc;
import tgc.edu.yzy.service.LexiconService;

@CronTask("0 0 4 * * ? *")
public class Timingfive implements TimeJob{
	public static List<Lexicon> list = new ArrayList<>();
	public static List<Lexicon> numberlexicon =new ArrayList<>();
	public LexiconService lservice = SpringContextUtil.getBean(LexiconService.class);
	@Override
	public void execute(MsgSender msgSender, CQCodeUtil cqCodeUtil) {
		String a[] = {BaseConfiguration.getCqPath()+"\\data\\image\\image\\lscc",
				BaseConfiguration.getCqPath()+"\\data\\image\\image\\lstj",
				BaseConfiguration.getCqPath()+"\\data\\image\\image\\bldz",
				BaseConfiguration.getCqPath()+"\\data\\image\\image\\pz",
				"C:\\cocutil\\lscc","C:\\cocutil\\lstj","C:\\cocutil\\bldz","C:\\cocutil\\clanAll",
				BaseConfiguration.getCqPath()+"\\data\\image",
				BaseConfiguration.getCqPath()+"\\data\\image\\image\\clanAll"
		};
		for (String string : a) {
			delAllFile(string);
		}
		String group_list= "未获取到";
		try {
			GroupList groupList = msgSender.GETTER.getGroupList();
			Group[] list = groupList.getList();
			group_list = String.valueOf(list.length);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		MyqqRobot.list = lservice.query().eq("type", "模糊").eq("isuser", "1").ne("number", "随机").list();
		OpenLayoutImpl openlayout = SpringContextUtil.getBean(OpenLayoutImpl.class);
		String updateMap = openlayout.updateMap();
		String blackList = XjMysqlJdbc.blackList();
		String success = QqCqcustom.success(msgSender,false);
		String Image_Result = "";
		for(Entry<String, Integer> I_sum:MyqqRobot.Image_SUM.entrySet()){
			Image_Result += "\n"+I_sum.getKey()+":"+I_sum.getValue();
			OpenLayoutJDBC.ImageSave(I_sum.getKey(), I_sum.getValue(), I_sum.getKey());
			MyqqRobot.Image_SUM.put(I_sum.getKey(), 0);
		}
		String remsg = "词库已加载完毕！\n"
				+ "全发："+MyqqRobot.list.size()+"条数据\n"
				+ "共监管群聊："+group_list+" 个群！\n"
				+ updateMap+"\n"+blackList+"\n"+success+Image_Result;
		if("936642284".equals(MyqqRobot.administratorQQ)) {
			msgSender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,remsg);
		}else {
			msgSender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,remsg);
			msgSender.SENDER.sendPrivateMsg("936642284",remsg);
		}
	}
    /*** 
     * 删除指定文件夹下所有文件 
     *  
     * @param path 文件夹完整绝对路径 
     * @return 
     */  
    public static  boolean delAllFile(String path) {
    	System.out.println("@CronTask(\"0 0 4 * * ? *\")定时任务：清楚文件:"+path+"\t"+TimeUtils.getStringDate());
        boolean flag = false;  
        File file = new File(path);  
        if (!file.exists()) {  
            return flag;  
        }  
        if (!file.isDirectory()) {  
            return flag;  
        }  
        String[] tempList = file.list();  
        File temp = null;  
        for (int i = 0; i < tempList.length; i++) {  
            if (path.endsWith(File.separator)) {  
                temp = new File(path + tempList[i]);  
            } else {  
                temp = new File(path + File.separator + tempList[i]);  
            }  
            if (temp.isFile()) {  
                temp.delete();  
            }  
            if (temp.isDirectory()) {  
                //delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件  
                flag = true;  
            }  
        }  
        return flag;  
    } 
}
