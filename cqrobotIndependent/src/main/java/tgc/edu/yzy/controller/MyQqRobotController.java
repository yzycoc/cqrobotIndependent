package tgc.edu.yzy.controller;

import java.util.List;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.anno.depend.Beans;
import com.forte.qqrobot.beans.messages.msgget.FriendAddRequest;
import com.forte.qqrobot.beans.messages.msgget.GroupAddRequest;
import com.forte.qqrobot.beans.messages.msgget.GroupMemberIncrease;
import com.forte.qqrobot.beans.messages.msgget.GroupMemberReduce;
import com.forte.qqrobot.beans.messages.result.GroupInfo;
import com.forte.qqrobot.beans.messages.types.GroupAddRequestType;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.CqrobotIndependentApplication;
import tgc.edu.yzy.Impl.GroupRequestImpl;
import tgc.edu.yzy.bean.GroupList;
import tgc.edu.yzy.bean.GroupRequest;
import tgc.edu.yzy.custom.BaseConfiguration;
import tgc.edu.yzy.custom.CacheMap;
import tgc.edu.yzy.custom.QqCqcustom;
import tgc.edu.yzy.custom.SpringContextUtil;
import tgc.edu.yzy.custom.TimeUtils;
import tgc.edu.yzy.custom.Txt;
import tgc.edu.yzy.custom.UpdateGroupList;
import tgc.edu.yzy.jdbc.OpenLayoutJDBC;
import tgc.edu.yzy.jdbc.XjMysqlJdbc;
import tgc.edu.yzy.jdbc.entity.ScoureUserForm;
import tgc.edu.yzy.jdbc.entity.Support;
import tgc.edu.yzy.jdbc.score.Score;
import tgc.edu.yzy.service.GroupListService;

@Beans
public class MyQqRobotController {
	
	private static  CacheMap<String, Boolean> ca = new CacheMap<>();
	//获取群申请数据库信息
	private GroupRequestImpl groupRequestImpl = SpringContextUtil.getBean(GroupRequestImpl.class);
	
	@Listen(MsgGetTypes.groupAddRequest)
	public void groupAddRequest(GroupAddRequest msg,MsgSender sender,GroupAddRequestType requestType,CQCodeUtil cqCodeUtil) {//	群添加请求事件
		/*if(MyqqRobot.GROUP_GoTo) {
			ScoureUserForm updateGroupIsNull = Score.updateGroupIsNull(msg.getGroup());
			if(updateGroupIsNull!=null) {
				msgsender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,msg.getGroup()+"这是一个不受管理的新群，机器正在加入"+msg.getBeOperatedQQ());
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), "大家好，我是天使，主要服务于COC类的群聊![CQ:face,id=205]\nPS：我的所有功能均已开启！发送‘指令’可查看具体指令，进入网站http://yzycoc.com/i/A即可知道天使的相关细则哦！记住哦，天使免费分享的哦，存在交易情况记得联系作者拉黑。");
				QqCqcustom.updateGroupList(msg,msgsender);
				Boolean robotUpdate = UpdateGroupList.RobotUpdate(msgsender, cqCodeUtil,msg.getGroup());
				if(robotUpdate&&!"永久授权".equals(updateGroupIsNull.getCreateName())) {
					boolean deleteGroup = Score.deleteGroup(msg.getGroup());
					System.out.println("删除一次性拉群的用户群"+msg.getGroup()+"\t状态"+deleteGroup);
				}
			}else {
				
			}
		}*/
		//判断邀请方是不是自己
		if(!UpdateGroupList.qqgroupList.contains(msg.getGroup())) {
			
			Boolean boolean1 = ca.get(msg.getGroup());
			if(boolean1!=null&&boolean1) {
				return ;
			}
			ca.putPlusMinutes(msg.getGroup(), true, 5);
			ca.detect();
			StringBuffer Qq_Msg = new StringBuffer();
			Qq_Msg.append("你现在正在邀请天使加入群聊："+msg.getGroup()+",目前机器状态为");
			String remsg = msg.getQQ()+"想让你去"+msg.getGroup();
			GroupRequest re = new GroupRequest();
			re.setGroupcode(msg.getGroup());
			re.setQq(msg.getQQ());
			re.setFlag(msg.getFlag());
			re.setType("1");
			if(MyqqRobot.GROUP_GoTo) {
				ScoureUserForm updateGroupIsNull = Score.updateGroupIsNull(msg.getGroup());
				if(updateGroupIsNull!=null) {
					re.setType("购买积分自动进群");
					MyqqRobot.groupList.add(msg.getGroup());
					requestType = GroupAddRequestType.INVITE;
		            CqrobotIndependentApplication.msgSender.SETTER.setGroupAddRequest(msg.getFlag(), requestType, true, msg.getFlag());
		            Qq_Msg.append("积分购买机会后可自动进群,您已购买，现已自动进群。");
					Boolean robotUpdate = UpdateGroupList.RobotUpdate(sender, cqCodeUtil,msg.getGroup());
					StringBuffer buffer = new StringBuffer();
					buffer.append(msg.getGroup()+"这是用积分购买的群，机器正在加入!");
					if(robotUpdate&&!"永久授权".equals(updateGroupIsNull.getCreateName())) {
						boolean deleteGroup = Score.deleteGroup(msg.getGroup());
						System.out.println("删除一次性拉群的用户群"+msg.getGroup()+"\t状态"+deleteGroup);
						buffer.append("一次性用户，且已删除使用次数!");
					}
					sender.SENDER.sendPrivateMsg("936642284",buffer.toString());
				}else {
					Qq_Msg.append("积分购买机会后可自动进群,关于如何兑换请看http://yzycoc.com/i/C指令，你也可以前往网站http://yzycoc.com/i/AA查看人工同意需要的规则。");
					
				}
			}else {
				Qq_Msg.append("积分购买自动进群机会也无法自动加入群聊（群已满）,请认真查看http://yzycoc.com/i/AA审核规则。");
			}
			try {
				GroupInfo groupInfo = sender.GETTER.getGroupInfo(msg.getGroup());
				String[] adminList = groupInfo.getAdminList();
				String adminLists = "" ;
				for (int i = 0; i < adminList.length; i++) {adminLists += adminList[i] + ",";}
				re.setBoard(groupInfo.getCompleteIntro());
				re.setGroupname(groupInfo.getName());
				re.setGroupadmin(adminLists);
				re.setGroupadminstartor(groupInfo.getOwnerQQ());
				re.setNumber(groupInfo.getMemberNum().toString());
				re.setMaxnumber(groupInfo.getMaxMember().toString());
				groupRequestImpl.save(re);
				Qq_Msg.append("您本次邀请已记录到数据库，如果可以，我们将会在后台进行审核同意。");
			} catch (Exception e) {
				remsg+="，但是失败了.";
				re.setBoard("获取群错误，可能的问题，群名群介绍带特殊表情");
				groupRequestImpl.save(re);
				Qq_Msg.append("您本次邀请已记录到数据库，但是我们无法获取你群的详细信息，请您运行你的群被搜索，这样我们才能选择性的进行同意，请修改您的群设置后5分钟再次尝试邀请。");
				new Txt().set(e.toString());
			}
			sender.SENDER.sendPrivateMsg(msg.getQQ(), Qq_Msg.toString());
			remsg += "\n\n复制我同意群邀请["+msg.getFlag()+"]";
			System.out.println(remsg);
		}else {
			Integer integer = MyqqRobot.myrobotqq.get(msg.getQQ());
			if(integer == 1) {
				requestType = GroupAddRequestType.ADD;
				sender.SETTER.setGroupAddRequest(msg.getFlag(), requestType, false, "机器人！");
				return;
			}else if(integer == 2) {
				requestType = GroupAddRequestType.ADD;
				sender.SETTER.setGroupAddRequest(msg.getFlag(), requestType, false, "天使一级黑名单，不予处理！");
				return;
			}else if(integer == 4) {
				requestType = GroupAddRequestType.ADD;
				sender.SETTER.setGroupAddRequest(msg.getFlag(), requestType, false, "黑名单用户，自动拒绝！");
				sender.SENDER.sendGroupMsg(msg.getGroup(), "刚刚黑名单用户“"+msg.getQQ()+"”预加群，已被拦截。");
				return;
			}
		}
	}
	
	/****
	 *请求-添加好友
	 */
	@Listen(MsgGetTypes.friendAddRequest)
	@Filter
	public void friendAddRequest(FriendAddRequest msg,MsgSender sender) {//	好友添加申请事件
		if(MyqqRobot.PRIVATE_GoTo) {
			String addReturn = "";
			System.out.println(msg.getQQ()+"请求-添加好友");
			//黑名单管理
			Integer integer = MyqqRobot.myrobotqq.get(msg.getQQ());
			if(integer!=null) {
				return;
			}
			try {
				Support findSupportAll = OpenLayoutJDBC.findSupportAll();//
				List<String> qq = findSupportAll.getQq();
				if(qq.contains(msg.getQQ())) {
					sender.SETTER.setFriendAddRequest(msg.getFlag(), TimeUtils.getStringDate(),true);
					new XjMysqlJdbc().setQQlist(BaseConfiguration.getLocalQQCode(), msg.getQQ());
					MyqqRobot.myrobotqq.put("qqlist",MyqqRobot.myrobotqq.get("qqlist")+1);
					System.out.println("支持者添加好友啦！！！");
				}else {
					Integer qqList = new XjMysqlJdbc().QQList(msg.getQQ());
					Integer scoureUser = Score.getScoureUser("qq", msg.getQQ());
					if(qqList!=null&&qqList<scoureUser) {
						if(qqList==1) {
							addReturn = "\n\n这是你添加的第"+(qqList+1)+"个机器人，目前你已经处于被限制添加好友状态。请勿添加多个机器人占用好友资源。如果后期有需要，可以找大群管理进行说明解除限制！";
						}
						sender.SETTER.setFriendAddRequest(msg.getFlag(), TimeUtils.getStringDate(),true);
						new XjMysqlJdbc().setQQlist(BaseConfiguration.getLocalQQCode(), msg.getQQ());
					}
				}
			}catch (Exception e) {
				sender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ, msg.getQQ()+"已添加为好友！发送错误" + msg.getFlag());
			}
			sender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ, msg.getQQ()+"已添加为好友！" + msg.getFlag());
			sender.SENDER.sendPrivateMsg(msg.getQQ(), "您好，机器人天使为你服务！\r\n"
					+ "所有功能均已开启！发送‘指令’可查看具体指令！需要报名、群管功能、需要自己设置专属自己的机器人请添加群聊893778789，了解熙酱的功能！"
					+ "\n[CQ:emoji,id=12349]天使体验群请加190926109！有什么问题，优化，bug欢迎加群反馈！也欢迎热爱COC的玩家加入！"
					+ "\n[CQ:emoji,id=128276]输入‘指令’可以查看我具有那些指令哦"+addReturn);
		}
	}
	/***
	 * 检测到加群
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	@Listen(MsgGetTypes.groupMemberIncrease)
	@Filter	
	public void groupindex(GroupMemberIncrease msg,CQCodeUtil cqCodeUtil,MsgSender msgsender) {
		try {
			//BaseConfiguration.getLocalQQCode() 本机QQ号
			if(msg.getBeOperatedQQ().equals(BaseConfiguration.getLocalQQCode())) {
				if(MyqqRobot.groupList.contains(msg.getGroup())) {
					msgsender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,"机器加入了一个新的群聊天："+msg.getGroup());
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), "大家好，我是天使，主要服务于COC类的群聊![CQ:face,id=205]\nPS：我的所有功能均已开启！发送‘指令’可查看具体指令，进入网站http://yzycoc.com/i/A即可知道天使的相关细则哦！指令操作详情请查看http://yzycoc.com/i/C（点击左上角的正方形可以查看其他指令哦）");
					QqCqcustom.updateGroupList(msg,msgsender);
					MyqqRobot.groupList.remove(msg.getGroup());
					UpdateGroupList.uodate(msgsender, cqCodeUtil);
				}else {
					ScoureUserForm updateGroupIsNull = Score.updateGroupIsNull(msg.getGroup());
					if(updateGroupIsNull!=null) {
						msgsender.SENDER.sendGroupMsg(msg.getGroup(), "大家好，我是天使，主要服务于COC类的群聊![CQ:face,id=205]\nPS：我的所有功能均已开启！发送‘指令’可查看具体指令，进入网站http://yzycoc.com/i/A即可知道天使的相关细则哦！指令操作详情请查看http://yzycoc.com/i/C（点击左上角的正方形可以查看其他指令哦）");
						Boolean robotUpdate = UpdateGroupList.RobotUpdate(msgsender, cqCodeUtil,msg.getGroup());
						if(robotUpdate&&!"永久授权".equals(updateGroupIsNull.getCreateName())) {
							boolean deleteGroup = Score.deleteGroup(msg.getGroup());
							System.out.println("删除一次性拉群的用户群"+msg.getGroup()+"\t状态"+deleteGroup);
						}
						MyqqRobot.groupList.remove(msg.getGroup());
						msgsender.SENDER.sendPrivateMsg("936642284","使用积分自动同意进群，"+updateGroupIsNull.getCreateName()+"，机器加入了一个新的群聊天："+msg.getGroup());
					}else {
						msgsender.SENDER.sendGroupMsg(msg.getGroup(), "此群还未经过审核，无法进行使用。你可以使用积分兑换自动进群的模式，这样就可以不需要审核也能自动加入群聊，兑换方式请看http://yzycoc.com/i/C。你也可以将群设置为需要审核才能进入的群聊，这样你就可以正常邀请天使，走人工审核流程了。相关规则请看http://yzycoc.com/i/AA。");
						msgsender.SETTER.setGroupLeave(msg.getGroup());
					}
				}
			}
		} catch (Exception e) {
			msgsender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,"酷Q机器人发生错误，位置在[更新群聊]!QQ群："+msg.getGroup()+"！时间在："+TimeUtils.getStringDate() );
		}
	}
	
	
	
	/***
	 * 	监听加群消息 并保存
	 * @param msg
	 */
	@Listen(MsgGetTypes.groupMemberIncrease)
	@Filter
	public void groupMemberIncrease(GroupMemberIncrease msg) {
		if(msg.getGroup().length()<4) {
			return ;
		}
		GroupListService Service = SpringContextUtil.getBean(GroupListService.class);
		GroupList group = Service.query().eq("groupcode", msg.getGroup()).one();
		if(group!=null) {
			String number = group.getNumber();
			int i = 2;
			try {
				i = Integer.parseInt(number);
			} catch (Exception e) {
				i = 2;
			}
			number  = (i + 1) +"";
			group.setNumber(number);
			Service.updateById(group);
		}
	}
	
	/****
	 * 监听退群消息
	 * @param msg
	 */
	@Listen(MsgGetTypes.groupMemberReduce)
	@Filter
	public void groupMemberReduce(GroupMemberReduce msg) {
		
		if(msg.getGroup().length()<4) {
			return ;
		}
		GroupListService Service = SpringContextUtil.getBean(GroupListService.class);
		GroupList group = Service.query().eq("groupcode", msg.getGroup()).one();
		if(group!=null) {
			String number = group.getNumber();
			int i = 2;
			try {
				i = Integer.parseInt(number);
			} catch (Exception e) {
				i = 2;
			}
			number  = (i - 1) +"";
			group.setNumber(number);
			Service.updateById(group);
		}
	}
	
}
