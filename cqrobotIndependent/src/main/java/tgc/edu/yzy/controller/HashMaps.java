package tgc.edu.yzy.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import tgc.edu.yzy.custom.TimeUtils;

public class HashMaps<K,V> {
	public HashMap<K,String[]> map = new HashMap<>();
	/*public static void main(String args[]) {
		HashMaps<String, String[]> map = new HashMaps<>();
		String file = map.get("bldz");
	}*/
	public String get(String K){
		String[] strings = map.get(K);
		if(strings==null) {
			return null;
		}
		String data = strings[0];
		if(TimeUtils.timedifference(data) < 0) {
			map.remove(K);
			return null;
		}else {
			return strings[1];
		}
	}
	/****
	 * 分钟
	 * @param k 
	 * @param value 存入的值
	 * @param date
	 */
	public void setMin(K k,String value,int date) {
		String endMin = TimeUtils.endMin(date);
		String[] in = {endMin,value};
		map.put(k, in);
	}
	
	public void remove() {
		map = new HashMap<>();
	}
	
	public void tostring() {
		 Set<K> keyset=map.keySet();
		 Iterator<K> it= keyset.iterator();
		 while(it.hasNext()) {
			 Object key=it.next();
	         String[] val=map.get(key);
	         System.out.println(key+" : "+Arrays.toString(val));
		 }
	}
}
