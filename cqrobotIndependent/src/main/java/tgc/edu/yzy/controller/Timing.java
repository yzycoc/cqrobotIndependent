package tgc.edu.yzy.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.forte.qqrobot.anno.timetask.CronTask;
import com.forte.qqrobot.beans.messages.types.GroupAddRequestType;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.timetask.TimeJob;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.Impl.GroupRequestImpl;
import tgc.edu.yzy.bean.GroupRequest;
import tgc.edu.yzy.custom.SpringContextUtil;
import tgc.edu.yzy.custom.TimeUtils;
import tgc.edu.yzy.jdbc.OpenLayoutJDBC;
import tgc.edu.yzy.jdbc.entity.Support;
@CronTask("0 14,34,54 * * * ? *")
public class Timing  implements TimeJob{
	private GroupRequestImpl dao = SpringContextUtil.getBean(GroupRequestImpl.class);
	
	@Override
	public void execute(MsgSender msgSender, CQCodeUtil cqCodeUtil) {
		//获取所有还在请求的人
		List<GroupRequest> list = dao.query().eq("type","1").list();
		if(list.size() == 0&&!MyqqRobot.GROUP_GoTo) {
			return;
		}
		// 获取赞助者的人
		Support findSupportAll = OpenLayoutJDBC.findSupportAll();//
		List<String> grouplist = findSupportAll.getGrouplist();
		List<String> qq = findSupportAll.getQq();
		//1.0首先同意满足QQ群
		for (GroupRequest groupRequest : list) {
			String groupcode = groupRequest.getGroupcode() ==  null?"-":groupRequest.getGroupcode();
			if(grouplist.contains(groupcode)) {
				GroupAddRequestType requestType = GroupAddRequestType.INVITE;
				String flag = groupRequest.getFlag();
				msgSender.SETTER.setGroupAddRequest(flag, requestType, true, flag);
				MyqqRobot.myrobotqq.put("qqlist",MyqqRobot.myrobotqq.get("qqlist")+1);
				groupRequest.setCreateName("支持者群："+groupcode);
				groupRequest.setCreateDate(TimeUtils.getStringDate());
				groupRequest.setType("已同意");
				dao.updateById(groupRequest);
				System.out.println("支持者群同意！"+TimeUtils.getStringDate());
				return ;
			}
		}
		
		//2.0满足拉的人是这个QQ号的
		for (GroupRequest groupRequest : list) {
			String groupcode = groupRequest.getGroupcode() ==  null?"-":groupRequest.getGroupcode();
			if(qq.contains(groupcode)) {
				GroupAddRequestType requestType = GroupAddRequestType.INVITE;
				String flag = groupRequest.getFlag();
				msgSender.SETTER.setGroupAddRequest(flag, requestType, true, flag);
				groupRequest.setCreateName("支持者用户："+groupcode);
				groupRequest.setCreateDate(TimeUtils.getStringDate());
				groupRequest.setType("已同意");
				dao.updateById(groupRequest);
				System.out.println("支持者拉人！"+TimeUtils.getStringDate());
				return ;
			}
		}
		//3.0根据规则同意
		for (GroupRequest groupRequest : list) {
			String board = groupRequest.getBoard();
			if(StringUtils.isEmpty(board)) {
				board="无";
			}
			String groupname = groupRequest.getGroupname();
			if(StringUtils.isEmpty(groupname)) {
				groupname="无";
			}
			if(board.indexOf("部落冲突")!=-1||board.indexOf("coc")!=-1||board.indexOf("COC")!=-1||groupname.indexOf("部落冲突")!=-1||groupname.indexOf("COC")!=-1||groupname.indexOf("coc")!=-1) {
				GroupAddRequestType requestType = GroupAddRequestType.INVITE;
				String flag = groupRequest.getFlag();
				msgSender.SETTER.setGroupAddRequest(flag, requestType, true, flag);
				groupRequest.setCreateName("自动同意");
				groupRequest.setCreateDate(TimeUtils.getStringDate());
				groupRequest.setType("已同意");
				dao.updateById(groupRequest);
				System.out.println("自动同意！"+TimeUtils.getStringDate());
				return ;
			}
		}
	}
	
	public static void main(String[] args) {
		String text = "";
		
		String digits = text.replaceAll("[^0-9]", "");
		if(StringUtils.isEmpty(text)) {
			System.out.println("aaaa");
		}
		System.out.println(digits);
	}
}
