package tgc.edu.yzy.controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.anno.depend.Beans;
import com.forte.qqrobot.beans.messages.msgget.GroupMsg;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.beans.types.CQCodeTypes;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.Impl.OpenLayoutImpl;
import tgc.edu.yzy.bean.CocBinding;
import tgc.edu.yzy.bean.GroupList;
import tgc.edu.yzy.bean.Lexicon;
import tgc.edu.yzy.bean.OpenLayout;
import tgc.edu.yzy.custom.AjaxResult;
import tgc.edu.yzy.custom.BaseConfiguration;
import tgc.edu.yzy.custom.CocCustom;
import tgc.edu.yzy.custom.DlCustom;
import tgc.edu.yzy.custom.GroupHtmlAndQqService;
import tgc.edu.yzy.custom.ImageCustom;
import tgc.edu.yzy.custom.QqCqcustom;
import tgc.edu.yzy.custom.RedisUtil;
import tgc.edu.yzy.custom.SpringContextUtil;
import tgc.edu.yzy.custom.TimeUtils;
import tgc.edu.yzy.custom.UpdateGroupList;
import tgc.edu.yzy.jdbc.OpenLayoutJDBC;
import tgc.edu.yzy.jdbc.XjMysqlJdbc;
import tgc.edu.yzy.jdbc.score.ScooureController;
import tgc.edu.yzy.mapper.LexiconMapper;
import tgc.edu.yzy.service.GroupListService;
import tgc.edu.yzy.service.LexiconService;

@Beans
public class QQGroupAtController {
	public LexiconMapper lsmapper = SpringContextUtil.getBean(LexiconMapper.class);
	public LexiconService lservice = SpringContextUtil.getBean(LexiconService.class);
	public GroupListService grouplistservice = SpringContextUtil.getBean(GroupListService.class);
	
	public static void main(String[] args) {
		String sql ="";
		System.out.println(StringUtils.isEmpty(sql));
	}
	
	@Listen(MsgGetTypes.groupMsg)
	@Filter
	public void grouprecords(GroupMsg msg,CQCodeUtil cqCodeUtil,MsgSender msgsender) {
		// 1.0 这一块，是查询限制发言
		if(StringUtils.isEmpty(msg.getQQ())) {
			return;
		}
		Integer integer =MyqqRobot.groupPl.get(msg.getQQ());
		if(integer!=null&&integer>3) {
			return ;
		}
		// 2.0 这一块，是对特殊发言进行处理 对其他用户不予处理
		if(msg.getQQ().equals(BaseConfiguration.getLocalQQCode())||msg.getQQ().equals("2854196310")||msg.getQQ().equals("2854196312")||msg.getQQ().equals("2854196306")||msg.getGroup().equals("893778789")||msg.getGroup().indexOf("点歌")!=-1) {
			if (msg.getGroup().equals("893778789") && (MyqqRobot.adminList.contains(msg.getQQ()) || MyqqRobot.administratorQQ.equals(msg.getQQ())) && ("退出长时间未使用的群聊".equals(msg.getMsg()) || "检测退群".equals(msg.getMsg()))) {
                this.quitQqGroup(msg, msgsender);
            }
			if(msg.getGroup().equals("893778789")&&("936642284".equals(msg.getQQ())||MyqqRobot.administratorQQ.equals(msg.getQQ()))&&(msg.getMsg().indexOf("同步")!=-1)) {
				tongbu(msg,msgsender);
			}
			if(msg.getGroup().equals("893778789")&&(MyqqRobot.adminList.contains(msg.getQQ())||MyqqRobot.administratorQQ.equals(msg.getQQ()))) {
				switch (msg.getMsg()) {
				case "天使状态":
					String success = QqCqcustom.success(msgsender,false);
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), success);
					return;
				default:
					break;
				}
			}
			return ;
			
		}
		// 3.0 查询时候存在机器人，对于这些机器人的发言不予处理 且退出群聊 ------- 黑名单系统
		Integer integer2 = MyqqRobot.myrobotqq.get(msg.getQQ());
		if((integer2!=null)&&!"893778789".equals(msg.getGroup())) {
			switch (integer2) {
			case 1://是机器人。退群
				msgsender.SETTER.setGroupLeave(msg.getGroup());
				UpdateGroupList.qqgroupList.remove(msg.getGroup());
				return;
			case 4://超级黑名单用户
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:emoji,id=10060]本群存在黑名单: "+msg.getQQ()+";对于此类危害机器人正常运行的人，我们采取敬而远之的态度。在此十分抱歉，我不能提供帮助，如有需要请先清理本群黑名单！");
				msgsender.SETTER.setGroupLeave(msg.getGroup());
				new XjMysqlJdbc().messageboard(BaseConfiguration.getLocalQQCode(),"群："+msg.getGroup()+"存在黑名单"+msg.getQQ());
				//删除这个群的状态
				UpdateGroupList.qqgroupList.remove(msg.getGroup());
				return;
			case 2://普通不需要理会的黑名单
				return;
			default:
				break;
			}
			
			
		}
		//读取信息，为空退出群聊
		String m = msg.getMsg();
		if(m==null) {
			return ;
		}
		if(m.length()>0) {
			try {
				String clansmsg  = m;
				//At 
				if(cqCodeUtil.isAt(clansmsg, BaseConfiguration.getLocalQQCode())) {
					//用户艾特触发事件
					String ms = cqCodeUtil.removeCQCodeFromMsg(msg.getMsg());
					ms = ms.replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "#");
					if("绑定情况".equals(ms)||"绑定列表".equals(ms)||"我的绑定情况".equals(ms)||"我的绑定".equals(ms)) {
						boolean restrict = QqCqcustom.groupPb(msg.getQQ());
						if(restrict) {
							msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
							return ;
						}
						List<CocBinding> list = Custom.bdservice.list(msg.getQQ());
						String gomsg = "≮-"+cqCodeUtil.getCQCode_At(msg.getQQ())+"-≯\r\n" + 
								"◎━━-绑定列表-━━◎\n ※类型  丨  ※标签  丨  ※代称";
						for (int i = 0; i < list.size(); i++) {
							CocBinding cocBinding = list.get(i);
							gomsg +="\n"+ cocBinding.getType()+"丨#"+cocBinding.getTag()+"丨"+cocBinding.getMsg();
						}
						gomsg += "\n◎━━━----◎----━━━◎";
						msgsender.SENDER.sendGroupMsg(msg.getGroup(), gomsg);
						grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
						return;
					}
					
					clansmsg  = cqCodeUtil.removeCQCodeFromMsg(clansmsg, CQCodeTypes.at);
					clansmsg = clansmsg.replace(" ", "");
					
					//绑定的村庄 存入缓存中吧
					//CocBinding qq = Custom.bdservice.query().eq("qqcode", msg.getQQ()).eq("msg", clansmsg).one();
					CocBinding qq = Custom.bdservice.get(msg.getQQ(), clansmsg);
					if(qq!=null) {
						switch (qq.getType()) {
						case "部落":
							CocCustom.ClanTag(qq.getTag(),msg,cqCodeUtil,msgsender);
							grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
							return ;
						case "村庄":
							//CocCustom.PlayersTag(qq.getTag(),msg,cqCodeUtil,msgsender);
							CocCustom.ImagePlayersTag(qq.getTag(),msg,cqCodeUtil,msgsender);
							grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
							return ;
						default:
							return ;
						}
					}
				}
				if("部落鱼情".equals(clansmsg)||"coc鱼情".equals(clansmsg)||"查询鱼情".equals(clansmsg)||"查询部落鱼情".equals(clansmsg)||"查询coc鱼情".equals(clansmsg)||"玩家状态".equals(clansmsg)||"全球玩家状态".equals(clansmsg)) {
					boolean restrict = QqCqcustom.groupPb(msg.getQQ());
					if(restrict) {
						msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
						return ;
					}
					yuqing(cqCodeUtil,msgsender,msg);
					grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
					return ;
				}
				Boolean binding = binding(clansmsg, msg,cqCodeUtil,msgsender);
				if(binding) {
					grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
					return ;
				}
				boolean clans = clans(clansmsg, msg,cqCodeUtil,msgsender);
				if(clans) {
					grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
					return ;
				}
				//阵型分享
				Boolean openLyuout = openLyuout(clansmsg,msg,cqCodeUtil,msgsender);
				if(openLyuout) {
					grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
					return ;
				}
				
				
				boolean lexicon = lexicon(clansmsg,msg,cqCodeUtil,msgsender);
				if(lexicon) {
					grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
					return ;
				}
				String ms = msg.getMsg().replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "#");
				String[] split = ms.split("#");
				if(split.length>1&&("积分".equals(split[0]))) {
					boolean restrict = QqCqcustom.groupPb(msg.getQQ());
					if(restrict) {
						msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
						return;
					}
					ScooureController Scooure = new ScooureController();
					AjaxResult ajaxResult = Scooure.get(split,msg.getQQ(),true);
					if(ajaxResult.getSuccess()) {
						msgsender.SENDER.sendGroupMsg(msg.getGroup(), ajaxResult.getMsg());
						grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
						return ;
					}
				}
				if(cqCodeUtil.isAt(m, BaseConfiguration.getLocalQQCode())) {
					//限制聊天频率
					boolean restrict = QqCqcustom.groupPb(msg.getQQ());
					if(restrict) {
						msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
						return;
					}
					String error ="";
					RedisUtil utils = SpringContextUtil.getBean(RedisUtil.class);
					Object object = utils.get("Tulin"+msg.getQQ());
					if(object!=null) {
						grouplistservice.update().eq("groupcode", msg.getGroup()).set("endtime", TimeUtils.getStringDate()).update();
						tuling(msg,cqCodeUtil,msgsender,clansmsg,error);
					}
				}
				if(MyqqRobot.adminList.contains(msg.getQQ())&&msg.getMsg()!=null&&msg.getMsg().length()>0) {
					Admin(msg,msgsender);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("异常：查询部落相关功能出错！群"+msg.getGroup()+"内容"+msg.getMsg());
			}
		}
			
		
		
	}


	private void Admin(GroupMsg msg, MsgSender msgsender) {
		String[] split = msg.getMsg().split("#");
		if(split.length==2) {
			switch (split[0]) {
			case "解除屏蔽":
				RedisUtil utils = SpringContextUtil.getBean(RedisUtil.class);
				Integer su = (Integer)utils.get(split[1]);
				if(su!=null&&su>3) {
					utils.del(split[1]);
					MyqqRobot.groupPl.remove(split[1]);
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 被屏蔽的状态已恢复！");
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 未被天使屏蔽，无需解除！");
				}
				return ;
			case "解除":
				utils = SpringContextUtil.getBean(RedisUtil.class);
				su = (Integer)utils.get(split[1]);
				if(su!=null&&su>3) {
					utils.del(split[1]);
					MyqqRobot.groupPl.remove(split[1]);
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 被屏蔽的状态已恢复！");
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 未被天使屏蔽，无需解除！");
				}
				return ;
			/*case "解除好友控制":
				Integer qqList = new XjMysqlJdbc().QQList(msg.getQQ());
				if(qqList>=2) {
					new XjMysqlJdbc().remove(msg.getQQ());
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 已经可以添加天使了！");
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 未被限制添加天使，无需解除好友添加限制！");
				}
				return ;*/
			/*case "解控":
				Integer qqLists = new XjMysqlJdbc().QQList(msg.getQQ());
				if(qqLists>=2) {
					new XjMysqlJdbc().remove(msg.getQQ());
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+ " 已经可以添加天使了！");
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), "QQ:"+split[1]+" 未被限制添加天使，无需解除好友添加限制！");
				}
				return ;*/
			default:
				break;
			}
		}
	}

	private void tongbu(GroupMsg GroupMsg, MsgSender msgsender) {
		String[] m = GroupMsg.getMsg().split("#");
		if(m.length==3) {
			String msg = m[2];
			String[] split = msg.split("\\[CQ:image,file=");
			if(split.length>1) {
				String re = "同步成功！代码如下：";
				//将所有图片的文件名取出
				String a[] = new String[split.length-1];
				for (int i = 1; i < split.length; i++) {
					a[i-1] = split[i].substring(0, split[i].indexOf("]"));
				}
				for (int i = 0; i < a.length; i++) {
					String string = a[i];
					//处理图片
					String cqPath = BaseConfiguration.getCqPath();
					InputStream input;
					try {
						input = new FileInputStream(cqPath+"\\data\\image\\"+string+".cqimg");
						FileOutputStream output = new FileOutputStream(cqPath+"\\data\\image\\image\\user\\"+m[1]+".cqimg" ); 
						FileCopyUtils.copy(input, output);
						re +="&#91;CQ:image,file=image/user/"+m[1]+"&#93;";
					} catch (Exception e) {
						e.printStackTrace();
					} 
				}
				msgsender.SENDER.sendPrivateMsg(GroupMsg.getQQ(), re);
			}
		}else if("同步".equals(GroupMsg.getMsg())){
			MyqqRobot.list = lservice.query().eq("type", "模糊").eq("isuser", "1").ne("number", "随机").list();
			OpenLayoutImpl openlayout = SpringContextUtil.getBean(OpenLayoutImpl.class);
			String updateMap = openlayout.updateMap();
			String blackList = XjMysqlJdbc.blackList();
			XjMysqlJdbc xj = new XjMysqlJdbc();
			xj.lexiconNo();
			String Image_Result = "";
			for(Entry<String, Integer> I_sum:MyqqRobot.Image_SUM.entrySet()){
				Image_Result += "\n"+I_sum.getKey()+":"+I_sum.getValue();
			}
			String remsg = "词库已加载完毕！\n"
					+ "全发："+MyqqRobot.list.size()+"条数据\n"
					+ updateMap+"\n"+blackList+"\n聊天违禁词："+MyqqRobot.lexiconNo.size()+"\n自动同意进群状态："+MyqqRobot.GROUP_GoTo+"\n自动添加好友状态："+MyqqRobot.PRIVATE_GoTo+Image_Result;
			if("936642284".equals(MyqqRobot.administratorQQ)) {
				msgsender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,remsg);
			}else {
				msgsender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,remsg);
				msgsender.SENDER.sendPrivateMsg("936642284",remsg);
			}
			
			UpdateGroupList.uodate(msgsender, null);
		}
	}
	/***
	 * 	退出群聊
	 * @param msg 
	 * @param msgsender
	 */
	private void quitQqGroup(GroupMsg msg, MsgSender msgsender) {
		List<GroupList> list = grouplistservice.list();
		String re ="已执行完毕！";
		int a = 0;
		List<List<String>> content = new ArrayList<>();
		for (GroupList groupList : list) {
			List<String> txt = new ArrayList<>();
			if(groupList.getGroupcode().equals("190926109")||groupList.getGroupcode().equals("893778789")||"851931545".equals(groupList.getGroupcode())) {
				continue;
			}
			String endtime = groupList.getEndtime();
			if(endtime==null||endtime=="") {
				txt.add(TimeUtils.CalculateTime(groupList.getEndtime()));
				txt.add(isNull(groupList.getGroupcode()));
				txt.add(isNull(groupList.getName()));
				txt.add(isNull(groupList.getNumber()));
				content.add(txt);
				msgsender.SETTER.setGroupLeave(groupList.getGroupcode());
				UpdateGroupList.qqgroupList.remove(msg.getGroup());
				grouplistservice.removeById(groupList);
				a++;
				continue;
			}else {
				long datadifference = TimeUtils.datadifference(endtime);
				if(datadifference>864000) {
					txt.add(TimeUtils.CalculateTime(groupList.getEndtime()));
					txt.add(isNull(groupList.getGroupcode()));
					txt.add(isNull(groupList.getName()));
					txt.add(isNull(groupList.getNumber()));
					content.add(txt);
					msgsender.SETTER.setGroupLeave(groupList.getGroupcode());
					UpdateGroupList.qqgroupList.remove(msg.getGroup());
					grouplistservice.removeById(groupList);
					a++;
				}
			}
		}
		if(a==0) {
			re="没有需要退出的群，已加入"+list.size()+"个群。";
		}else {
			//头部标题内容
			List<String> titles = new ArrayList<>();
		    titles.add("现有"+list.size()+"个群！退出 "+a+" 个超过10天未使用的群，以下为退出群列表：");
			//头部数据
			List<String[]> headTitles = new ArrayList<>();//,"MAX群人数","最后使用时间"
		    String[] h1 = new String[]{"使用时间","群号","群名","群人数"};
		    headTitles.add(h1);
		    List<List<List<String>>> allValue = new ArrayList<>();   
		    allValue.add(content);
		    try {
		    	String stringDates = TimeUtils.getStringDates();
				ImageCustom.graphicsGeneration(allValue,titles,headTitles ,"",4,BaseConfiguration.getCqPath()+"\\data\\image\\"+stringDates+".jpg");
				re = "[CQ:image,file="+stringDates+".jpg]";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), re);
	}
	private String isNull(String txt) {
		return txt==null?"—":txt;
	}


	/****
	 * 阵型分享
	 * @param clansmsg
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	private Boolean openLyuout(String clansmsg, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		if(clansmsg.indexOf("阵型")!=-1) {
			if("部落阵型".equals(clansmsg)||"coc阵型".equals(clansmsg)||"COC阵型".equals(clansmsg)) {
				boolean restrictc = QqCqcustom.groupPb(msg.getQQ());
				if(restrictc) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！如果需要阵型，可以前往http://yzycoc.com/qq/cocApi/openlayout。查看自己需要的阵型，有喜欢的复制编号在分享，以给天使降压。\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				//随机从map中取出一个值
				Map<String, OpenLayout> openlayoutMap = OpenLayoutImpl.openlayoutMap;
				String[] keys = openlayoutMap.keySet().toArray(new String[0]);
				Random random = new Random(); 
				String key = keys[random.nextInt(keys.length)];
				OpenLayout open = openlayoutMap.get(key);
				String dldate = open.getDldate();
				if(dldate!=null) {
					long timedifference = TimeUtils.timedifference(dldate);
					if(timedifference>0) {
						msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:face,id=190] 编号："+open.getNumber()+"\n[CQ:emoji,id=127758]"+open.getDlurl()+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark());
						return true;
					}
				}
				String url = DlCustom.dl(open.getUrl());
				open.setDlurl(url);
				open.setDldate(TimeUtils.endDdMM());
				String re = "[CQ:face,id=190] 编号："+open.getNumber()+"\n[CQ:emoji,id=127758]"+url+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark();
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), re);
				OpenLayoutJDBC.updateById(open);
				return true;
			}else {
				//这里也可以存入缓存中进行
				if(clansmsg.length()<6||clansmsg.length()>14) {
					return false;
				}
				OpenLayout op = OpenLayoutImpl.openlayoutMap.get(clansmsg);
				if(op!=null) {
					boolean restrictc = QqCqcustom.groupPb(msg.getQQ());
					if(restrictc) {
						msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！如果需要阵型，可以前往http://yzycoc.com/qq/cocApi/openlayout。查看自己需要的阵型，有喜欢的复制编号在分享，以给天使降压。\n解除时间："+TimeUtils.endMinTime(10));
						return true;
					}
					String dldate = op.getDldate();
					if(dldate!=null) {
						long timedifference = TimeUtils.timedifference(dldate);
						if(timedifference>0) {
							msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:face,id=190] 编号："+op.getNumber()+"\n[CQ:emoji,id=127758]"+op.getDlurl()+"[CQ:image,file="+op.getImageUrl()+"]"+op.getRemark());
							return true;
						}
					}
					String url = DlCustom.dl(op.getUrl());
					String re = "[CQ:face,id=190] 编号："+op.getNumber()+"\n[CQ:emoji,id=127758]"+url+"[CQ:image,file="+op.getImageUrl()+"]"+op.getRemark();
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), re);
					op.setDlurl(url);
					op.setDldate(TimeUtils.endDdHhmmss());
					OpenLayoutImpl.openlayoutMap.put(op.getNumber(), op);
					OpenLayoutJDBC.updateById(op);
					return true;
				}
				/*List<OpenLayout> list = openlayout.query().eq("number", clansmsg).list();
				for (OpenLayout open : list) {
					String dldate = open.getDldate();
					if(dldate!=null) {
						long timedifference = TimeUtils.timedifference(dldate);
						if(timedifference>0) {
							msgsender.SENDER.sendGroupMsg(msg.getGroup(), "[CQ:face,id=190] 编号："+open.getNumber()+"\n[CQ:emoji,id=127758]"+open.getDlurl()+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark());
							continue;
						}
					}
					String url = DlCustom.dl(open.getUrl());
					open.setDlurl(url);
					open.setDldate(TimeUtils.endDdHhmmss());
					String re = "[CQ:face,id=190] 编号："+open.getNumber()+"\n[CQ:emoji,id=127758]"+url+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark();
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), re);
					openlayout.updateById(open);
					continue;
				}
				if(list.size()>0) {
					return true;
				}*/
			}
		}
		return false;
	}

	/***
	 * 鱼情
	 * @param cqCodeUtil
	 * @param msgsender
	 * @param msg
	 */
	private void yuqing(CQCodeUtil cqCodeUtil, MsgSender msgsender, GroupMsg msg) {
		boolean yuqing = CocCustom.getYuqing();
		if(yuqing) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(),"[CQ:image,file=yuqing.jpg]");
		}else {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 鱼情获取失败，请稍后重试！");
		}
		return;
		/*String[] c = MyqqRobot.clashofclansforecaster;
		long datadifference = TimeUtils.datadifference(c[1]);
		if(datadifference<120&&"1".equals(c[2])) {
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), c[0]);
		}else {
			String goyuqing = CocCustom.goyuqing();
			if(goyuqing==null) {
				MyqqRobot.clashofclansforecaster[2] = "0";
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), c[0] + "\n"+datadifference/60+"◎━分钟前获取━◎");
			}else {
				MyqqRobot.clashofclansforecaster[0] = goyuqing;
				MyqqRobot.clashofclansforecaster[1] = TimeUtils.getStringDate();
				MyqqRobot.clashofclansforecaster[2] = "1";
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), goyuqing);
			}
		}*/
	}




	/***
	 * 应答功能
	 * @param clansmsg
	 * @param msg 
	 * @param cqCodeUtil
	 * @param msgsender
	 * @return
	 */
	private boolean lexicon(String clansmsg, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		//一1.首先查询是否由多条选择一条的 完全匹配
		/*Lexicon Lexicon =  lsmapper.findByNumber(clansmsg);
		if(Lexicon!=null&&Lexicon.getAntistop()!=null&&Lexicon.getAntistop()!="") {
			boolean restrict = QqCqcustom.groupPb(msg.getQQ());
			if(restrict) {
				msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
				return true;
			}
			msgsender.SENDER.sendGroupMsg(msg.getGroup(), GroupHtmlAndQqService.getMsghtml(Lexicon.getResponse(),cqCodeUtil,msg,msgsender));
			Lexicon.setSum(Lexicon.getSum()+1);
			lservice.updateById(Lexicon);
			return true;
		}*/
		//一2.做完全匹配模式
		//List<Lexicon> list2 = lservice.query().eq("type", "完全").eq("isuser", "1").eq("antistop", clansmsg).ne("number", "随机").list();
		List<Lexicon> list2 = lservice.query().eq("type", "完全").eq("isuser", "1").eq("antistop", clansmsg).list();
		if(list2.size()>0) {
			boolean restrict = QqCqcustom.groupPb(msg.getQQ());
			if(restrict) {
				msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
				return true;
			}
		}
		for (Lexicon lexicon : list2) {
			String number = lexicon.getNumber()==null?"-":lexicon.getNumber();
			if("随机".equals(number)) {
				String response = lexicon.getResponse();
				String[] split = response.split("#分割#");
				long max=split.length,min=0;
				int time = (int) (Math.random()*(max-min)+min);
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), GroupHtmlAndQqService.getMsghtml(split[time],cqCodeUtil,msg,msgsender));
				lexicon.setSum(lexicon.getSum()+1);
				lservice.updateById(lexicon);
			}else {
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), GroupHtmlAndQqService.getMsghtml(lexicon.getResponse(),cqCodeUtil,msg,msgsender));
				lexicon.setSum(lexicon.getSum()+1);
				lservice.updateById(lexicon);
			}
				
		}
		if(list2.size()>0) {
			return true;
		}
		boolean re = false;
		
		//后期优化，因为数据库做不到这个效果
		//二1.模糊匹配。多条选择一条
		/*List<Lexicon> numberlexicon = MyqqRobot.numberlexicon;
		Collections.shuffle(numberlexicon);//打乱表的顺序
		for (Lexicon lexi : numberlexicon) {
			if(clansmsg.indexOf(lexi.getAntistop())!=-1) {
				boolean restrict = QqCqcustom.groupPb(msg.getQQ());
				if(restrict) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), GroupHtmlAndQqService.getMsghtml(lexi.getResponse(),cqCodeUtil,msg,msgsender));
				lexi.setSum(lexi.getSum()+1);
				lservice.updateById(lexi);
				return true;
			}
		}*/
		
		//二2.首先做模糊匹配
		List<Lexicon> list = MyqqRobot.list;
		for (Lexicon lexicon : list) {
			if(clansmsg.indexOf(lexicon.getAntistop())!=-1) {
				boolean restrict = QqCqcustom.groupPb(msg.getQQ());
				if(restrict) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				String number = lexicon.getNumber()==null?"-":lexicon.getNumber();
				if("随机".equals(number)) {
					String response = lexicon.getResponse();
					String[] split = response.split("#分割#");
					long max=split.length,min=0;
					int time = (int) (Math.random()*(max-min)+min);
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), GroupHtmlAndQqService.getMsghtml(split[time],cqCodeUtil,msg,msgsender));
					lexicon.setSum(lexicon.getSum()+1);
					lservice.updateById(lexicon);
					re = true;
				}else {
					msgsender.SENDER.sendGroupMsg(msg.getGroup(), GroupHtmlAndQqService.getMsghtml(lexicon.getResponse(),cqCodeUtil,msg,msgsender));
					lexicon.setSum(lexicon.getSum()+1);
					lservice.updateById(lexicon);
					re = true;
				}
				
				
			}
		}
		if(re) {
			return true;
		}
		return false;
	}
	/****
	 * 玩家绑定
	 * @param clansmsg
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 */
	private Boolean binding(String clansmsg, GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		clansmsg = clansmsg.replaceAll("＃", "#");
		String[] split = clansmsg.split("#");
		if(split.length==3) {//绑定
			if("我的村庄".equals(split[2])||"我的部落".equals(split[2])) {
				msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 绑定失败！禁止绑定"+split[2]+"，请换一个触发词");
				return true;
			}
			switch (split[0]) {
			case "绑定村庄":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑定玩家":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑村庄":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑玩家":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑定部落":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑定城堡":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑部落":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑城堡":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "联赛":
				CocCustom.goCocUrlLs(split,msg,cqCodeUtil,msgsender);
				return true;
			case "部落联赛":
				CocCustom.goCocUrlLs(split,msg,cqCodeUtil,msgsender);
				return true;
			case "对战统计":
				CocCustom.goCocUrlLs(split,msg,cqCodeUtil,msgsender);
				return true;
			default:
				if(split[0].indexOf("绑")!=-1&&split[0].indexOf("部落")!=-1) {
					Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
					return true;
				}
				if(split[0].indexOf("绑")!=-1&&split[0].indexOf("村庄")!=-1) {
					Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
					return true;
				}
				
			}
		}else if(split.length==2) {//绑定
			switch (split[0]) {
			case "解除绑定":
				return Custom.relieve(split[1],msg.getQQCode(),msg.getGroup(),msgsender);
			case "解绑":
				return Custom.relieve(split[1],msg.getQQCode(),msg.getGroup(),msgsender);
			case "取消绑定":
				return Custom.relieve(split[1],msg.getQQCode(),msg.getGroup(),msgsender);
			default:
				return false;
			}
		}
		return false;
	}
	
	/***
	 * 查询部落的功能
	 * @param m
	 * @param msg
	 * @param cqCodeUtil
	 * @param msgsender
	 * @return
	 */
	private boolean clans(String m,GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		//查询部落
		m = m.replaceAll("＃", "#");
		String[] split = m.split("#");
		if(split.length==2) {
			String Qq_tag = split[1].replaceAll("[^a-z^A-Z^0-9]", "");
			switch (split[0]) {
			case "绑定部落":
				boolean restrict = QqCqcustom.groupPb(msg.getQQ());
				if(restrict) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 绑定规则\n绑定部落#标签#触发词\n·触发词：即使绑定你部落的名称！");
				return true;
			case "绑定村庄":
				boolean restrictH = QqCqcustom.groupPb(msg.getQQ());
				if(restrictH) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				msgsender.SENDER.sendGroupMsg(msg.getGroupCode(), cqCodeUtil.getCQCode_At(msg.getQQ())+" 绑定规则\n绑定部落#标签#触发词\n·触发词：即使绑定你部落的名称！");
				return true;
			case "查询部落":
				CocCustom.ClanTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "查询城堡":
				CocCustom.ClanTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "城堡":
				CocCustom.ClanTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "部落":
				CocCustom.ClanTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "我的部落":
				CocCustom.ClanTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "查询村庄":
				CocCustom.ImagePlayersTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "查询玩家":
				CocCustom.ImagePlayersTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "玩家":
				CocCustom.ImagePlayersTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "村庄":
				CocCustom.ImagePlayersTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
				
			case "玩家名":
				CocCustom.NamePlayers(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询玩家名":
				CocCustom.NamePlayers(split[1],msg,cqCodeUtil,msgsender);
				return true;
				
			case "部落名":
				CocCustom.NameClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询部落名":
				CocCustom.NameClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
				
			case "我的村庄":
				CocCustom.ImagePlayersTag(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "查询部落配置":
				boolean restrictC = QqCqcustom.groupPb(msg.getQQ());
				if(restrictC) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+""
						+ " 正在查询["+Qq_tag+"]的部落配置，请等待...");
				CocCustom.ClanAll(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "部落配置":
				boolean restrictc = QqCqcustom.groupPb(msg.getQQ());
				if(restrictc) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+""
						+ " 正在查询["+Qq_tag+"]的部落配置，请等待...");
				CocCustom.ClanAll(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "内测部落配置":
				boolean restrictcc = QqCqcustom.groupPb(msg.getQQ());
				if(restrictcc) {
					msgsender.SENDER.sendGroupMsg(msg.getGroupCode(),cqCodeUtil.getCQCode_At(msg.getQQ())+" 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
					return true;
				}
				msgsender.SENDER.sendGroupMsg(msg.getGroup(), cqCodeUtil.getCQCode_At(msg.getQQ())+""
						+ " 正在查询["+Qq_tag+"]的部落配置，请等待...");
				CocCustom.ClanAllstart(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "联赛":
				CocCustom.leagueinfo(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "查询联赛":
				CocCustom.leagueinfo(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "联赛统计":
				CocCustom.leagueinfo(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "部落战":
				CocCustom.Clanwar(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "查询部落战":
				CocCustom.Clanwar(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "部落对战":
				CocCustom.Clanwar(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			case "部落战统计":
				CocCustom.Clanwar(Qq_tag,msg,cqCodeUtil,msgsender);
				return true;
			default:
				if(split[0].indexOf("查询部落")!=-1) {
					CocCustom.ClanTag(Qq_tag,msg,cqCodeUtil,msgsender);
					return true;
				}
				if(split[0].indexOf("查询村庄")!=-1) {
					CocCustom.ImagePlayersTag(Qq_tag,msg,cqCodeUtil,msgsender);
					return true;
				}
				return false;
			}
		}
		return false;
	}
	static String[] tulin= {"f6dcad3b5465483bb4406e8b49e76f68","f655de2fc8cd44a28203f0a2b78fdbc8","f6a00b9a8dcf4ec4a76898b98b1d3905","bf4a74bb6ed1422bbf34fc394571533b"};
	private String[] lexiconNoMsg = {"可能发言嘴臭了，被作者限制了。","作者不让我回答emmmm~~","可能作者知道接下来知道我要开始嘴臭了，不让我说了。","应答千万种，文明靠大家！"
			,"可能语词比较低俗，已被作者屏蔽。","不回答你这个问题.","我拒绝回答。","不想回复你。","请不要触发我的“胡言乱语”模式。"};
	private void tuling(GroupMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender, String m, String error) {
		int max=tulin.length ,min=0;
		int r = (int) (Math.random()*(max-min)+min); 
		String goTulingUrl = QqCqcustom.goTulingUrl(tulin[r],m,msg.getQQ());
		List<String> lexiconNo = MyqqRobot.lexiconNo;
		for (String string : lexiconNo) {
			if(goTulingUrl.indexOf(string)!=-1) {
				int lexionmax=lexiconNoMsg.length ,lexionmin=0;
				int lexionr = (int) (Math.random()*(lexionmax-lexionmin)+lexionmin); 
				goTulingUrl = lexiconNoMsg[lexionr];
				break;
			}
		}
		msgsender.SENDER.sendGroupMsg(msg.getGroup(),cqCodeUtil.getCQCode_At(msg.getQQ())+" "+goTulingUrl+error);
	}
}
