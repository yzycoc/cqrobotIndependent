package tgc.edu.yzy.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.forte.qqrobot.anno.Filter;
import com.forte.qqrobot.anno.Listen;
import com.forte.qqrobot.anno.depend.Beans;
import com.forte.qqrobot.beans.messages.msgget.PrivateMsg;
import com.forte.qqrobot.beans.messages.types.GroupAddRequestType;
import com.forte.qqrobot.beans.messages.types.MsgGetTypes;
import com.forte.qqrobot.sender.MsgSender;
import com.forte.qqrobot.utils.CQCodeUtil;

import tgc.edu.yzy.Impl.OpenLayoutImpl;
import tgc.edu.yzy.bean.CocBinding;
import tgc.edu.yzy.bean.Lexicon;
import tgc.edu.yzy.bean.OpenLayout;
import tgc.edu.yzy.custom.AjaxResult;
import tgc.edu.yzy.custom.CocCustom;
import tgc.edu.yzy.custom.DlCustom;
import tgc.edu.yzy.custom.GroupHtmlAndQqService;
import tgc.edu.yzy.custom.QqAndHtml;
import tgc.edu.yzy.custom.QqCqcustom;
import tgc.edu.yzy.custom.RedisUtil;
import tgc.edu.yzy.custom.SpringContextUtil;
import tgc.edu.yzy.custom.TimeUtils;
import tgc.edu.yzy.custom.UpdateGroupList;
import tgc.edu.yzy.jdbc.OpenLayoutJDBC;
import tgc.edu.yzy.jdbc.XjMysqlJdbc;
import tgc.edu.yzy.jdbc.score.ScooureController;
import tgc.edu.yzy.mapper.LexiconMapper;
import tgc.edu.yzy.service.LexiconService;

@Beans
@Listen(MsgGetTypes.privateMsg)
public class QqPrivateMsgRecordController {
	public LexiconMapper lsmapper = SpringContextUtil.getBean(LexiconMapper.class);
	public LexiconService lservice = SpringContextUtil.getBean(LexiconService.class);
	public RedisUtil utils = SpringContextUtil.getBean(RedisUtil.class);
	
	@Filter
	public void qudaima(PrivateMsg msg,MsgSender msgsender,GroupAddRequestType requestType,CQCodeUtil cqCodeUtil) {
		if(msg==null||msg.getMsg()==null||msg.getMsg().length()<1) {
			return ;
		}
		String message = msg.getMsg();
		if(message.length()>2&&"留言".equals(message.substring(0, 2))) {
			new XjMysqlJdbc().messageboard(msg.getQQ(),msg.getMsg());
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "留言成功,作者不定时查看，欢迎留言疑似伤害机器人的行为，一经核实，永久拉黑！");
			return ;
		}
		String originalData = msg.getOriginalData();
		if(originalData.contains("邀请你加入群聊")) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "邀请我进群请先查看我的相关状态公告：http://yzycoc.com/i/AA。\n群组织893778789。请认真查看相关，不懂的可以发送“留言+想说的话”！如长时间不回复，请添加群组织找管理！");
			return ;
		}
		if(MyqqRobot.adminList.contains(msg.getQQ())) {
			String[] split = message.split("#");
			if(split.length==2) {
				switch (split[0]) {
				case "添加":
					String msg2 = msg.getMsg().substring(3,msg.getMsg().length());
					new XjMysqlJdbc().lexiconNo(msg2);
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "回复违禁词添加成功！");
					return ;
				case "解除屏蔽":
					Integer su = (Integer)utils.get(split[1]);
					if(su!=null&&su>2) {
						utils.del(split[1]);
						MyqqRobot.groupPl.remove(split[1]);
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"的状态已恢复！");
					}else {
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"未被天使屏蔽，无需解除！");
					}
					return ;
				case "解除":
					su = (Integer)utils.get(split[1]);
					if(su!=null&&su>2) {
						utils.del(split[1]);
						MyqqRobot.groupPl.remove(split[1]);
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"的状态已恢复！");
					}else {
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"未被天使屏蔽，无需解除！");
					}
					return ;
				/*case "解除好友控制":
					Integer qqList = new XjMysqlJdbc().QQList(msg.getQQ());
					if(qqList>=2) {
						new XjMysqlJdbc().remove(msg.getQQ());
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"已经可以添加天使了！");
					}else {
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"未被限制添加天使，无需解除好友添加限制！");
					}
					return ;
				case "解控":
					Integer qqLists = new XjMysqlJdbc().QQList(msg.getQQ());
					if(qqLists>=3) {
						new XjMysqlJdbc().remove(msg.getQQ());
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"已经可以添加天使了！");
					}else {
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "QQ:"+split[1]+"未被限制添加天使，无需解除好友添加限制！");
					}
					return ;*/
				default:
					break;
				}
			}
			
		}
		//黑名单管理
		Integer boolean1 = MyqqRobot.myrobotqq.get(msg.getQQ());
		if(boolean1!=null) {
			new XjMysqlJdbc().messageboard(msg.getQQ(),"黑名单用户私聊:"+msg.getMsg());
			return;
		}
		
		
		/***
		 * 管理员下达指令
		 */
		if(msg.getQQ().equals(MyqqRobot.administratorQQ)) {
			/** 发送[编号] 即可 同意别人的群邀请 */
			if(msg.getMsg().split("复制我同意群邀请&#91;").length>1) {
				String obj1 = msg.getMsg();
				obj1 = obj1.replaceAll("复制我同意群邀请&#91;","");
				obj1 = obj1.replaceAll("&#93;","");
				requestType = GroupAddRequestType.INVITE;
				boolean setGroupAddRequest = msgsender.SETTER.setGroupAddRequest(obj1,requestType, true,obj1);
				if(setGroupAddRequest) {
					//刷新所在的群
					UpdateGroupList.uodate(msgsender, cqCodeUtil);
				}else {
				}
				return;
			}
			if(msg.getMsg().indexOf("更新群")!=-1) {
				String updatemsg = msg.getMsg().replaceAll("更新群", "").replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "#");
				boolean updateGroupList = QqCqcustom.updateGroupList(msgsender,updatemsg);
				msgsender.SENDER.sendPrivateMsg(MyqqRobot.administratorQQ,"·状态：  "+updateGroupList+ " \n· 更新群："+updatemsg);
				return;
			}
			switch (msg.getMsg()) {
			case "开启":
				MyqqRobot.GROUP_GoTo = true;
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "自动同意群状态开启！");
				return;
			case "关闭":
				MyqqRobot.GROUP_GoTo = false;
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "自动同意群状态关闭！");
				return;
			case "好友开启":
				MyqqRobot.PRIVATE_GoTo = true;
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "自动同意好友开启！");
				return;
			case "好友关闭":
				MyqqRobot.PRIVATE_GoTo = false;
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "自动同意好友关闭！");
				return;
			default:
				break;
			}
		}
		// 1.0 这一块，是查询限制发言
		Integer integer = (Integer)utils.get(msg.getQQ());
		if(integer!=null&&integer>3) {
			return ;
		}else {
			boolean restrict = QqCqcustom.restrict(msg.getQQ(),utils);
			if(restrict) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ()," 检测你多次调用机器人，为降低发言频率，现10分钟不予处理你的消息！\n解除时间："+TimeUtils.endMinTime(10));
				return;
			}
		}
		/** 发送[编号] 即可 同意别人的群邀请 */
		if(msg.getMsg().split("取代码").length>1) {
			String msgobj = msg.getMsg();
			if(msgobj!=null&&msgobj.length()>0&&msgobj.indexOf("CQ:image")!=-1) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), QqAndHtml.getMsgCq(msgobj) );
			}else {
				String obj = msgobj.replaceAll("\\[", "&#91;");
				obj = obj.replaceAll("\\]","&#93;");
				obj = obj.replaceAll("取代码","");
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), obj);
			}
			return;
		}
		
		String clansmsg = msg.getMsg();
		if("部落鱼情".equals(clansmsg)||"coc鱼情".equals(clansmsg)||"查询鱼情".equals(clansmsg)||"查询部落鱼情".equals(clansmsg)||"查询coc鱼情".equals(clansmsg)||"玩家状态".equals(clansmsg)||"全球玩家状态".equals(clansmsg)) {
			yuqing(cqCodeUtil,msgsender,msg);
			return ;
		}
		//点赞的功能
		if(msg.getMsg().length()>0) {
			switch (msg.getMsg()) {
			case "点赞":
				msgsender.SENDER.sendLike(msg.getQQCode(), 10);
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "好，我赞了哦，你可要赞我呀！");
				return;
			default:
				break;
			}
		}
		//阵型分享
		Boolean openLyuout = openLyuout(clansmsg,msg,cqCodeUtil,msgsender);
		if(openLyuout) {
			return ;
		}
		//绑定
		boolean binding = binding( msg,cqCodeUtil,msgsender);
		if(binding) {
			return ;
		}
		
		//查询部落
		boolean clans = clans( msg,cqCodeUtil,msgsender);
		if (clans) {
			return ;
		}
		//绑定的村庄
		//CocBinding qq = Custom.bdservice.query().eq("qqcode", msg.getQQ()).eq("msg", msg.getMsg()).one();
		CocBinding qq = Custom.bdservice.get(msg.getQQ(), msg.getMsg());
		if(qq!=null) {
			switch (qq.getType()) {
			case "部落":
				CocCustom.ClanTag(qq.getTag(),msg,cqCodeUtil,msgsender);
				return ;
			case "村庄":
				CocCustom.ImagePlayersTag(qq.getTag(),msg,cqCodeUtil,msgsender);
				return ;
			default:
				return ;
			}
		}
			//用户艾特出发事件
		String ms = cqCodeUtil.removeCQCodeFromMsg(msg.getMsg());
		ms = ms.replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "#");
		if("绑定情况".equals(ms)||"绑定列表".equals(ms)||"我的绑定情况".equals(ms)||"我的绑定".equals(ms)) {
			List<CocBinding> list = Custom.bdservice.list(msg.getQQ());
			String gomsg = "≮-"+cqCodeUtil.getCQCode_At(msg.getQQ())+"-≯\r\n" + 
					"◎━━-绑定列表-━━◎\n ※类型  丨  ※标签  丨  ※代称";
			for (int i = 0; i < list.size(); i++) {
				CocBinding cocBinding = list.get(i);
				gomsg +="\n"+ cocBinding.getType()+"丨#"+cocBinding.getTag()+"丨"+cocBinding.getMsg();
			}
			gomsg += "\n◎━━━----◎----━━━◎";
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), gomsg);
			return;
		}
			
		//词库答复
		boolean lexicon = lexicon(msg,cqCodeUtil,msgsender);
		if (lexicon) {
			return ;
		}
		String jfmsg = msg.getMsg().replaceAll(" ", "").replaceAll("	", "").replaceAll(",", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("＃", "#");
		String[] split = jfmsg.split("#");
		if(split.length>1&&("积分".equals(split[0]))) {
			ScooureController scoour = new ScooureController();
			AjaxResult ajaxResult = scoour.get(split,msg.getQQ(),false);
			if(ajaxResult.getSuccess()) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), ajaxResult.getMsg());
				return ;
			}
		}
		//限制聊天频率
		String error ="";
		Object object = utils.get("Tulin"+msg.getQQ());
		if(object!=null) {
			tuling(msg,cqCodeUtil,msgsender,error);
		}
	}
	private Boolean openLyuout(String clansmsg, PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		if(clansmsg.indexOf("阵型")!=-1) {
			if("部落阵型".equals(clansmsg)||"coc阵型".equals(clansmsg)||"COC阵型".equals(clansmsg)) {
				//限制聊天频率
				String error ="";
				
				Map<String, OpenLayout> openlayoutMap = OpenLayoutImpl.openlayoutMap;
				String[] keys = openlayoutMap.keySet().toArray(new String[0]);
				Random random = new Random(); 
				String key = keys[random.nextInt(keys.length)];
				OpenLayout open = openlayoutMap.get(key);
				String dldate = open.getDldate();
				if(dldate!=null) {
					long timedifference = TimeUtils.timedifference(dldate);
					if(timedifference>0) {
						msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:face,id=190] 编号："+open.getNumber()+"\n[CQ:emoji,id=127758]"+open.getDlurl()+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark());
						return true;
					}
				}
				String url = DlCustom.dl(open.getUrl());
				open.setDlurl(url);
				open.setDldate(TimeUtils.endDdMM());
				String re = "[CQ:face,id=190] 编号："+open.getNumber()+error+"\n[CQ:emoji,id=127758]"+url+"[CQ:image,file="+open.getImageUrl()+"]"+open.getRemark();
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), re);
				OpenLayoutJDBC.updateById(open);
				return true;
			}else {
				//这里也可以存入缓存中进行
				if(clansmsg.length()<6||clansmsg.length()>14) {
					return false;
				}
				OpenLayout op = OpenLayoutImpl.openlayoutMap.get(clansmsg);
				if(op!=null) {
					String dldate = op.getDldate();
					if(dldate!=null) {
						long timedifference = TimeUtils.timedifference(dldate);
						if(timedifference>0) {
							msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "[CQ:face,id=190] 编号："+op.getNumber()+"\n[CQ:emoji,id=127758]"+op.getDlurl()+"[CQ:image,file="+op.getImageUrl()+"]"+op.getRemark());
							return true;
						}
					}
					String url = DlCustom.dl(op.getUrl());
					String re = "[CQ:face,id=190] 编号："+op.getNumber()+"\n[CQ:emoji,id=127758]"+url+"[CQ:image,file="+op.getImageUrl()+"]"+op.getRemark();
					msgsender.SENDER.sendPrivateMsg(msg.getQQ(), re);
					op.setDlurl(url);
					op.setDldate(TimeUtils.endDdHhmmss());
					OpenLayoutImpl.openlayoutMap.put(op.getNumber(), op);
					OpenLayoutJDBC.updateById(op);
					return true;
				}
				
			}
		}
		return false;
	}
	
	/***
	 * get鱼情
	 * @param cqCodeUtil
	 * @param msgsender
	 * @param msg
	 */
	private void yuqing(CQCodeUtil cqCodeUtil, MsgSender msgsender, PrivateMsg msg) {
		boolean yuqing = CocCustom.getYuqing();
		if(yuqing) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(),"[CQ:image,file=yuqing.jpg]");
		}else {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), "鱼情获取失败，请稍后重试！");
		}
		return;
		/*String[] c = MyqqRobot.clashofclansforecaster;
		long datadifference = TimeUtils.datadifference(c[1]);
		if(datadifference<120&&"1".equals(c[2])) {
			msgsender.SENDER.sendPrivateMsg(msg.getQQ(), c[0]);
		}else {
			String goyuqing = CocCustom.goyuqing();
			if(goyuqing==null) {
				MyqqRobot.clashofclansforecaster[2] = "0";
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), c[0] + "\n"+datadifference/60+"◎━分钟前获取━◎");
			}else {
				MyqqRobot.clashofclansforecaster[0] = goyuqing;
				MyqqRobot.clashofclansforecaster[1] = TimeUtils.getStringDate();
				MyqqRobot.clashofclansforecaster[2] = "1";
				msgsender.SENDER.sendPrivateMsg(msg.getQQ(), goyuqing);
			}
		}*/
	}

	private boolean binding(PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		String clansmsg = msg.getMsg().replaceAll("＃", "#");
		String[] split = clansmsg.split("#");
		if(split.length==3) {//绑定
			
			switch (split[0]) {
			case "绑定村庄":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑定玩家":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑村庄":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑玩家":
				Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑定部落":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑定城堡":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑部落":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "绑城堡":
				Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
				return true;
			case "联赛":
				CocCustom.goCocUrlLsprivatem(split,msg,cqCodeUtil,msgsender);
				return true;
			case "部落联赛":
				CocCustom.goCocUrlLsprivatem(split,msg,cqCodeUtil,msgsender);
				return true;
			case "对战统计":
				CocCustom.goCocUrlLsprivatem(split,msg,cqCodeUtil,msgsender);
				return true;
			default:
				if(split[0].indexOf("绑")!=-1&&split[0].indexOf("部落")!=-1) {
					Custom.ClansClan(split,msg,cqCodeUtil,msgsender);
					return true;
				}
				if(split[0].indexOf("绑")!=-1&&split[0].indexOf("村庄")!=-1) {
					Custom.ClansPlayer(split,msg,cqCodeUtil,msgsender);
					return true;
				}
			}
		}else if(split.length==2) {//绑定
			switch (split[0]) {
			case "解除绑定":
				return Custom.relieve(split[1],msg.getQQCode(),msgsender);
			case "解绑":
				return Custom.relieve(split[1],msg.getQQCode(),msgsender);
			case "取消绑定":
				return Custom.relieve(split[1],msg.getQQCode(),msgsender);
			default:
				return false;
			}
		}
		return false;
	}
	private void tuling(PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender, String error) {
		int max=QQGroupAtController.tulin.length,min=0;
		int r = (int) (Math.random()*(max-min)+min); 
		String goTulingUrl = QqCqcustom.goTulingUrl(QQGroupAtController.tulin[r],msg.getMsg(),msg.getQQ());
		List<String> lexiconNo = MyqqRobot.lexiconNo;
		for (String string : lexiconNo) {
			if(goTulingUrl.indexOf(string)!=-1) {
				return ;
			}
		}
		msgsender.SENDER.sendPrivateMsg(msg.getQQCode(),goTulingUrl+error);
	}

	private boolean lexicon(PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		String clansmsg = msg.getMsg();
		/*Lexicon Lexicon =  lsmapper.findByNumber(clansmsg);
		if(Lexicon!=null&&Lexicon.getAntistop()!=null&&Lexicon.getAntistop()!="") {
			msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), GroupHtmlAndQqService.getMsghtml(Lexicon.getResponse(),cqCodeUtil,msg,msgsender));
			Lexicon.setSum(Lexicon.getSum()+1);
			lservice.updateById(Lexicon);
			return true;
		}*/
		//一2.做完全匹配模式
		List<Lexicon> list2 = lservice.query().eq("type", "完全").eq("isuser", "1").eq("antistop", clansmsg).list();
		for (Lexicon lexicon : list2) {
			String number = lexicon.getNumber()==null?"-":lexicon.getNumber();
			if("随机".equals(number)) {
				String response = lexicon.getResponse();
				String[] split = response.split("#分割#");
				long max=split.length,min=0;
				int time = (int) (Math.random()*(max-min)+min);
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), GroupHtmlAndQqService.getMsghtml(split[time],cqCodeUtil,msg,msgsender));
				lexicon.setSum(lexicon.getSum()+1);
				lservice.updateById(lexicon);
			}else {
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), GroupHtmlAndQqService.getMsghtml(lexicon.getResponse(),cqCodeUtil,msg,msgsender));
				lexicon.setSum(lexicon.getSum()+1);
				lservice.updateById(lexicon);
			}
			
		}
		if(list2.size()>0) {
			return true;
		}
		boolean re = false;
		
		//后期优化，因为数据库做不到这个效果
		//二1.模糊匹配。多条选择一条
		/*List<Lexicon> numberlexicon = MyqqRobot.numberlexicon;
		Collections.shuffle(numberlexicon);//打乱表的顺序
		for (Lexicon lexi : numberlexicon) {
			if(clansmsg.indexOf(lexi.getAntistop())!=-1) {
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), GroupHtmlAndQqService.getMsghtml(lexi.getResponse(),cqCodeUtil,msg,msgsender));
				lexi.setSum(lexi.getSum()+1);
				lservice.updateById(lexi);
				return true;
			}
		}
		*/
		//二2.首先做模糊匹配
		List<Lexicon> list = MyqqRobot.list;
		for (Lexicon lexicon : list) {
			if(clansmsg.indexOf(lexicon.getAntistop())!=-1) {
				String number = lexicon.getNumber()==null?"-":lexicon.getNumber();
				if("随机".equals(number)) {
					String response = lexicon.getResponse();
					String[] split = response.split("#分割#");
					long max=split.length,min=0;
					int time = (int) (Math.random()*(max-min)+min);
					msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), GroupHtmlAndQqService.getMsghtml(split[time],cqCodeUtil,msg,msgsender));
					lexicon.setSum(lexicon.getSum()+1);
					lservice.updateById(lexicon);
					re = true;
				}else {
					msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), GroupHtmlAndQqService.getMsghtml(lexicon.getResponse(),cqCodeUtil,msg,msgsender));
					lexicon.setSum(lexicon.getSum()+1);
					lservice.updateById(lexicon);
					re = true;
				}
			}
		}
		if(re) {
			return true;
		}
		return false;
	}

	private boolean clans(PrivateMsg msg, CQCodeUtil cqCodeUtil, MsgSender msgsender) {
		String m = msg.getMsg();
		//查询部落
		m = m.replaceAll("＃", "#");
		String[] split = m.split("#");
		if(split.length==2) {
			switch (split[0]) {
			case "查询部落":
				CocCustom.ClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询城堡":
				CocCustom.ClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "城堡":
				CocCustom.ClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "部落":
				CocCustom.ClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "部落名":
				CocCustom.NameClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询部落名":
				CocCustom.NameClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
				
				
			case "我的部落":
				CocCustom.ClanTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询村庄":
				CocCustom.ImagePlayersTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询玩家":
				CocCustom.ImagePlayersTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "村庄":
				CocCustom.ImagePlayersTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "玩家":
				CocCustom.ImagePlayersTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
				
			case "玩家名":
				CocCustom.NamePlayers(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询玩家名":
				CocCustom.NamePlayers(split[1],msg,cqCodeUtil,msgsender);
				return true;
				
			case "我的村庄":
				CocCustom.ImagePlayersTag(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询部落配置":
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "正在查询["+split[1]+"]的部落配置。请勿反复查询！");
				CocCustom.ClanAllprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "部落配置":
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "正在查询["+split[1]+"]的部落配置。请勿反复查询！");
				CocCustom.ClanAllprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "内测部落配置":
				msgsender.SENDER.sendPrivateMsg(msg.getQQCode(), "正在查询["+split[1]+"]的部落配置。请勿反复查询！");
				CocCustom.ClanAllstartprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "联赛":
				CocCustom.leagueinfoprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询联赛":
				CocCustom.leagueinfoprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "联赛统计":
				CocCustom.leagueinfoprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "部落战":
				CocCustom.Clanwarprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "查询部落战":
				CocCustom.Clanwarprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "部落对战":
				CocCustom.Clanwarprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			case "部落战统计":
				CocCustom.Clanwarprivate(split[1],msg,cqCodeUtil,msgsender);
				return true;
			default:
				if(split[0].indexOf("查询部落")!=-1) {
					CocCustom.ClanTag(split[1],msg,cqCodeUtil,msgsender);
					return true;
				}
				if(split[0].indexOf("查询村庄")!=-1) {
					CocCustom.ImagePlayersTag(split[1],msg,cqCodeUtil,msgsender);
					return true;
				}
			
			}
		}
		return false;
	}
}
